#!/usr/bin/env bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
SHADER_SOURCES_PATH=$SCRIPT_PATH/src/graphics/shaders
COMPILED_SHADERS_PATH=$SCRIPT_PATH/assets/shaders
GLSLC="glslc -O"

cd $SHADER_SOURCES_PATH
find . -type d -exec mkdir -p $COMPILED_SHADERS_PATH/{} \;
find . -type f \( -iname "*.vert" -o -iname "*.frag" \) -exec $GLSLC {} -o $COMPILED_SHADERS_PATH/{}.spv \;

