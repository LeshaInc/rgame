//! Asset management

mod handle;
mod handle_allocator;
mod manager;
mod progress;
mod source;

pub use self::handle::{AsAssetId, Handle, Id, WeakHandle};
pub use self::handle_allocator::HandleAllocator;
pub use self::manager::{Assets, NewAssets};
pub use self::progress::ProgressCounter;
pub use self::source::{AssetSource, DirectorySource};

use std::io::{BufRead, Seek, SeekFrom};
use std::path::Path;

use anyhow::{Context, Result};
use serde::Deserialize;

/// Asset or resource used by the game engine. Can be a sprite, a font, a sound, etc.
pub trait Asset: Sized + Send + Sync + 'static {}

/// Asset with the default loader
pub trait AssetDefaultLoader: Asset {
    /// Default loader type
    type Loader: AssetLoader<Self> + Default;
}

/// Asset loader
pub trait AssetLoader<A: Asset>: Sized + Send + Sync + 'static {
    /// Load an asset at the specified path
    fn load(&self, new: &NewAssets, source: &dyn AssetSource, path: &Path) -> Result<A>;
}

/// Trait for reading asset data
pub trait AssetReader: BufRead + Seek + Send + 'static {}

impl<T: BufRead + Seek + Send + 'static> AssetReader for T {}

/// Simple asset loader for assets consisting of one file only and no dependencies
pub trait SimpleAssetLoader<A: Asset>: Sized + Send + Sync + 'static {
    /// Load an asset from the specified reader
    fn load(&self, reader: impl AssetReader) -> Result<A>;
}

impl<A, L> AssetLoader<A> for L
where
    A: Asset,
    L: SimpleAssetLoader<A>,
{
    fn load(&self, _: &NewAssets, source: &dyn AssetSource, path: &Path) -> Result<A> {
        SimpleAssetLoader::load(self, source.load(path)?)
    }
}

/// Asset loader which processes bytes directly
pub trait ByteAssetLoader<A: Asset>: Sized + Send + Sync + 'static {
    /// Convert bytes to an asset
    fn load(&self, bytes: Vec<u8>) -> Result<A>;
}

impl<A, L> SimpleAssetLoader<A> for L
where
    A: Asset,
    L: ByteAssetLoader<A>,
{
    fn load(&self, mut reader: impl AssetReader) -> Result<A> {
        let size = reader.seek(SeekFrom::End(0))? as usize;
        reader.seek(SeekFrom::Start(0))?;
        let mut buf = Vec::with_capacity(size);
        reader.read_to_end(&mut buf)?;
        ByteAssetLoader::load(self, buf)
    }
}

/// JSON format loader. Works for any deserializable asset
#[derive(Default)]
pub struct JsonLoader;

impl<A> ByteAssetLoader<A> for JsonLoader
where
    A: for<'a> Deserialize<'a> + Asset,
{
    fn load(&self, bytes: Vec<u8>) -> Result<A> {
        Ok(serde_json::de::from_slice(&bytes).context("JSON parse error")?)
    }
}
