use std::any::type_name;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;
use std::sync::{Arc, Weak};

/// ID of an asset
pub struct Id<T> {
    /// The underlying integer representation of an ID
    pub inner: usize,
    marker: PhantomData<T>,
}

impl<T> Id<T> {
    /// Create an ID from integer
    pub const fn new(inner: usize) -> Id<T> {
        Id {
            inner,
            marker: PhantomData,
        }
    }
}

impl<T> Clone for Id<T> {
    fn clone(&self) -> Id<T> {
        Id {
            inner: self.inner,
            marker: PhantomData,
        }
    }
}

impl<T> Copy for Id<T> {}

impl<T> PartialEq for Id<T> {
    fn eq(&self, other: &Id<T>) -> bool {
        self.inner == other.inner
    }
}

impl<T> Eq for Id<T> {}

impl<T> Hash for Id<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.inner.hash(state);
    }
}

impl<T> fmt::Debug for Id<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}({})", type_name::<T>(), self.inner)
    }
}

impl<T> From<&Handle<T>> for Id<T> {
    fn from(handle: &Handle<T>) -> Id<T> {
        handle.id()
    }
}

/// Refcounted handle to a specific asset. Beware that handles are compared by pointer, not by inner ID
pub struct Handle<T> {
    inner: Arc<Id<T>>,
}

impl<T> Handle<T> {
    /// Create a new handle
    pub fn new(id: Id<T>) -> Handle<T> {
        Handle {
            inner: Arc::new(id),
        }
    }

    /// Get the underlying ID of the resource
    pub fn id(&self) -> Id<T> {
        *self.inner
    }

    /// Downgrade the handle to its weak counterpart, which doesn't keep the resource alive
    pub fn downgrade(&self) -> WeakHandle<T> {
        WeakHandle {
            inner: Arc::downgrade(&self.inner),
        }
    }
}

impl<T> Clone for Handle<T> {
    fn clone(&self) -> Handle<T> {
        Handle {
            inner: self.inner.clone(),
        }
    }
}

impl<T> PartialEq for Handle<T> {
    fn eq(&self, other: &Handle<T>) -> bool {
        Arc::ptr_eq(&self.inner, &other.inner)
    }
}

impl<T> Eq for Handle<T> {}

impl<T> fmt::Debug for Handle<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}({})", type_name::<T>(), self.id().inner)
    }
}

/// Weak counterpart of the Handle. Doesn't keep the resource alive. Compared by pointer
pub struct WeakHandle<T> {
    inner: Weak<Id<T>>,
}

impl<T> WeakHandle<T> {
    /// Try to upgrade the weak handle to a strong one, if it's still alive.
    pub fn upgrade(&self) -> Option<Handle<T>> {
        self.inner.upgrade().map(|inner| Handle { inner })
    }

    /// Returns `true` if the resource is dead. Any subsequent calls to `upgrade()` are guaranteed
    /// to return `None`.
    pub fn is_dead(&self) -> bool {
        self.upgrade().is_none()
    }
}

impl<T> Clone for WeakHandle<T> {
    fn clone(&self) -> WeakHandle<T> {
        WeakHandle {
            inner: self.inner.clone(),
        }
    }
}

impl<T> PartialEq for WeakHandle<T> {
    fn eq(&self, other: &WeakHandle<T>) -> bool {
        Weak::ptr_eq(&self.inner, &other.inner)
    }
}

impl<T> Eq for WeakHandle<T> {}

impl<T> fmt::Debug for WeakHandle<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(id) = self.inner.upgrade() {
            write!(f, "{}({})", type_name::<T>(), id.inner)
        } else {
            write!(f, "{}(dead)", type_name::<T>())
        }
    }
}

/// Trait for types which can be viewed as an `Id<A>`
pub trait AsAssetId<A> {
    /// Perform the conversion
    fn as_asset_id(self) -> Id<A>;
}

impl<A> AsAssetId<A> for Id<A> {
    fn as_asset_id(self) -> Id<A> {
        self
    }
}

impl<A> AsAssetId<A> for &Handle<A> {
    fn as_asset_id(self) -> Id<A> {
        self.id()
    }
}
