use std::path::{Path, PathBuf};

use anyhow::Error;
use fxhash::FxHashSet;

/// Progress counter
#[derive(Default, Debug)]
pub struct ProgressCounter {
    total: u32,
    done: u32,
    loading: FxHashSet<PathBuf>,
    errors: Vec<(PathBuf, Error)>,
}

impl ProgressCounter {
    /// Create a new progress counter
    pub fn new() -> ProgressCounter {
        Default::default()
    }

    /// Schedule loading
    pub fn schedule_loading(&mut self) {
        self.total += 1;
    }

    /// Start loading a file
    pub fn start_loading(&mut self, path: PathBuf) {
        self.loading.insert(path);
    }

    /// Finish loading a file
    pub fn finish_loading(&mut self, path: &PathBuf) {
        self.done += 1;
        self.loading.remove(path);
    }

    /// Add an error
    pub fn add_error(&mut self, path: PathBuf, error: Error) {
        self.errors.push((path, error));
    }

    pub fn num_total(&self) -> u32 {
        self.total
    }

    pub fn num_done(&self) -> u32 {
        self.done
    }

    pub fn percentage(&self) -> u8 {
        (self.done * 100)
            .checked_div(self.total)
            .unwrap_or(100)
            .min(100) as u8
    }

    pub fn iter_loading(&self) -> impl Iterator<Item = &Path> + '_ {
        self.loading.iter().map(|v| &**v)
    }

    pub fn iter_errors(&self) -> impl Iterator<Item = (&Path, &Error)> + '_ {
        self.errors.iter().map(|(path, error)| (&**path, error))
    }
}
