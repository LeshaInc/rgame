use legion::{component, Entity, World};
use nalgebra::{Point3, UnitQuaternion, Vector2, Vector3};
use ncollide3d::query::ray_toi_with_plane;

use crate::assets::{Assets, Handle};
use crate::graphics::draw3d::{AnimationMix, Camera, Model};
use crate::world::{Position, Rotation};
use crate::{DeltaTime, Input, ScheduleBuilder};

pub struct Velocity(pub Vector3<f32>);

pub struct Character {
    forw_speed: f32,
    back_speed: f32,
    side_speed: f32,
    direction: Vector3<f32>,
}

impl Character {
    pub fn new() -> Character {
        Character {
            forw_speed: 6.0,
            back_speed: 2.0,
            side_speed: 3.0,
            direction: Vector3::zeros(),
        }
    }

    pub fn spawn(self, world: &mut World, assets: &mut Assets, pos: Point3<f32>) -> Entity {
        let model: Handle<Model> = assets.load("models/character.json");
        world.push((
            self,
            Position(pos),
            Rotation(UnitQuaternion::identity()),
            Velocity(Vector3::zeros()),
            model,
            vec![AnimationMix::new()],
            AnimationManager::new(),
        ))
    }
}

#[legion::system(for_each)]
fn controls(
    #[resource] input: &Input,
    #[resource] camera: &Camera,
    character: &mut Character,
    position: &Position,
    velocity: &mut Velocity,
) {
    let ray = camera.raycast(input.ndc_cursor_pos());
    let toi = ray_toi_with_plane(&Point3::origin(), &Vector3::z(), &ray).unwrap_or(0.0);
    let mut target = ray.point_at(toi);
    target.z = 0.0;
    let dir = (target - position.0).normalize();
    character.direction = dir;

    let motion_x = input.get_axis("character.motion.x");
    let motion_y = input.get_axis("character.motion.y");
    let motion_unorm = Vector2::new(motion_x, motion_y);
    let motion = match motion_unorm.try_normalize(1e-3) {
        Some(v) => v,
        None => {
            velocity.0 = Vector3::zeros();
            return;
        }
    };

    let dir = dir.xy().normalize();
    let side = Vector2::new(-dir.y, dir.x);

    let cos = motion.dot(&dir);
    let sin = motion.dot(&side);

    let par_speed = if cos >= 0.0 {
        character.forw_speed
    } else {
        character.back_speed
    };

    let vel = if (character.side_speed * cos / sin).abs() < character.forw_speed {
        dir * character.side_speed * cos / sin + side * character.side_speed
    } else {
        dir * par_speed + side * par_speed * sin / cos
    };

    velocity.0 = motion_unorm.component_mul(&vel.abs()).push(0.0);
}

#[legion::system(for_each)]
#[filter(component::<Character>())]
fn camera(#[resource] camera: &mut Camera, position: &Position) {
    camera.set_focus(position.0);
    camera.set_yaw(180f32.to_radians());
}

#[legion::system(for_each)]
#[filter(component::<Character>())]
fn integrate_velocity(
    #[resource] dt: &DeltaTime,
    velocity: &mut Velocity,
    position: &mut Position,
) {
    position.0 += velocity.0 * dt.0;
}

struct AnimationManager {
    state: &'static str,
    prev_state: &'static str,
    time: f32,
    prev_time: f32,
    trans_dur: f32,
    alpha: f32,
}

impl AnimationManager {
    fn new() -> AnimationManager {
        AnimationManager {
            state: "idle",
            prev_state: "",
            time: 0.0,
            prev_time: 0.0,
            trans_dur: 0.0,
            alpha: 1.0,
        }
    }

    fn transition(&mut self, new_state: &'static str, dur: f32) {
        self.prev_state = self.state;
        self.state = new_state;
        self.trans_dur = dur;
        self.alpha = 0.0;
        self.prev_time = self.time;
        self.time = 0.0;
    }

    fn advance(&mut self, dt: f32) {
        self.time += dt;
        self.alpha += dt / self.trans_dur;
        self.alpha = self.alpha.min(1.0);
    }

    fn to_mix(&self) -> AnimationMix {
        let mut mix = AnimationMix::new().with(self.state, self.time, self.alpha);

        if self.alpha < 0.99 {
            mix = mix.with(self.prev_state, self.prev_time, 1.0 - self.alpha);
        }

        mix
    }
}

#[legion::system(for_each)]
fn animate(
    #[resource] dt: &DeltaTime,
    character: &Character,
    manager: &mut AnimationManager,
    animations: &mut Vec<AnimationMix>,
    velocity: &Velocity,
    rotation: &mut Rotation,
) {
    let vel = velocity.0.magnitude_squared();
    let is_moving = vel > 0.1;

    let side = character.direction.cross(&Vector3::z()).normalize();
    let dot = side.dot(&velocity.0.normalize());
    let angle = dot.abs().acos().to_degrees();
    let is_strafe = angle < 45.0;
    let is_strafe_left = is_strafe && dot <= 0.0;
    let is_strafe_right = is_strafe && dot > 0.0;
    let is_backward = !is_strafe && velocity.0.dot(&character.direction) < 0.0;
    let is_forward = !is_strafe && !is_backward;

    let turn_duration = 24.0 / 60.0;

    match manager.state {
        _ if manager.state != "running" && is_moving && is_forward => {
            manager.transition("running", 0.1)
        }
        _ if manager.state != "walking_backwards" && is_moving && is_backward => {
            manager.transition("walking_backwards", 0.1)
        }
        _ if manager.state != "strafe_left" && is_moving && is_strafe_left => {
            manager.transition("strafe_left", 0.1)
        }
        _ if manager.state != "strafe_right" && is_moving && is_strafe_right => {
            manager.transition("strafe_right", 0.1)
        }
        _ if is_moving => {
            rotation.0 = UnitQuaternion::face_towards(&-character.direction, &Vector3::z())
                * UnitQuaternion::from_euler_angles(90f32.to_radians(), 0.0, 180f32.to_radians());
        }
        "idle" if manager.alpha > 0.99 => {
            let cur_direction = rotation.0.transform_vector(&-Vector3::y());
            let tgt_direction = character.direction;
            let cos = cur_direction.dot(&tgt_direction);
            let sin = cur_direction.cross(&Vector3::z()).dot(&tgt_direction);
            let angle = sin.atan2(cos).to_degrees();
            if angle < -60.0 {
                manager.transition("turn_left", 0.05);
            } else if angle > 60.0 {
                manager.transition("turn_right", 0.05);
            }
        }
        "turn_right" if manager.time >= turn_duration => {
            manager.time = 0.0;
            manager.transition("idle", 0.05);
            rotation.0 =
                rotation.0 * UnitQuaternion::from_euler_angles(0.0, 0.0, -90f32.to_radians());
        }
        "turn_left" if manager.time >= turn_duration => {
            manager.time = 0.0;
            manager.transition("idle", 0.05);
            rotation.0 =
                rotation.0 * UnitQuaternion::from_euler_angles(0.0, 0.0, 90f32.to_radians());
        }
        "running" | "walking_backwards" | "strafe_left" | "strafe_right" if !is_moving => {
            manager.transition("idle", 0.1)
        }
        _ => {}
    }

    animations[0] = manager.to_mix();
    manager.advance(dt.0);
}

pub fn character_bundle(schedule: &mut ScheduleBuilder) {
    schedule.on_stage("UPDATE", |stage| {
        stage
            .add_system(controls_system())
            .add_system(integrate_velocity_system())
            .add_system(camera_system())
            .add_system(animate_system());
    });
}
