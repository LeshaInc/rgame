pub mod atlas;
pub mod draw3d;

mod context;
mod framebuffers;
mod texture_storage;
mod uniforms;
mod util;

mod settings;
pub use self::settings::GraphicsSettings;

mod bundle;
pub use self::bundle::graphics_bundle;

mod texture;
pub use self::texture::{PngLoader, Texture, TextureData, TextureFormat};

use std::path::PathBuf;

use anyhow::{anyhow, Context, Result};
use futures::executor::LocalPool;
use futures::task::LocalSpawnExt;
use winit::dpi::PhysicalSize;
use winit::window::Window;

use self::atlas::{AtlasEntryEncoder, TextureAtlas};
use self::context::DrawCtx;
use self::draw3d::{Draw3D, DrawList3D};
use self::framebuffers::Framebuffers;
use self::texture_storage::TextureStorage;
use self::uniforms::UniformBuffer;
use self::util::{load_shader, view_as_bytes};
use crate::assets::Assets;

pub struct Graphics {
    settings: GraphicsSettings,
    resolution: PhysicalSize<u32>,
    device: wgpu::Device,
    queue: wgpu::Queue,
    surface: wgpu::Surface,
    swap_chain: wgpu::SwapChain,
    belt: wgpu::util::StagingBelt,
    shaders_path: PathBuf,
    framebuffers: Framebuffers,
    local_pool: LocalPool,
    frame_idx: u64,
    texture_storage: TextureStorage,
    draw3d: Draw3D,
}

fn create_swap_chain(
    device: &wgpu::Device,
    settings: &GraphicsSettings,
    mut resolution: PhysicalSize<u32>,
    surface: &wgpu::Surface,
) -> wgpu::SwapChain {
    if resolution.width == 0 {
        resolution.width = 1;
    }
    if resolution.height == 0 {
        resolution.height = 1;
    }

    let desc = wgpu::SwapChainDescriptor {
        usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
        format: wgpu::TextureFormat::Bgra8Unorm,
        width: resolution.width,
        height: resolution.height,
        present_mode: settings.present_mode,
    };

    device.create_swap_chain(surface, &desc)
}

impl Graphics {
    pub fn new(
        settings: GraphicsSettings,
        shaders_path: PathBuf,
        window: &Window,
    ) -> Result<Graphics> {
        let span = tracing::info_span!("Graphics::new");
        let _guard = span.enter();

        let backends = settings.backend.into();
        let instance = wgpu::Instance::new(backends);

        let surface = unsafe { instance.create_surface(window) };

        let mut chosen_adapter = None;

        if let Some(spec) = &settings.adapter {
            for adapter in instance.enumerate_adapters(backends) {
                if spec.matches_info(&adapter.get_info()) {
                    chosen_adapter = Some(adapter);
                    break;
                }
            }
        };

        let adapter_preferences = wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::HighPerformance,
            compatible_surface: Some(&surface),
        };

        let adapter = match chosen_adapter {
            Some(v) => v,
            None => futures::executor::block_on(instance.request_adapter(&adapter_preferences))
                .ok_or_else(|| anyhow!("No video adapters found"))?,
        };

        let info = adapter.get_info();
        tracing::info!(
            name = info.name.as_str(),
            device = info.device,
            "type" = ?info.device_type,
            backend = ?info.backend,
            "Selected video adapter"
        );

        let device_desc = wgpu::DeviceDescriptor {
            features: Self::min_features(),
            limits: Self::min_limits(),
            ..Default::default()
        };

        let (device, queue) =
            futures::executor::block_on(adapter.request_device(&device_desc, None))
                .context("Cannot open device")?;

        let resolution = window.inner_size();
        let swap_chain = create_swap_chain(&device, &settings, resolution, &surface);

        let draw3d = Draw3D::new(&device, &queue, &shaders_path)?;

        let mut framebuffers = Framebuffers::new(resolution);
        framebuffers.register(
            "DepthStencil",
            wgpu::TextureFormat::Depth24PlusStencil8,
            wgpu::TextureUsage::OUTPUT_ATTACHMENT,
            1.0,
        );

        framebuffers.register(
            "ColorLinear",
            wgpu::TextureFormat::Bgra8Unorm,
            wgpu::TextureUsage::OUTPUT_ATTACHMENT | wgpu::TextureUsage::SAMPLED,
            1.0,
        );

        framebuffers.register(
            "ColorSRGB",
            wgpu::TextureFormat::Bgra8Unorm,
            wgpu::TextureUsage::OUTPUT_ATTACHMENT | wgpu::TextureUsage::SAMPLED,
            1.0,
        );

        framebuffers.register(
            "SMAAEdges",
            wgpu::TextureFormat::Bgra8Unorm,
            wgpu::TextureUsage::OUTPUT_ATTACHMENT | wgpu::TextureUsage::SAMPLED,
            1.0,
        );

        framebuffers.register(
            "SMAABlend",
            wgpu::TextureFormat::Bgra8Unorm,
            wgpu::TextureUsage::OUTPUT_ATTACHMENT | wgpu::TextureUsage::SAMPLED,
            1.0,
        );

        Ok(Graphics {
            settings,
            resolution,
            device,
            queue,
            surface,
            swap_chain,
            belt: wgpu::util::StagingBelt::new(1024 * 1024),
            shaders_path,
            framebuffers,
            local_pool: LocalPool::new(),
            frame_idx: 0,
            texture_storage: TextureStorage::default(),
            draw3d,
        })
    }

    pub fn settings(&self) -> &GraphicsSettings {
        &self.settings
    }

    fn min_limits() -> wgpu::Limits {
        wgpu::Limits {
            max_push_constant_size: 16,
            ..Default::default()
        }
    }

    fn min_features() -> wgpu::Features {
        wgpu::Features::PUSH_CONSTANTS
    }

    pub fn draw(
        &mut self,
        window: &Window,
        assets: &mut Assets,
        list: &mut DrawList3D,
    ) -> Result<()> {
        let span = tracing::info_span!("Graphics::draw", frame = self.frame_idx);
        let _guard = span.enter();

        self.update_size(window);
        self.framebuffers.maintain(&self.device, self.resolution);

        let frame = self.swap_chain.get_current_frame().unwrap().output;
        self.framebuffers.set_swapchain_texture(frame);

        let desc = wgpu::CommandEncoderDescriptor { label: None };
        let mut encoder = self.device.create_command_encoder(&desc);

        encoder.push_debug_group("Upload textures");
        self.texture_storage
            .upload(&self.device, &mut encoder, assets);
        encoder.pop_debug_group();

        let mut ctx = DrawCtx {
            device: &self.device,
            queue: &self.queue,
            shaders_path: &self.shaders_path,
            encoder: &mut encoder,
            framebuffers: &self.framebuffers,
            belt: &mut self.belt,
            textures: &self.texture_storage,
            resolution: self.resolution,
        };

        ctx.encoder.push_debug_group("Draw3D");
        self.draw3d.draw(&mut ctx, assets, list)?;
        ctx.encoder.pop_debug_group();

        self.belt.finish();
        self.queue.submit(Some(encoder.finish()));

        self.framebuffers.take_swapchain_texture();

        self.device.poll(wgpu::Maintain::Poll);

        self.texture_storage.cleanup(assets);

        self.local_pool
            .spawner()
            .spawn_local(self.belt.recall())
            .unwrap();

        self.local_pool.run_until_stalled();

        self.frame_idx += 1;
        Ok(())
    }

    fn update_size(&mut self, window: &Window) {
        let res = window.inner_size();
        if res == self.resolution {
            return;
        }

        tracing::debug!(new_size = ?res, "Resizing window");

        self.resolution = res;
        self.recreate_swapchain();
    }

    fn recreate_swapchain(&mut self) {
        self.swap_chain =
            create_swap_chain(&self.device, &self.settings, self.resolution, &self.surface);
    }

    pub fn change_settings(&mut self, new: GraphicsSettings) {
        let old = &mut self.settings;

        if old.present_mode != new.present_mode {
            old.present_mode = new.present_mode;
            self.recreate_swapchain();
        }
    }
}
