//! Dynamic texture atlas

use std::hash::Hash;

use fxhash::FxHashMap;
use guillotiere::{AllocId, AtlasAllocator, Rectangle, Size};
use wgpu::util::{BufferInitDescriptor, DeviceExt};

pub const MAX_ATLAS_SIZE: u32 = 8192;

#[derive(Debug)]
struct AllocatedEntry {
    id: AllocId,
    rect: Rectangle,
    is_live: bool,
}

#[derive(Debug)]
struct NewEntry<K> {
    key: K,
    id: AllocId,
    rect: Rectangle,
    data_offset: u64,
    data_stride: u32,
}

/// Dynamic texture atlas
pub struct TextureAtlas<K> {
    texture: wgpu::Texture,
    format: wgpu::TextureFormat,
    bytes_per_pixel: u8,
    usage: wgpu::TextureUsage,
    label: String,

    allocator: AtlasAllocator,
    allocated: FxHashMap<K, AllocatedEntry>,
    new_entries: Vec<NewEntry<K>>,
    new_data: Vec<u8>,
    old_size: u32,
}

impl<K> std::fmt::Debug for TextureAtlas<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("TextureAtlas")
            .field("texture", &self.texture)
            .field("format", &self.format)
            .finish()
    }
}

impl<K: Hash + Eq> TextureAtlas<K> {
    /// Create a new texture atlas
    pub fn new(
        device: &wgpu::Device,
        format: wgpu::TextureFormat,
        bytes_per_pixel: u8,
        usage: wgpu::TextureUsage,
        size: u32,
        label: Option<&str>,
    ) -> TextureAtlas<K> {
        let label = label.unwrap_or("Unnamed").to_owned();

        let tex_label = format!("Texture Atlas ({})", &label);
        let desc = wgpu::TextureDescriptor {
            label: Some(&tex_label),
            size: wgpu::Extent3d {
                width: size,
                height: size,
                depth: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format,
            usage,
        };
        let texture = device.create_texture(&desc);

        TextureAtlas {
            texture,
            format,
            bytes_per_pixel,
            usage,
            label,
            allocator: AtlasAllocator::new(Size::new(size as _, size as _)),
            allocated: FxHashMap::default(),
            new_entries: Vec::new(),
            new_data: Vec::new(),
            old_size: size,
        }
    }

    pub fn create_view(&self) -> wgpu::TextureView {
        let label = format!("Texture Atlas ({})/View", self.label);
        let desc = wgpu::TextureViewDescriptor {
            label: Some(&label),
            format: Some(self.format),
            dimension: Some(wgpu::TextureViewDimension::D2),
            aspect: wgpu::TextureAspect::All,
            base_mip_level: 0,
            level_count: None,
            base_array_layer: 0,
            array_layer_count: None,
        };
        self.texture.create_view(&desc)
    }

    /// Get rect of the entry, or None if it doesn't exist
    pub fn get_rect(&self, key: &K) -> Option<Rectangle> {
        self.allocated.get(key).map(|e| e.rect)
    }

    /// Get rect of the entry in normalized floating coordinates, or None if it doesn't exist
    pub fn get_rect_f32(&self, key: &K) -> Option<[f32; 4]> {
        let rect = self.get_rect(key)?;
        let size = self.allocator.size().cast::<f32>();
        Some([
            (rect.min.x as f32) / size.width,
            (rect.min.y as f32) / size.height,
            (rect.width() as f32) / size.width,
            (rect.height() as f32) / size.height,
        ])
    }

    /// Add an entry to the texture atlas. If it is already uploaded, mark it as live, otherwise call
    /// `data_fn` to get its pixel data
    pub fn add<F>(&mut self, key: K, size: (u32, u32), data_fn: F) -> bool
    where
        F: for<'a> FnOnce(AtlasEntryEncoder<'a>),
    {
        // check if there is entry with the same key

        match self.allocated.get_mut(&key) {
            Some(e) if e.rect.size() == Size::new(size.0 as _, size.1 as _) => {
                // sizes match, keep the old image
                e.is_live = true;
                return true;
            }
            Some(e) => {
                // size mismatch, update the image
                self.allocator.deallocate(e.id);
                self.allocated.remove(&key);
            }
            _ => {}
        }

        // allocate space for image data

        let start = self.new_data.len();
        let stride = 1
            + (((u32::from(self.bytes_per_pixel) * size.0) - 1)
                | (wgpu::COPY_BYTES_PER_ROW_ALIGNMENT - 1)); // align up
        let len = (stride as usize) * (size.1 as usize);
        let end = start + len;
        self.new_data.extend((0..len).map(|_| 0));

        // get image data

        let encoder = AtlasEntryEncoder {
            bytes: &mut self.new_data[start..end],
            stride: stride as usize,
            width: (u32::from(self.bytes_per_pixel) * size.0) as usize,
            height: size.1 as _,
        };

        data_fn(encoder);

        // allocate rectangle in the atlas

        let alloc = loop {
            let res = self.allocator.allocate(Size::new(size.0 as _, size.1 as _));
            match res {
                Some(alloc) => break alloc,
                None => {
                    let size = self.size() * 2;
                    if size >= MAX_ATLAS_SIZE {
                        return false;
                    }

                    self.allocator.grow(Size::new(size as _, size as _));
                }
            }
        };

        self.new_entries.push(NewEntry {
            key,
            id: alloc.id,
            rect: alloc.rectangle,
            data_offset: start as u64,
            data_stride: stride,
        });

        true
    }

    /// Get atlas size
    pub fn size(&self) -> u32 {
        self.allocator.size().width as _
    }

    /// Deallocate one allocated entry
    pub fn deallocate(&mut self, key: &K) {
        let id = match self.allocated.get(key) {
            Some(entry) => entry.id,
            _ => return,
        };
        self.allocator.deallocate(id);
    }

    /// Deallocate dead entries
    pub fn deallocate_dead(&mut self) {
        let allocator = &mut self.allocator;
        self.allocated.retain(|_, entry| {
            if !entry.is_live {
                allocator.deallocate(entry.id);
            }
            entry.is_live
        });

        // mark all as dead
        for entry in self.allocated.values_mut() {
            entry.is_live = false;
        }
    }

    /// Upload new entries to the texture atlas, growing it if necessary
    pub fn flush(&mut self, device: &wgpu::Device, encoder: &mut wgpu::CommandEncoder) {
        if self.new_entries.is_empty() {
            return;
        }

        self.resize(device, encoder);

        let label = format!("Texture Atlas ({})/Staging Buffer", &self.label);
        let desc = BufferInitDescriptor {
            label: Some(&label),
            contents: &self.new_data,
            usage: wgpu::BufferUsage::COPY_SRC,
        };
        let buffer = device.create_buffer_init(&desc);

        for entry in self.new_entries.drain(..) {
            self.allocated.insert(
                entry.key,
                AllocatedEntry {
                    id: entry.id,
                    rect: entry.rect,
                    is_live: false,
                },
            );

            let src = wgpu::BufferCopyView {
                buffer: &buffer,
                layout: wgpu::TextureDataLayout {
                    offset: entry.data_offset,
                    bytes_per_row: entry.data_stride,
                    rows_per_image: 0,
                },
            };

            let size = wgpu::Extent3d {
                width: entry.rect.width() as _,
                height: entry.rect.height() as _,
                depth: 1,
            };

            let dst = wgpu::TextureCopyView {
                texture: &self.texture,
                mip_level: 0,
                origin: wgpu::Origin3d {
                    x: entry.rect.min.x as _,
                    y: entry.rect.min.y as _,
                    z: 0,
                },
            };

            encoder.copy_buffer_to_texture(src, dst, size);
        }

        self.new_data.clear();
    }

    fn resize(&mut self, device: &wgpu::Device, encoder: &mut wgpu::CommandEncoder) {
        let old_size = self.old_size;
        let new_size = self.size();

        if old_size == new_size {
            return;
        }

        let label = format!("Texture Atlas ({})", &self.label);
        let desc = wgpu::TextureDescriptor {
            label: Some(&label),
            size: wgpu::Extent3d {
                width: new_size as _,
                height: new_size as _,
                depth: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: self.format,
            usage: self.usage,
        };
        let new_texture = device.create_texture(&desc);

        let src = wgpu::TextureCopyView {
            texture: &self.texture,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
        };

        let dst = wgpu::TextureCopyView {
            texture: &new_texture,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
        };

        let size = wgpu::Extent3d {
            width: old_size as _,
            height: old_size as _,
            depth: 1,
        };

        encoder.copy_texture_to_texture(src, dst, size);

        self.texture = new_texture;
    }
}

/// Encodes texture atlas data. Index it with a Y value to get a row
pub struct AtlasEntryEncoder<'a> {
    bytes: &'a mut [u8],
    stride: usize,
    width: usize,
    #[allow(dead_code)] // TODO
    height: usize,
}

impl std::ops::Index<usize> for AtlasEntryEncoder<'_> {
    type Output = [u8];
    fn index(&self, index: usize) -> &[u8] {
        // index = self.height - index - 1;
        &self.bytes[index * self.stride..(index + 1) * self.stride][..self.width]
    }
}

impl std::ops::IndexMut<usize> for AtlasEntryEncoder<'_> {
    fn index_mut(&mut self, index: usize) -> &mut [u8] {
        // index = self.height - index - 1;
        &mut self.bytes[index * self.stride..(index + 1) * self.stride][..self.width]
    }
}
