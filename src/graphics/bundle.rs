use std::path::PathBuf;

use legion::{Resources, World};
use winit::window::Window;

use super::draw3d::{AnimatedMesh, Camera, DrawList3DEncoder, EnvMap, Material, Mesh, Model};
use super::{Graphics, GraphicsSettings, Texture};
use crate::assets::Assets;
use crate::input::Input;
use crate::ScheduleBuilder;

fn init(_world: &mut World, resources: &mut Resources) {
    let mut assets = resources.get_mut::<Assets>().unwrap();
    assets.register::<Mesh>();
    assets.register::<AnimatedMesh>();
    assets.register::<Texture>();
    assets.register::<Material>();
    assets.register::<Model>();
    assets.register::<EnvMap>();
    drop(assets);

    let window = resources.get::<Window>().unwrap();
    let settings = GraphicsSettings::default();
    let shaders_path = PathBuf::from("assets/shaders");
    let graphics = Graphics::new(settings, shaders_path, &window).unwrap();
    drop(window);
    resources.insert(graphics);
    resources.insert(DrawList3DEncoder::default());
}

#[legion::system]
fn update_window_size(#[resource] window: &Window, #[resource] input: &mut Input) {
    input.update_window_size(window.inner_size());
}

#[legion::system]
fn update_aspect(#[resource] input: &Input, #[resource] camera: &mut Camera) {
    camera.set_aspect(input.window_size().x / input.window_size().y);
}

#[legion::system]
fn draw(
    #[resource] graphics: &mut Graphics,
    #[resource] window: &Window,
    #[resource] assets: &mut Assets,
    #[resource] encoder_res: &mut DrawList3DEncoder,
    #[resource] camera: &Camera,
) {
    let mut encoder = std::mem::take(encoder_res);
    encoder.set_camera(*camera);

    let mut list = encoder.finish();

    if let Err(e) = graphics.draw(window, assets, &mut list) {
        tracing::error!("{:?}", e);
    }

    list.clear();
    *encoder_res = DrawList3DEncoder::new(list);
}

pub fn graphics_bundle(schedule: &mut ScheduleBuilder) {
    schedule.add_initializer(init);
    schedule.add_stage_before("UPDATE", "INIT_DRAW");
    schedule.on_stage("INIT_DRAW", |stage| {
        stage
            .add_system(update_window_size_system())
            .add_system(update_aspect_system());
    });
    schedule.add_stage_after("DRAW", "DRAW_FLUSH");
    schedule.on_stage("DRAW_FLUSH", |stage| {
        stage.add_thread_local(draw_system());
    });
}
