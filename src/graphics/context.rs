use std::path::Path;

use winit::dpi::PhysicalSize;

use super::{Framebuffers, TextureStorage};

pub struct DrawCtx<'a> {
    pub device: &'a wgpu::Device,
    pub queue: &'a wgpu::Queue,
    pub encoder: &'a mut wgpu::CommandEncoder,
    pub shaders_path: &'a Path,
    pub framebuffers: &'a Framebuffers,
    pub belt: &'a mut wgpu::util::StagingBelt,
    pub textures: &'a TextureStorage,
    pub resolution: PhysicalSize<u32>,
}
