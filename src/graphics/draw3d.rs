mod camera;
pub use self::camera::Camera;

mod encoder;
pub use self::encoder::{AnimationMix, DrawList3D, DrawList3DEncoder};

mod envmap;
pub use self::envmap::{EnvMap, EnvMapData, EnvMapLoader};

mod material;
pub use self::material::{Material, MaterialProperties, MaterialProperty};

mod mesh;
pub use self::mesh::{
    AnimatedMesh, AnimatedMeshLoader, Animation, JointId, JointTransform, Mesh, MeshData,
    MeshLoader, Pose,
};

mod model;
pub use self::model::{Model, ModelLoader, ModelMesh, ModelPart};

mod vertex;
pub use self::vertex::{AnimatedMeshVertex, Instance, MeshVertex};

mod context;
mod envmap_storage;
mod globals;
mod mesh_storage;
mod passes;
mod upload;

use std::path::Path;

use anyhow::Result;

use self::context::{DrawCtx3D, InitCtx3D};
use self::encoder::AnimationSource;
use self::envmap_storage::EnvMapStorage;
use self::globals::{GlobalBindings, GlobalUniforms};
use self::material::MaterialRegistry;
use self::mesh_storage::MeshStorage;
use self::passes::{ForwardPass, GammaCorrectionPass, SmaaPass};
use self::upload::UploadedDrawList;
use self::vertex::make_vbuf_desc;
use super::DrawCtx;
use crate::assets::Assets;

pub struct Draw3D {
    meshes: MeshStorage,
    envmaps: EnvMapStorage,
    globals: GlobalBindings,
    materials: MaterialRegistry,

    brdf_lut: wgpu::Texture,

    forward_pass: ForwardPass,
    gamma_pass: GammaCorrectionPass,
    smaa_pass: SmaaPass,
}

impl Draw3D {
    pub fn new(device: &wgpu::Device, queue: &wgpu::Queue, shaders_path: &Path) -> Result<Draw3D> {
        let meshes = MeshStorage::default();
        let envmaps = EnvMapStorage::default();
        let globals = GlobalBindings::new(&device);

        let ctx = InitCtx3D {
            device,
            queue,
            shaders_path,
            globals: &globals,
        };

        let materials = MaterialRegistry::new(device);
        let forward_pass = ForwardPass::default();
        let gamma_pass = GammaCorrectionPass::new(&ctx)?;
        let smaa_pass = SmaaPass::new(&ctx)?;

        Ok(Draw3D {
            meshes,
            envmaps,
            globals,
            materials,

            brdf_lut: create_brdf_lut(device, queue),

            forward_pass,
            gamma_pass,
            smaa_pass,
        })
    }

    pub fn draw(
        &mut self,
        ctx: &mut DrawCtx<'_>,
        assets: &mut Assets,
        list: &mut DrawList3D,
    ) -> Result<()> {
        list.resolve_models(assets);

        self.materials
            .upload(&ctx.device, ctx.shaders_path, &self.globals, assets)?;
        self.meshes.upload(&ctx.device, assets);
        self.meshes.cleanup(assets);

        self.envmaps.upload(&ctx.device, &ctx.queue, assets);
        self.envmaps.cleanup(assets);

        let camera = list.camera();
        self.globals.set_uniforms(GlobalUniforms::new(camera));

        ctx.encoder.push_debug_group("Upload globals");
        self.globals
            .upload(&ctx.device, &mut ctx.encoder, &mut ctx.belt);
        ctx.encoder.pop_debug_group();

        let mut ctx = DrawCtx3D {
            device: &ctx.device,
            queue: &ctx.queue,
            shaders_path: &ctx.shaders_path,
            encoder: &mut ctx.encoder,
            framebuffers: &ctx.framebuffers,
            resolution: ctx.resolution,
            belt: &mut ctx.belt,
            textures: &ctx.textures,
            meshes: &self.meshes,
            envmaps: &self.envmaps,
            brdf_lut: &self.brdf_lut,
            globals: &self.globals,
            materials: &self.materials,
            assets: &assets,
        };

        ctx.encoder.push_debug_group("Upload draw list");
        let uploaded_list = UploadedDrawList::create(&mut ctx, list, "Main");
        ctx.encoder.pop_debug_group();

        ctx.encoder.push_debug_group("Forward pass");
        self.forward_pass.run(&mut ctx, &uploaded_list);
        ctx.encoder.pop_debug_group();

        ctx.encoder.push_debug_group("Gamma correction pass");
        self.gamma_pass.run(&mut ctx, "ColorLinear", "ColorSRGB");
        ctx.encoder.pop_debug_group();

        ctx.encoder.push_debug_group("SMAA pass");
        self.smaa_pass.run(&mut ctx);
        ctx.encoder.pop_debug_group();

        Ok(())
    }
}

fn create_brdf_lut(device: &wgpu::Device, queue: &wgpu::Queue) -> wgpu::Texture {
    let size = wgpu::Extent3d {
        width: 512,
        height: 512,
        depth: 1,
    };

    let brdf_lut = device.create_texture(&wgpu::TextureDescriptor {
        label: Some("BRDF LUT"),
        size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Rg32Float,
        usage: wgpu::TextureUsage::SAMPLED | wgpu::TextureUsage::COPY_DST,
    });

    let view = wgpu::TextureCopyView {
        texture: &brdf_lut,
        mip_level: 0,
        origin: wgpu::Origin3d::ZERO,
    };

    let data = include_bytes!("draw3d/brdf-lut.raw");

    let layout = wgpu::TextureDataLayout {
        offset: 0,
        bytes_per_row: 512 * 8,
        rows_per_image: 512,
    };

    queue.write_texture(view, data, layout, size);

    brdf_lut
}
