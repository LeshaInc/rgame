use nalgebra::{
    IsometryMatrix3, Matrix4, Orthographic3, Point2, Point3, Projective3, UnitQuaternion, Vector3,
};
use ncollide3d::query::Ray;

#[derive(Clone, Copy, Debug)]
pub struct Camera {
    focus: Point3<f32>,
    distance: f32,
    yaw: f32,
    pitch: f32,
    scale: f32,
    znear: f32,
    zfar: f32,
    aspect: f32,

    direction: Vector3<f32>,
    position: Point3<f32>,
    view: IsometryMatrix3<f32>,
    inv_view: IsometryMatrix3<f32>,
    projection: Projective3<f32>,
    inv_projection: Projective3<f32>,
}

impl Camera {
    pub fn new() -> Camera {
        Camera::default()
    }

    pub fn focus(&self) -> Point3<f32> {
        self.focus
    }

    pub fn set_focus(&mut self, focus: Point3<f32>) {
        self.focus = focus;
        self.update_position();
        self.update_view();
    }

    pub fn distance(&self) -> f32 {
        self.distance
    }

    pub fn set_distance(&mut self, distance: f32) {
        self.distance = distance;
        self.update_position();
        self.update_view();
    }

    pub fn yaw(&self) -> f32 {
        self.yaw
    }

    pub fn set_yaw(&mut self, yaw: f32) {
        self.yaw = yaw;
        self.update_direction();
        self.update_view();
    }

    pub fn pitch(&self) -> f32 {
        self.pitch
    }

    pub fn set_pitch(&mut self, pitch: f32) {
        self.pitch = pitch;
        self.update_direction();
        self.update_view();
    }

    pub fn scale(&self) -> f32 {
        self.scale
    }

    pub fn set_scale(&mut self, scale: f32) {
        self.scale = scale;
        self.update_projection();
    }

    pub fn znear(&self) -> f32 {
        self.znear
    }

    pub fn set_znear(&mut self, znear: f32) {
        self.znear = znear;
        self.update_projection();
    }

    pub fn zfar(&self) -> f32 {
        self.zfar
    }

    pub fn set_zfar(&mut self, zfar: f32) {
        self.zfar = zfar;
        self.update_projection();
    }

    pub fn aspect(&self) -> f32 {
        self.aspect
    }

    pub fn set_aspect(&mut self, aspect: f32) {
        self.aspect = aspect;
        self.update_projection();
    }

    pub fn position(&self) -> Point3<f32> {
        self.position
    }

    pub fn direction(&self) -> Vector3<f32> {
        self.direction
    }

    pub fn view(&self) -> Matrix4<f32> {
        self.view.to_homogeneous()
    }

    pub fn inv_view(&self) -> Matrix4<f32> {
        self.inv_view.to_homogeneous()
    }

    pub fn projection(&self) -> Matrix4<f32> {
        self.projection.to_homogeneous()
    }

    pub fn inv_projection(&self) -> Matrix4<f32> {
        self.inv_projection.to_homogeneous()
    }

    pub fn inv_view_projection(&self) -> Matrix4<f32> {
        (self.inv_view * self.inv_projection).to_homogeneous()
    }

    fn update_direction(&mut self) {
        let rotation = UnitQuaternion::from_euler_angles(0.0, self.pitch, self.yaw);
        self.direction = (rotation * Vector3::new(0.0, 0.0, -1.0)).normalize();
    }

    fn update_position(&mut self) {
        self.position = self.focus - self.distance * self.direction;
    }

    fn update_view(&mut self) {
        self.view = IsometryMatrix3::look_at_rh(&self.position(), &self.focus, &Vector3::z());
        self.inv_view = self.view.inverse();
    }

    fn update_projection(&mut self) {
        let height = self.scale;
        let width = height * self.aspect;
        let ortho = Orthographic3::new(-width, width, -height, height, self.znear, self.zfar);

        // map -1..1 z-range to 0..1
        let fix_z =
            Matrix4::new_translation(&Vector3::new(0.0, 0.0, 0.5)) * Matrix4::new_scaling(0.5);

        self.projection = Projective3::from_matrix_unchecked(fix_z * ortho.to_homogeneous());
        self.inv_projection = self.projection.inverse();
    }

    pub fn raycast(&self, ncd_pos: Point2<f32>) -> Ray<f32> {
        let ncd_pos = Point3::new(ncd_pos.x, ncd_pos.y, 0.0);
        let origin = self.inv_view_projection().transform_point(&ncd_pos);
        Ray::new(origin, self.direction)
    }
}

impl Default for Camera {
    fn default() -> Camera {
        let mut camera = Camera {
            focus: Point3::origin(),
            distance: 100.0,
            yaw: 0.0,
            pitch: 45f32.to_radians(),
            scale: 4.0,
            znear: 1e-3,
            zfar: 1e3,
            aspect: 1.0,
            direction: Vector3::zeros(),
            position: Point3::origin(),
            view: IsometryMatrix3::identity(),
            inv_view: IsometryMatrix3::identity(),
            projection: Projective3::identity(),
            inv_projection: Projective3::identity(),
        };

        camera.update_direction();
        camera.update_position();
        camera.update_view();
        camera.update_projection();

        camera
    }
}
