use std::path::Path;

use anyhow::Result;
use winit::dpi::PhysicalSize;

use super::{EnvMapStorage, GlobalBindings, MaterialRegistry, MeshStorage};
use crate::assets::Assets;
use crate::graphics::{load_shader, Framebuffers, TextureStorage};

pub struct InitCtx3D<'a> {
    pub device: &'a wgpu::Device,
    pub queue: &'a wgpu::Queue,
    pub shaders_path: &'a Path,
    pub globals: &'a GlobalBindings,
}

impl InitCtx3D<'_> {
    pub fn load_shader(&self, path: &str) -> Result<wgpu::ShaderModule> {
        load_shader(&self.device, &self.shaders_path, path)
    }
}

pub struct DrawCtx3D<'a> {
    pub device: &'a wgpu::Device,
    pub queue: &'a wgpu::Queue,
    pub shaders_path: &'a Path,
    pub encoder: &'a mut wgpu::CommandEncoder,
    pub framebuffers: &'a Framebuffers,
    pub resolution: PhysicalSize<u32>,
    pub belt: &'a mut wgpu::util::StagingBelt,
    pub textures: &'a TextureStorage,
    pub meshes: &'a MeshStorage,
    pub envmaps: &'a EnvMapStorage,
    pub brdf_lut: &'a wgpu::Texture,
    pub globals: &'a GlobalBindings,
    pub materials: &'a MaterialRegistry,
    pub assets: &'a Assets,
}
