use std::convert::TryInto;
use std::ops::Deref;
use std::sync::Arc;

use fxhash::FxHashMap;
use rayon::prelude::*;

use super::{
    AnimatedMesh, Camera, EnvMap, Instance, Material, MaterialProperties, Mesh, Model, ModelMesh,
    Pose,
};
use crate::assets::{AsAssetId, Assets, Id};
use crate::graphics::{view_as_bytes, Texture};

#[derive(Debug, Default)]
pub struct DrawList3D {
    material_batches: FxHashMap<Id<Material>, MaterialBatch>,
    models: Vec<ModelInstance>,
    camera: Camera,
    envmap: Option<Id<EnvMap>>,
}

impl DrawList3D {
    pub fn empty() -> DrawList3D {
        DrawList3D::default()
    }

    pub fn clear(&mut self) {
        self.material_batches.clear();
        self.models.clear();
    }

    pub fn iter_material_batches(
        &self,
    ) -> impl Iterator<Item = (Id<Material>, &MaterialBatch)> + '_ {
        self.material_batches
            .iter()
            .map(|(mat_id, batch)| (*mat_id, batch))
    }

    pub fn par_iter_material_batches(
        &self,
    ) -> impl ParallelIterator<Item = (Id<Material>, &MaterialBatch)> + '_ {
        self.material_batches
            .par_iter()
            .map(|(mat_id, batch)| (*mat_id, batch))
    }

    pub fn camera(&self) -> &Camera {
        &self.camera
    }

    pub fn envmap(&self) -> Option<Id<EnvMap>> {
        self.envmap
    }

    pub fn resolve_models(&mut self, assets: &Assets) {
        let mut models = std::mem::take(&mut self.models);
        let list = std::mem::take(self);
        let mut encoder = DrawList3DEncoder::new(list);
        let mut textures = Vec::new();

        for inst in models.drain(..) {
            let mut animations = inst.animations.into_iter();
            let model = assets.get_by_id(inst.model).unwrap();
            for part in &model.parts {
                textures.clear();
                for handle in &part.textures {
                    textures.push(handle.id());
                }

                match &part.mesh {
                    ModelMesh::Regular(mesh) => encoder.mesh(
                        inst.instance,
                        mesh,
                        &part.material,
                        &part.properties,
                        &textures,
                    ),
                    ModelMesh::Animated(mesh) => {
                        encoder.animated_mesh(
                            inst.instance,
                            mesh,
                            &part.material,
                            &part.properties,
                            &textures,
                            animations.next().unwrap(),
                        );
                    }
                }
            }
        }

        *self = encoder.finish();
        self.models = models;
    }
}

#[derive(Debug)]
pub struct ModelInstance {
    pub instance: Instance,
    pub model: Id<Model>,
    pub animations: Vec<AnimationMix>,
}

#[derive(Debug, Default)]
pub struct MaterialBatch {
    regular_meshes: FxHashMap<Id<Mesh>, RegularMeshBatch>,
    animated_meshes: FxHashMap<Id<AnimatedMesh>, AnimatedMeshBatch>,
}

impl MaterialBatch {
    pub fn iter_regular_meshes(&self) -> impl Iterator<Item = (Id<Mesh>, &RegularMeshBatch)> + '_ {
        self.regular_meshes
            .iter()
            .map(|(mesh_id, subbatch)| (*mesh_id, subbatch))
    }

    pub fn iter_animated_meshes(
        &self,
    ) -> impl Iterator<Item = (Id<AnimatedMesh>, &AnimatedMeshBatch)> + '_ {
        self.animated_meshes
            .iter()
            .map(|(mesh_id, subbatch)| (*mesh_id, subbatch))
    }
}

#[derive(Debug)]
pub struct RegularMeshBatch {
    instances: Vec<Instance>,
    params: Vec<u8>,
    textures: Vec<Id<Texture>>,
    textures_per_instance: u8,
}

impl RegularMeshBatch {
    pub fn instances(&self) -> &[Instance] {
        &self.instances
    }

    pub fn params(&self) -> &[u8] {
        &self.params
    }

    pub fn textures(&self) -> &[Id<Texture>] {
        &self.textures
    }

    pub fn textures_per_instance(&self) -> u8 {
        self.textures_per_instance
    }
}

#[derive(Debug)]
pub struct AnimatedMeshBatch {
    regular: RegularMeshBatch,
    animations: Vec<AnimationMix>,
}

impl AnimatedMeshBatch {
    pub fn animations(&self) -> &[AnimationMix] {
        &self.animations
    }
}

impl Deref for AnimatedMeshBatch {
    type Target = RegularMeshBatch;

    fn deref(&self) -> &RegularMeshBatch {
        &self.regular
    }
}

#[derive(Clone, Debug, Default)]
pub struct AnimationMix {
    sources: Vec<(f32, AnimationSource)>,
}

impl AnimationMix {
    pub fn new() -> AnimationMix {
        AnimationMix::default()
    }

    pub fn with(mut self, animation: impl Into<String>, time: f32, alpha: f32) -> AnimationMix {
        self.sources
            .push((alpha, AnimationSource::Bundled(animation.into(), time)));
        self
    }

    pub fn with_pose(mut self, pose: &Arc<Pose>, alpha: f32) -> AnimationMix {
        self.sources
            .push((alpha, AnimationSource::Pose(pose.clone())));
        self
    }

    pub(super) fn sources(&self) -> &[(f32, AnimationSource)] {
        &self.sources
    }
}

#[derive(Clone, Debug)]
pub enum AnimationSource {
    Bundled(String, f32),
    Pose(Arc<Pose>),
}

#[derive(Debug, Default)]
pub struct DrawList3DEncoder {
    list: DrawList3D,
}

impl DrawList3DEncoder {
    pub fn new(list: DrawList3D) -> DrawList3DEncoder {
        DrawList3DEncoder { list }
    }

    pub fn set_camera(&mut self, camera: Camera) {
        self.list.camera = camera;
    }

    pub fn set_envmap(&mut self, envmap: impl AsAssetId<EnvMap>) {
        self.list.envmap = Some(envmap.as_asset_id());
    }

    pub fn model(
        &mut self,
        instance: Instance,
        model: impl AsAssetId<Model>,
        animations: impl Into<Vec<AnimationMix>>,
    ) {
        self.list.models.push(ModelInstance {
            instance,
            model: model.as_asset_id(),
            animations: animations.into(),
        });
    }

    pub fn material(&mut self, material: impl AsAssetId<Material>) -> MaterialBatchEncoder<'_> {
        MaterialBatchEncoder {
            batch: self
                .list
                .material_batches
                .entry(material.as_asset_id())
                .or_default(),
        }
    }

    pub fn mesh(
        &mut self,
        instance: Instance,
        mesh: impl AsAssetId<Mesh>,
        material: impl AsAssetId<Material>,
        props: &MaterialProperties,
        textures: &[Id<Texture>],
    ) {
        self.material(material)
            .mesh(instance, mesh, props, textures);
    }

    pub fn animated_mesh(
        &mut self,
        instance: Instance,
        mesh: impl AsAssetId<AnimatedMesh>,
        material: impl AsAssetId<Material>,
        props: &MaterialProperties,
        textures: &[Id<Texture>],
        animation: AnimationMix,
    ) {
        self.material(material)
            .animated_mesh(instance, mesh, props, textures, animation);
    }

    pub fn finish(self) -> DrawList3D {
        self.list
    }
}

#[derive(Debug)]
pub struct MaterialBatchEncoder<'a> {
    batch: &'a mut MaterialBatch,
}

impl MaterialBatchEncoder<'_> {
    pub fn mesh(
        &mut self,
        instance: Instance,
        mesh: impl AsAssetId<Mesh>,
        props: &MaterialProperties,
        textures: &[Id<Texture>],
    ) {
        encode_regular_mesh(
            self.batch,
            instance,
            mesh.as_asset_id(),
            view_as_bytes(&props.data),
            textures,
        );
    }

    pub fn animated_mesh(
        &mut self,
        instance: Instance,
        mesh: impl AsAssetId<AnimatedMesh>,
        props: &MaterialProperties,
        textures: &[Id<Texture>],
        animation: AnimationMix,
    ) {
        encode_animated_mesh(
            self.batch,
            instance,
            mesh.as_asset_id(),
            view_as_bytes(&props.data),
            textures,
            animation,
        );
    }
}

fn encode_regular_mesh(
    batch: &mut MaterialBatch,
    instance: Instance,
    mesh_id: Id<Mesh>,
    params: &[u8],
    textures: &[Id<Texture>],
) {
    let subbatch = batch
        .regular_meshes
        .entry(mesh_id)
        .or_insert_with(|| RegularMeshBatch {
            instances: Vec::new(),
            params: Vec::new(),
            textures: Vec::new(),
            textures_per_instance: textures.len().try_into().unwrap(),
        });
    subbatch.instances.push(instance);
    subbatch.params.extend_from_slice(params);
    subbatch.textures.extend_from_slice(textures);
}

fn encode_animated_mesh(
    batch: &mut MaterialBatch,
    instance: Instance,
    mesh_id: Id<AnimatedMesh>,
    params: &[u8],
    textures: &[Id<Texture>],
    mix: AnimationMix,
) {
    let subbatch = batch
        .animated_meshes
        .entry(mesh_id)
        .or_insert_with(|| AnimatedMeshBatch {
            regular: RegularMeshBatch {
                instances: Vec::new(),
                params: Vec::new(),
                textures: Vec::new(),
                textures_per_instance: textures.len().try_into().unwrap(),
            },
            animations: Vec::new(),
        });
    subbatch.regular.instances.push(instance);
    subbatch.regular.params.extend_from_slice(params);
    subbatch.regular.textures.extend_from_slice(textures);
    subbatch.animations.push(mix);
}
