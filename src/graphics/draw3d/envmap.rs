use std::path::PathBuf;
use std::str::FromStr;

use std::io::BufReader;

use anyhow::Result;
use image::codecs::hdr::HdrDecoder;
use image::Rgb;

use crate::assets::{Asset, AssetDefaultLoader, AssetReader, SimpleAssetLoader};

#[derive(Clone, Debug)]
pub struct EnvMap {
    pub data: Option<EnvMapData>,
}

#[derive(Clone, Debug)]
pub struct EnvMapData {
    pub sky: (u32, Vec<Rgb<f32>>),
    pub irradiance: (u32, Vec<Rgb<f32>>),
    pub prefilter: (u32, Vec<Vec<Rgb<f32>>>),
}

impl Asset for EnvMap {}

impl AssetDefaultLoader for EnvMap {
    type Loader = EnvMapLoader;
}

#[derive(Default)]
pub struct EnvMapLoader;

impl SimpleAssetLoader<EnvMap> for EnvMapLoader {
    fn load(&self, reader: impl AssetReader) -> Result<EnvMap> {
        let mut tar = tar::Archive::new(reader);

        let mut data = EnvMapData {
            sky: (0, Vec::new()),
            irradiance: (0, Vec::new()),
            prefilter: (0, Vec::new()),
        };

        for entry in tar.entries()? {
            let entry = entry?;
            let path = PathBuf::from(entry.path()?);
            if path.extension().and_then(|v| v.to_str()) != Some("hdr") {
                continue;
            }

            let decoder = HdrDecoder::new(BufReader::new(entry))?;
            let size = decoder.metadata().width;
            let hdr = decoder.read_image_hdr()?;

            let name = path.file_name().and_then(|v| v.to_str()).unwrap();

            if name == "sky.hdr" {
                data.sky = (size / 6, hdr);
            } else if name == "irradiance.hdr" {
                data.irradiance = (size / 6, hdr);
            } else if name.starts_with("prefilter") {
                let mip = usize::from_str(&name[9..10])?;
                if mip == 0 {
                    data.prefilter.0 = size / 6;
                }

                if mip <= data.prefilter.1.len() {
                    data.prefilter.1.resize(mip + 1, Vec::new());
                }

                data.prefilter.1[mip] = hdr;
            }
        }

        Ok(EnvMap { data: Some(data) })
    }
}
