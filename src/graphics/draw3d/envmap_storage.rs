use fxhash::FxHashMap;
use image::{Pixel, Rgb};

use super::{EnvMap, EnvMapData};
use crate::assets::{Assets, Id};
use crate::graphics::view_as_bytes;

#[derive(Debug, Default)]
pub struct EnvMapStorage {
    envmaps: FxHashMap<Id<EnvMap>, UploadedEnvMap>,
}

#[derive(Debug)]
pub struct UploadedEnvMap {
    pub sky: wgpu::Texture,
    pub irradiance: wgpu::Texture,
    pub prefilter: wgpu::Texture,
}

fn upload_cubemap(
    device: &wgpu::Device,
    queue: &wgpu::Queue,
    size: u32,
    source: &[&[Rgb<f32>]],
) -> wgpu::Texture {
    let cubemap = device.create_texture(&wgpu::TextureDescriptor {
        label: None, // TODO
        size: wgpu::Extent3d {
            width: size,
            height: size,
            depth: 6,
        },
        mip_level_count: source.len() as _,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Rgba32Float,
        usage: wgpu::TextureUsage::COPY_DST | wgpu::TextureUsage::SAMPLED,
    });

    for (mip, source) in source.iter().enumerate() {
        let size = size as usize / 2usize.pow(mip as _);

        let view = wgpu::TextureCopyView {
            texture: &cubemap,
            mip_level: mip as _,
            origin: wgpu::Origin3d::ZERO,
        };

        let mut data = vec![[0.0; 4]; size * size * 6];

        for side in 0..6 {
            for y in 0..size {
                for x in 0..size {
                    let rgba = source[x + side * size + y * 6 * size].to_rgba();
                    data[x + (y + side * size) * size] = rgba.0;
                }
            }
        }

        let layout = wgpu::TextureDataLayout {
            offset: 0,
            bytes_per_row: size as u32 * 16,
            rows_per_image: size as _,
        };

        let size = wgpu::Extent3d {
            width: size as _,
            height: size as _,
            depth: 6,
        };

        queue.write_texture(view, view_as_bytes(&data), layout, size);
    }

    cubemap
}

impl UploadedEnvMap {
    fn new(device: &wgpu::Device, queue: &wgpu::Queue, data: &EnvMapData) -> UploadedEnvMap {
        let sky = upload_cubemap(device, queue, data.sky.0, &[&data.sky.1]);
        let irradiance = upload_cubemap(device, queue, data.irradiance.0, &[&data.irradiance.1]);
        let it = data.prefilter.1.iter();
        let prefilter_src = it.map(|v| v.as_slice()).collect::<Vec<_>>();
        let prefilter = upload_cubemap(device, queue, data.prefilter.0, &prefilter_src);
        UploadedEnvMap {
            sky,
            irradiance,
            prefilter,
        }
    }
}

impl EnvMapStorage {
    pub fn get(&self, id: Id<EnvMap>) -> &UploadedEnvMap {
        &self.envmaps[&id]
    }

    pub fn upload(&mut self, device: &wgpu::Device, queue: &wgpu::Queue, assets: &mut Assets) {
        for (handle, mesh) in assets.iter_mut::<EnvMap>() {
            let data = match mesh.data.take() {
                Some(v) => v,
                None => continue,
            };

            let uploaded = UploadedEnvMap::new(device, queue, &data);
            self.envmaps.insert(handle.id(), uploaded);
        }
    }

    pub fn cleanup(&mut self, assets: &mut Assets) {
        assets.cleanup_with(|id, _, _| {
            self.envmaps.remove(&id);
        });
    }
}
