use std::num::NonZeroU64;

use glsl_layout::Uniform;

use super::Camera;
use crate::graphics::UniformBuffer;

#[derive(Clone, Copy, Debug, Default, Uniform)]
pub struct GlobalUniforms {
    view: glsl_layout::mat4,
    projection: glsl_layout::mat4,
    view_projection: glsl_layout::mat4,
    camera_pos: glsl_layout::vec3,
    camera_dir: glsl_layout::vec3,
}

impl GlobalUniforms {
    pub fn new(camera: &Camera) -> GlobalUniforms {
        GlobalUniforms {
            view: camera.view().into(),
            projection: camera.projection().into(),
            view_projection: (camera.projection() * camera.view()).into(),
            camera_pos: camera.position().coords.into(),
            camera_dir: camera.direction().into(),
        }
    }

    fn size() -> NonZeroU64 {
        NonZeroU64::new(std::mem::size_of::<<GlobalUniforms as Uniform>::Std140>() as u64).unwrap()
    }
}

pub struct GlobalBindings {
    ubo: UniformBuffer<GlobalUniforms>,
    bind_group_layout: wgpu::BindGroupLayout,
    bind_group: wgpu::BindGroup,
}

impl GlobalBindings {
    pub fn new(device: &wgpu::Device) -> GlobalBindings {
        let ubo = UniformBuffer::new(device, "Global", GlobalUniforms::default());

        let desc = wgpu::SamplerDescriptor {
            label: Some("Sampler (Global Linear)"),
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Linear,
            ..Default::default()
        };
        let linear_sampler = device.create_sampler(&desc);

        let desc = wgpu::SamplerDescriptor {
            label: Some("Sampler (Global Nearest)"),
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        };
        let nearest_sampler = device.create_sampler(&desc);

        let desc = wgpu::BindGroupLayoutDescriptor {
            label: Some("Bind Group (Global)/Layout"),
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStage::VERTEX | wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::UniformBuffer {
                        dynamic: false,
                        min_binding_size: Some(GlobalUniforms::size()),
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 1,
                    visibility: wgpu::ShaderStage::VERTEX | wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::Sampler { comparison: false },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 2,
                    visibility: wgpu::ShaderStage::VERTEX | wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::Sampler { comparison: false },
                    count: None,
                },
            ],
        };
        let bind_group_layout = device.create_bind_group_layout(&desc);

        let desc = wgpu::BindGroupDescriptor {
            label: Some("Bind Group (Global)"),
            layout: &bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer(ubo.buffer().slice(..)),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&linear_sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: wgpu::BindingResource::Sampler(&nearest_sampler),
                },
            ],
        };
        let bind_group = device.create_bind_group(&desc);

        GlobalBindings {
            ubo,
            bind_group_layout,
            bind_group,
        }
    }

    pub fn set_uniforms(&mut self, uniforms: GlobalUniforms) {
        self.ubo.data = uniforms;
    }

    pub fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.bind_group_layout
    }

    pub fn bind_group(&self) -> &wgpu::BindGroup {
        &self.bind_group
    }

    pub fn upload(
        &self,
        device: &wgpu::Device,
        encoder: &mut wgpu::CommandEncoder,
        belt: &mut wgpu::util::StagingBelt,
    ) {
        self.ubo.upload(device, encoder, belt);
    }
}
