mod registry;
pub use self::registry::{MaterialEntry, MaterialRegistry};

use anyhow::{bail, Result};
use serde::de::Error as _;
use serde::{Deserialize, Deserializer};

use crate::assets::{Asset, AssetDefaultLoader, JsonLoader};

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Deserialize)]
pub enum MaterialProperty {
    Float,
    Float2,
    Float3,
    Float4,
}

impl From<MaterialProperty> for wgpu::VertexFormat {
    fn from(v: MaterialProperty) -> wgpu::VertexFormat {
        match v {
            MaterialProperty::Float => wgpu::VertexFormat::Float,
            MaterialProperty::Float2 => wgpu::VertexFormat::Float2,
            MaterialProperty::Float3 => wgpu::VertexFormat::Float3,
            MaterialProperty::Float4 => wgpu::VertexFormat::Float4,
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct Material {
    pub name: String,
    pub properties: Vec<MaterialProperty>,
    pub vert_shader: String,
    #[serde(default)]
    pub animated_vert_shader: Option<String>,
    pub frag_shader: String,
    #[serde(default)]
    pub uses_envmap: bool,
    #[serde(default)]
    pub num_textures: u32,
}

impl Asset for Material {}

impl AssetDefaultLoader for Material {
    type Loader = JsonLoader;
}

#[derive(Clone, Debug, Default)]
pub struct MaterialProperties {
    pub data: Vec<u32>,
}

impl MaterialProperties {
    pub fn new() -> MaterialProperties {
        MaterialProperties::default()
    }

    pub fn add(mut self, property: impl AsRef<[f32]>) -> MaterialProperties {
        let property = property.as_ref();
        assert!(property.len() > 0 && property.len() <= 4);
        for v in property {
            self.data.push(v.to_bits());
        }
        self
    }
}

impl MaterialProperties {
    pub fn from_json(json: serde_json::Value) -> Result<MaterialProperties> {
        let array = match json {
            serde_json::Value::Array(v) => v,
            _ => bail!("bad material properties"),
        };

        let mut data = Vec::with_capacity(256);

        let mut add_num = |num: serde_json::Number| {
            let val = num.as_f64().unwrap_or(0.0) as f32;
            data.push(val.to_bits());
        };

        for item in array {
            match item {
                serde_json::Value::Number(num) => add_num(num),
                serde_json::Value::Array(arr) if arr.len() <= 4 => {
                    for item in arr {
                        match item {
                            serde_json::Value::Number(num) => add_num(num),
                            _ => bail!("bad material properties"),
                        }
                    }
                }
                _ => bail!("bad material properties"),
            }
        }

        Ok(MaterialProperties { data })
    }
}

impl<'de> Deserialize<'de> for MaterialProperties {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let json = serde_json::Value::deserialize(deserializer)?;
        MaterialProperties::from_json(json).map_err(D::Error::custom)
    }
}
