use std::path::Path;

use anyhow::Result;
use bumpalo::Bump;
use fxhash::FxHashMap;

use crate::assets::{Assets, Id};
use crate::graphics::draw3d::{
    make_vbuf_desc, AnimatedMeshVertex, GlobalBindings, Instance, Material, MeshVertex,
};
use crate::graphics::load_shader;

pub struct MaterialEntry {
    pub name: String,
    pub uses_envmap: bool,
    pub regular_pipeline: wgpu::RenderPipeline,
    pub animated_pipeline: Option<wgpu::RenderPipeline>,
    pub textures_bind_group_layout: Option<wgpu::BindGroupLayout>,
}

pub struct MaterialRegistry {
    entries: FxHashMap<Id<Material>, MaterialEntry>,
    pub joints_bind_group_layout: wgpu::BindGroupLayout,
    pub envmap_bind_group_layout: wgpu::BindGroupLayout,
}

impl MaterialRegistry {
    pub fn new(device: &wgpu::Device) -> MaterialRegistry {
        MaterialRegistry {
            entries: FxHashMap::default(),
            joints_bind_group_layout: create_joints_bind_group_layout(device),
            envmap_bind_group_layout: create_envmap_bind_group_layout(device),
        }
    }

    pub fn upload(
        &mut self,
        device: &wgpu::Device,
        shaders_path: &Path,
        globals: &GlobalBindings,
        assets: &Assets,
    ) -> Result<()> {
        for (id, material) in assets.iter() {
            self.add(device, shaders_path, globals, id.id(), material)?;
        }

        Ok(())
    }

    // TODO: cleanup

    fn add(
        &mut self,
        device: &wgpu::Device,
        shaders_path: &Path,
        globals: &GlobalBindings,
        id: Id<Material>,
        material: &Material,
    ) -> Result<()> {
        if self.entries.contains_key(&id) {
            return Ok(());
        }

        let textures_bind_group_layout =
            create_textures_bind_group_layout(device, &material.name, material.num_textures);

        let envmap_bg_layout = if material.uses_envmap {
            Some(&self.envmap_bind_group_layout)
        } else {
            None
        };

        let regular_pipeline = create_pipeline(
            device,
            shaders_path,
            globals,
            material,
            textures_bind_group_layout.as_ref(),
            None,
            envmap_bg_layout,
        )?;

        let animated_pipeline = if material.animated_vert_shader.is_some() {
            Some(create_pipeline(
                device,
                shaders_path,
                globals,
                material,
                textures_bind_group_layout.as_ref(),
                Some(&self.joints_bind_group_layout),
                envmap_bg_layout,
            )?)
        } else {
            None
        };

        let entry = MaterialEntry {
            name: material.name.clone(),
            uses_envmap: material.uses_envmap,
            regular_pipeline,
            animated_pipeline,
            textures_bind_group_layout,
        };

        self.entries.insert(id, entry);
        Ok(())
    }

    pub fn get_material(&self, id: Id<Material>) -> &MaterialEntry {
        &self.entries[&id]
    }
}

fn create_textures_bind_group_layout(
    device: &wgpu::Device,
    name: &str,
    num_slots: u32,
) -> Option<wgpu::BindGroupLayout> {
    if num_slots == 0 {
        return None;
    }

    let mut entries = Vec::with_capacity(num_slots as _);

    for i in 0..num_slots {
        entries.push(wgpu::BindGroupLayoutEntry {
            binding: i,
            visibility: wgpu::ShaderStage::VERTEX | wgpu::ShaderStage::FRAGMENT,
            ty: wgpu::BindingType::SampledTexture {
                dimension: wgpu::TextureViewDimension::D2,
                component_type: wgpu::TextureComponentType::Float,
                multisampled: false,
            },
            count: None,
        });
    }

    let label = format!("Bind Group (Material {}, Textures)/Layout", name);
    let desc = wgpu::BindGroupLayoutDescriptor {
        label: Some(&label),
        entries: &entries,
    };

    Some(device.create_bind_group_layout(&desc))
}

fn create_joints_bind_group_layout(device: &wgpu::Device) -> wgpu::BindGroupLayout {
    let desc = wgpu::BindGroupLayoutDescriptor {
        label: Some("Bind Group (Joints)/Layout"),
        entries: &[wgpu::BindGroupLayoutEntry {
            binding: 0,
            visibility: wgpu::ShaderStage::VERTEX,
            ty: wgpu::BindingType::UniformBuffer {
                dynamic: true,
                min_binding_size: None,
            },
            count: None,
        }],
    };

    device.create_bind_group_layout(&desc)
}

fn create_envmap_bind_group_layout(device: &wgpu::Device) -> wgpu::BindGroupLayout {
    let desc = wgpu::BindGroupLayoutDescriptor {
        label: Some("Bind Group (EnvMap)/Layout"),
        entries: &[
            wgpu::BindGroupLayoutEntry {
                binding: 0,
                visibility: wgpu::ShaderStage::FRAGMENT,
                ty: wgpu::BindingType::SampledTexture {
                    dimension: wgpu::TextureViewDimension::Cube,
                    component_type: wgpu::TextureComponentType::Float,
                    multisampled: false,
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry {
                binding: 1,
                visibility: wgpu::ShaderStage::FRAGMENT,
                ty: wgpu::BindingType::SampledTexture {
                    dimension: wgpu::TextureViewDimension::Cube,
                    component_type: wgpu::TextureComponentType::Float,
                    multisampled: false,
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry {
                binding: 2,
                visibility: wgpu::ShaderStage::FRAGMENT,
                ty: wgpu::BindingType::SampledTexture {
                    dimension: wgpu::TextureViewDimension::D2,
                    component_type: wgpu::TextureComponentType::Float,
                    multisampled: false,
                },
                count: None,
            },
        ],
    };

    device.create_bind_group_layout(&desc)
}

fn create_pipeline(
    device: &wgpu::Device,
    shaders_path: &Path,
    globals: &GlobalBindings,
    material: &Material,
    bg_layout: Option<&wgpu::BindGroupLayout>,
    joints_bg_layout: Option<&wgpu::BindGroupLayout>,
    envmap_bg_layout: Option<&wgpu::BindGroupLayout>,
) -> Result<wgpu::RenderPipeline> {
    use wgpu::InputStepMode::{Instance as ModeInstance, Vertex as ModeVertex};

    let is_animated = joints_bg_layout.is_some();

    let vert_shader = load_shader(
        device,
        shaders_path,
        if is_animated {
            material.animated_vert_shader.as_ref().unwrap()
        } else {
            &material.vert_shader
        },
    )?;

    let frag_shader = load_shader(device, shaders_path, &material.frag_shader)?;

    let label = format!("Graphics Pipeline (Material {})/Layout", &material.name);

    let mut bind_group_layouts = vec![globals.bind_group_layout()];
    bind_group_layouts.extend(bg_layout);
    bind_group_layouts.extend(envmap_bg_layout);
    bind_group_layouts.extend(joints_bg_layout);

    let desc = wgpu::PipelineLayoutDescriptor {
        label: Some(&label),
        bind_group_layouts: &bind_group_layouts,
        push_constant_ranges: &[],
    };
    let layout = device.create_pipeline_layout(&desc);

    let mut loc = 0;
    let bump = Bump::new();

    let attrs = if is_animated {
        AnimatedMeshVertex::ATTRIBUTES
    } else {
        MeshVertex::ATTRIBUTES
    };

    let vert_vbuf = make_vbuf_desc(&bump, attrs, ModeVertex, &mut loc);
    let inst_vbuf = make_vbuf_desc(&bump, Instance::ATTRIBUTES, ModeInstance, &mut loc);

    let props = material.properties.iter().copied();
    let attrs = props.map(wgpu::VertexFormat::from).collect::<Vec<_>>();
    let prop_vbuf = make_vbuf_desc(&bump, &attrs, ModeInstance, &mut loc);

    let mut vbufs = vec![vert_vbuf, inst_vbuf, prop_vbuf];

    if material.num_textures > 0 {
        let tex_rects_attrs = (0..material.num_textures)
            .map(|_| wgpu::VertexFormat::Float4)
            .collect::<Vec<_>>();
        let tex_rects_vbuf = make_vbuf_desc(&bump, &tex_rects_attrs, ModeInstance, &mut loc);
        vbufs.push(tex_rects_vbuf);
    }

    let label = format!("Graphics Pipeline (Material {})", &material.name);
    let desc = wgpu::RenderPipelineDescriptor {
        label: Some(&label),
        layout: Some(&layout),
        vertex_stage: wgpu::ProgrammableStageDescriptor {
            module: &vert_shader,
            entry_point: "main",
        },
        fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
            module: &frag_shader,
            entry_point: "main",
        }),
        rasterization_state: None,
        primitive_topology: wgpu::PrimitiveTopology::TriangleList,
        color_states: &[wgpu::TextureFormat::Bgra8Unorm.into()],
        depth_stencil_state: Some(wgpu::DepthStencilStateDescriptor {
            format: wgpu::TextureFormat::Depth24PlusStencil8,
            depth_write_enabled: true,
            depth_compare: wgpu::CompareFunction::Less,
            stencil: wgpu::StencilStateDescriptor::default(),
        }),
        vertex_state: wgpu::VertexStateDescriptor {
            index_format: wgpu::IndexFormat::Uint32,
            vertex_buffers: &vbufs,
        },
        sample_count: 1,
        sample_mask: !0,
        alpha_to_coverage_enabled: false,
    };
    let pipeline = device.create_render_pipeline(&desc);
    Ok(pipeline)
}
