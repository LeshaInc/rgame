use std::io::BufRead;

use anyhow::{bail, Result};
use byteorder::{LittleEndian as LE, ReadBytesExt};
use fxhash::FxHashMap;
use nalgebra::{Matrix4, Quaternion, Unit, Vector2, Vector3, Vector4};

use super::{AnimatedMeshVertex, MeshVertex};
use crate::assets::{Asset, AssetDefaultLoader, SimpleAssetLoader};

#[derive(Clone, Debug)]
pub struct Mesh {
    pub data: Option<MeshData>,
}

impl Asset for Mesh {}

impl AssetDefaultLoader for Mesh {
    type Loader = MeshLoader;
}

#[derive(Clone, Debug, Default)]
pub struct MeshData {
    pub vertices: Vec<MeshVertex>,
    pub indices: Vec<u32>,
}

fn read_vec4(reader: &mut impl BufRead) -> Result<Vector4<f32>> {
    Ok(Vector4::new(
        reader.read_f32::<LE>()?,
        reader.read_f32::<LE>()?,
        reader.read_f32::<LE>()?,
        reader.read_f32::<LE>()?,
    ))
}

fn read_vec3(reader: &mut impl BufRead) -> Result<Vector3<f32>> {
    Ok(Vector3::new(
        reader.read_f32::<LE>()?,
        reader.read_f32::<LE>()?,
        reader.read_f32::<LE>()?,
    ))
}

fn read_vec2(reader: &mut impl BufRead) -> Result<Vector2<f32>> {
    Ok(Vector2::new(
        reader.read_f32::<LE>()?,
        reader.read_f32::<LE>()?,
    ))
}

fn read_mat4(reader: &mut impl BufRead) -> Result<Matrix4<f32>> {
    Ok(Matrix4::from_columns(&[
        read_vec4(reader)?,
        read_vec4(reader)?,
        read_vec4(reader)?,
        read_vec4(reader)?,
    ]))
}

/// Mesh loader with custom format
#[derive(Default)]
pub struct MeshLoader;

impl SimpleAssetLoader<Mesh> for MeshLoader {
    fn load(&self, mut reader: impl BufRead) -> Result<Mesh> {
        let mut buf = [0; 4];
        reader.read_exact(&mut buf)?;
        if &buf != b"MESH" {
            bail!("Invalid mesh signature");
        }

        let mut mesh_data = MeshData::default();

        let num_indices = reader.read_u32::<LE>()?;
        mesh_data.indices.reserve(num_indices as _);

        for _ in 0..num_indices {
            mesh_data.indices.push(reader.read_u32::<LE>()?);
        }

        let num_vertices = reader.read_u32::<LE>()?;
        mesh_data.vertices.reserve(num_vertices as _);

        for _ in 0..num_vertices {
            let pos = read_vec3(&mut reader)?;
            let normal = read_vec3(&mut reader)?;
            let tex = read_vec2(&mut reader)?;
            mesh_data.vertices.push(MeshVertex { pos, normal, tex });
        }

        Ok(Mesh {
            data: Some(mesh_data),
        })
    }
}

#[derive(Clone, Debug)]
pub struct Joint {
    pub parent: Option<JointId>,
    pub children: Vec<JointId>,
    pub transform: JointTransform,
    pub inv_bind_matrix: Matrix4<f32>,
}

#[derive(Clone, Copy, Debug, Default)]
#[repr(C)]
pub struct JointTransform {
    pub position: Vector3<f32>,
    pub rotation: Unit<Quaternion<f32>>,
    pub scale: Vector3<f32>,
}

impl JointTransform {
    pub fn to_matrix(&self) -> Matrix4<f32> {
        Matrix4::new_translation(&self.position)
            * self.rotation.to_homogeneous()
            * Matrix4::new_nonuniform_scaling(&self.scale)
    }

    pub fn lerp(&self, other: &JointTransform, alpha: f32) -> JointTransform {
        JointTransform {
            position: self.position.lerp(&other.position, alpha),
            rotation: self.rotation.slerp(&other.rotation, alpha),
            scale: self.scale.lerp(&other.scale, alpha),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct JointId(pub u8);

#[derive(Clone, Debug, Default)]
pub struct Pose {
    pub joints: FxHashMap<JointId, JointTransform>,
}

impl Pose {
    pub fn lerp(&mut self, other: &Pose, alpha: f32) {
        for (other_j, other_t) in &other.joints {
            match self.joints.get_mut(other_j) {
                Some(self_t) => {
                    *self_t = self_t.lerp(other_t, alpha);
                }
                None => {
                    self.joints.insert(*other_j, *other_t);
                }
            }
        }
    }
}

impl From<&[Joint]> for Pose {
    fn from(joints: &[Joint]) -> Pose {
        let it = joints.iter().enumerate();
        let joints = it
            .map(|(idx, joint)| (JointId(idx as _), joint.transform))
            .collect::<FxHashMap<_, _>>();
        Pose { joints }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Animation {
    pub frames: Vec<Pose>,
}

#[derive(Clone, Debug)]
pub struct AnimatedMesh {
    pub data: Option<AnimatedMeshData>,
    pub joints: Vec<Joint>,
    pub root_joint: JointId,
    pub animations: FxHashMap<String, Animation>,
    pub skeleton_transform: Matrix4<f32>,
}

impl Asset for AnimatedMesh {}

impl AssetDefaultLoader for AnimatedMesh {
    type Loader = AnimatedMeshLoader;
}

#[derive(Clone, Debug, Default)]
pub struct AnimatedMeshData {
    pub vertices: Vec<AnimatedMeshVertex>,
    pub indices: Vec<u32>,
}

/// Animated mesh loader with custom format
#[derive(Default)]
pub struct AnimatedMeshLoader;

impl SimpleAssetLoader<AnimatedMesh> for AnimatedMeshLoader {
    fn load(&self, mut reader: impl BufRead) -> Result<AnimatedMesh> {
        let mut buf = [0; 5];
        reader.read_exact(&mut buf)?;
        if &buf != b"AMESH" {
            bail!("Invalid mesh signature");
        }

        let mut mesh_data = AnimatedMeshData::default();
        let mut joints = Vec::new();
        let mut animations = FxHashMap::default();

        let num_indices = reader.read_u32::<LE>()?;
        mesh_data.indices.reserve(num_indices as _);

        for _ in 0..num_indices {
            mesh_data.indices.push(reader.read_u32::<LE>()?);
        }

        let num_vertices = reader.read_u32::<LE>()?;
        mesh_data.vertices.reserve(num_vertices as _);

        for _ in 0..num_vertices {
            let pos = read_vec3(&mut reader)?;
            let normal = read_vec3(&mut reader)?;
            let tex = read_vec2(&mut reader)?;

            let mut weights = [0.0; 4];
            for w in &mut weights {
                *w = reader.read_f32::<LE>()?;
            }

            let mut joints = [0; 4];
            reader.read_exact(&mut joints)?;

            mesh_data.vertices.push(AnimatedMeshVertex {
                pos,
                normal,
                tex,
                weights,
                joints,
            });
        }

        let skeleton_transform = read_mat4(&mut reader)?;

        let num_joints = reader.read_u8()?;
        joints.reserve(num_joints as _);

        let root_joint = JointId(reader.read_u8()?);

        for _ in 0..num_joints {
            let parent = match reader.read_u8()? {
                255 => None,
                v => Some(JointId(v)),
            };
            let inv_bind_matrix = read_mat4(&mut reader)?;

            let transform = JointTransform {
                position: read_vec3(&mut reader)?,
                rotation: Unit::new_normalize(Quaternion::from(read_vec4(&mut reader)?)),
                scale: read_vec3(&mut reader)?,
            };

            joints.push(Joint {
                parent,
                children: Vec::new(),
                inv_bind_matrix,
                transform,
            })
        }

        for i in 0..num_joints {
            let parent = joints[i as usize].parent;
            if let Some(parent) = parent {
                joints[parent.0 as usize].children.push(JointId(i));
            }
        }

        while let Ok(name_len) = reader.read_u32::<LE>() {
            let mut name_buf = vec![0; name_len as usize];
            reader.read_exact(&mut name_buf)?;
            let name = String::from_utf8(name_buf)?;

            let mut animation = Animation::default();

            let num_joints = reader.read_u8()?;

            for _ in 0..num_joints {
                let joint_idx = reader.read_u8()?;

                let num_frames = reader.read_u32::<LE>()? as usize;

                for i in 0..num_frames {
                    if animation.frames.len() <= i {
                        animation.frames.push(Pose::default());
                    }

                    let pose = &mut animation.frames[i];
                    pose.joints.insert(
                        JointId(joint_idx),
                        JointTransform {
                            position: read_vec3(&mut reader)?,
                            rotation: Unit::new_normalize(Quaternion::from(read_vec4(
                                &mut reader,
                            )?)),
                            scale: read_vec3(&mut reader)?,
                        },
                    );
                }
            }

            animations.insert(name, animation);
        }

        Ok(AnimatedMesh {
            data: Some(mesh_data),
            joints,
            root_joint,
            animations,
            skeleton_transform,
        })
    }
}
