use fxhash::FxHashMap;
use wgpu::util::{BufferInitDescriptor, DeviceExt};

use super::{AnimatedMesh, Mesh};
use crate::assets::{Assets, Id};
use crate::graphics::view_as_bytes;

#[derive(Debug, Default)]
pub struct MeshStorage {
    regular: FxHashMap<Id<Mesh>, UploadedMesh>,
    animated: FxHashMap<Id<AnimatedMesh>, UploadedMesh>,
}

#[derive(Debug)]
pub struct UploadedMesh {
    pub vbuf: wgpu::Buffer,
    pub ibuf: wgpu::Buffer,
    pub num_indices: u32,
}

impl UploadedMesh {
    fn new(device: &wgpu::Device, name: &str, vertices: &[u8], indices: &[u32]) -> UploadedMesh {
        let label = format!("Mesh ({})/Vertex Buffer", name);
        let desc = BufferInitDescriptor {
            label: Some(&label),
            contents: vertices,
            usage: wgpu::BufferUsage::VERTEX,
        };
        let vbuf = device.create_buffer_init(&desc);

        let label = format!("Mesh ({})/Index Buffer", name);
        let desc = BufferInitDescriptor {
            label: Some(&label),
            contents: view_as_bytes(&indices),
            usage: wgpu::BufferUsage::INDEX,
        };
        let ibuf = device.create_buffer_init(&desc);

        UploadedMesh {
            vbuf,
            ibuf,
            num_indices: indices.len() as _,
        }
    }
}

impl MeshStorage {
    pub fn get(&self, mesh_id: Id<Mesh>) -> &UploadedMesh {
        &self.regular[&mesh_id]
    }

    pub fn get_animated(&self, mesh_id: Id<AnimatedMesh>) -> &UploadedMesh {
        &self.animated[&mesh_id]
    }

    pub fn upload(&mut self, device: &wgpu::Device, assets: &mut Assets) {
        for (handle, mesh) in assets.iter_mut::<Mesh>() {
            let data = match mesh.data.take() {
                Some(v) => v,
                None => continue,
            };

            let name = format!("Regular {}", handle.id().inner);
            let mesh =
                UploadedMesh::new(device, &name, view_as_bytes(&data.vertices), &data.indices);

            self.regular.insert(handle.id(), mesh);
        }

        for (handle, mesh) in assets.iter_mut::<AnimatedMesh>() {
            let data = match mesh.data.take() {
                Some(v) => v,
                None => continue,
            };

            let name = format!("Animated {}", handle.id().inner);
            let mesh =
                UploadedMesh::new(device, &name, view_as_bytes(&data.vertices), &data.indices);

            self.animated.insert(handle.id(), mesh);
        }
    }

    pub fn cleanup(&mut self, assets: &mut Assets) {
        assets.cleanup_with(|id, _, _| {
            self.regular.remove(&id);
        });
        assets.cleanup_with(|id, _, _| {
            self.animated.remove(&id);
        });
    }
}
