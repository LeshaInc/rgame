use std::path::{Path, PathBuf};

use anyhow::{bail, Context, Result};
use serde::Deserialize;

use super::{AnimatedMesh, Material, MaterialProperties, Mesh};
use crate::assets::{Asset, AssetDefaultLoader, AssetLoader, AssetSource, Handle, NewAssets};
use crate::graphics::Texture;

pub struct Model {
    pub parts: Vec<ModelPart>,
}

impl Asset for Model {}

impl AssetDefaultLoader for Model {
    type Loader = ModelLoader;
}

pub struct ModelPart {
    pub mesh: ModelMesh,
    pub material: Handle<Material>,
    pub properties: MaterialProperties,
    pub textures: Vec<Handle<Texture>>,
}

pub enum ModelMesh {
    Regular(Handle<Mesh>),
    Animated(Handle<AnimatedMesh>),
}

type ModelDefinition = Vec<ModelPartDefinition>;

#[derive(Deserialize)]
struct ModelPartDefinition {
    pub mesh: PathBuf,
    pub material: PathBuf,
    pub properties: MaterialProperties,
    pub textures: Vec<PathBuf>,
}

#[derive(Default)]
pub struct ModelLoader;

impl AssetLoader<Model> for ModelLoader {
    fn load(&self, new: &NewAssets, source: &dyn AssetSource, path: &Path) -> Result<Model> {
        let reader = source.load(path)?;
        let def: ModelDefinition = serde_json::from_reader(reader)?;

        let mut parts = Vec::with_capacity(def.len());

        for part_def in def {
            let ext = part_def.mesh.extension();
            let ext = ext.and_then(|s| s.to_str()).context("No mesh extension")?;
            let mesh = match ext {
                "mesh" => ModelMesh::Regular(new.load(part_def.mesh)),
                "amesh" => ModelMesh::Animated(new.load(part_def.mesh)),
                _ => bail!("Bad mesh extension"),
            };

            parts.push(ModelPart {
                mesh,
                material: new.load(part_def.material),
                properties: part_def.properties,
                textures: part_def.textures.into_iter().map(|p| new.load(p)).collect(),
            });
        }

        Ok(Model { parts })
    }
}
