use fxhash::FxHashMap;

use crate::assets::Id;
use crate::graphics::draw3d::{DrawCtx3D, Material, MaterialRegistry, UploadedDrawList};
use crate::graphics::{Texture, TextureStorage};

#[derive(Debug)]
struct AtlasGroup {
    material_id: Id<Material>,
    bind_group: wgpu::BindGroup,
    atlas_sizes: Vec<u32>,
}

impl AtlasGroup {
    fn new(
        device: &wgpu::Device,
        tex_storage: &TextureStorage,
        materials: &MaterialRegistry,
        material_id: Id<Material>,
        atlas_indices: &[usize],
    ) -> AtlasGroup {
        let views = atlas_indices
            .iter()
            .map(|atlas_idx| tex_storage.get_atlas(*atlas_idx).create_view())
            .collect::<Vec<_>>();

        let atlas_sizes = atlas_indices
            .iter()
            .map(|atlas_idx| tex_storage.get_atlas(*atlas_idx).size())
            .collect::<Vec<_>>();

        let entries = views
            .iter()
            .enumerate()
            .map(|(i, view)| wgpu::BindGroupEntry {
                binding: i as _,
                resource: wgpu::BindingResource::TextureView(view),
            })
            .collect::<Vec<_>>();

        let material = materials.get_material(material_id);

        let label = format!("Bind Group (Material {})", material.name);
        let layout = material.textures_bind_group_layout.as_ref().unwrap();
        let desc = wgpu::BindGroupDescriptor {
            label: Some(&label),
            layout,
            entries: &entries,
        };

        let bind_group = device.create_bind_group(&desc);
        AtlasGroup {
            material_id,
            bind_group,
            atlas_sizes,
        }
    }
}

#[derive(Debug, Default)]
pub struct ForwardPass {
    atlas_groups: FxHashMap<Vec<usize>, AtlasGroup>,
}

impl ForwardPass {
    fn refresh_atlas_groups(
        &mut self,
        device: &wgpu::Device,
        tex_storage: &TextureStorage,
        materials: &MaterialRegistry,
    ) {
        'outer: for (atlas_indices, atlas_group) in self.atlas_groups.iter_mut() {
            for (&atlas_idx, &atlas_size) in atlas_indices.iter().zip(&atlas_group.atlas_sizes) {
                let atlas = tex_storage.get_atlas(atlas_idx);
                if atlas.size() != atlas_size {
                    continue;
                }

                *atlas_group = AtlasGroup::new(
                    device,
                    tex_storage,
                    materials,
                    atlas_group.material_id,
                    atlas_indices,
                );

                continue 'outer;
            }
        }
    }

    fn setup_atlas_group(
        &mut self,
        device: &wgpu::Device,
        tex_storage: &TextureStorage,
        textures: &[Id<Texture>],
        materials: &MaterialRegistry,
        material: Id<Material>,
        atlas_indices: &mut Vec<usize>,
    ) {
        if textures.is_empty() {
            return;
        }

        for &texture in textures {
            let atlas_idx = tex_storage.get_texture_atlas_idx(texture);
            atlas_indices.push(atlas_idx);
        }

        if !self.atlas_groups.contains_key(atlas_indices.as_slice()) {
            let atlas_group =
                AtlasGroup::new(device, tex_storage, materials, material, &atlas_indices);
            self.atlas_groups.insert(atlas_indices.clone(), atlas_group);
        }

        atlas_indices.clear();
    }

    fn get_bind_group(
        &self,
        tex_storage: &TextureStorage,
        textures: &[Id<Texture>],
        atlas_indices: &mut Vec<usize>,
    ) -> Option<&wgpu::BindGroup> {
        if textures.is_empty() {
            return None;
        }

        for &texture in textures {
            let atlas_idx = tex_storage.get_texture_atlas_idx(texture);
            atlas_indices.push(atlas_idx);
        }

        let atlas_group = &self.atlas_groups[atlas_indices.as_slice()];
        atlas_indices.clear();
        Some(&atlas_group.bind_group)
    }

    pub fn run(&mut self, ctx: &mut DrawCtx3D<'_>, list: &UploadedDrawList) {
        let list_instances_buf = match list.instances_buf() {
            Some(v) => v,
            None => return,
        };

        self.refresh_atlas_groups(ctx.device, ctx.textures, ctx.materials);

        let color_attachment = wgpu::RenderPassColorAttachmentDescriptor {
            attachment: ctx.framebuffers.get_view("ColorLinear"),
            resolve_target: None,
            ops: wgpu::Operations {
                load: wgpu::LoadOp::Clear(wgpu::Color::WHITE),
                store: true,
            },
        };

        let depth_attachment = wgpu::RenderPassDepthStencilAttachmentDescriptor {
            attachment: ctx.framebuffers.get_view("DepthStencil"),
            depth_ops: Some(wgpu::Operations {
                load: wgpu::LoadOp::Clear(1.0),
                store: true,
            }),
            stencil_ops: None,
        };

        let desc = wgpu::RenderPassDescriptor {
            color_attachments: &[color_attachment],
            depth_stencil_attachment: Some(depth_attachment),
        };
        let mut rpass = ctx.encoder.begin_render_pass(&desc);

        let mut atlas_indices = Vec::new();

        rpass.set_bind_group(0, &ctx.globals.bind_group(), &[]);

        for (mat_id, batch) in list.iter_batches() {
            for subbatch in batch.iter_all_meshes() {
                let mut prev_textures = &[][..];
                for (_, textures) in subbatch.textures().enumerate() {
                    if textures == prev_textures {
                        continue;
                    }

                    self.setup_atlas_group(
                        ctx.device,
                        ctx.textures,
                        textures,
                        ctx.materials,
                        mat_id,
                        &mut atlas_indices,
                    );

                    prev_textures = textures;
                }
            }
        }

        for (mat_id, batch) in list.iter_batches() {
            rpass.push_debug_group(&format!("Material Batch ({})", batch.name()));

            let material = ctx.materials.get_material(mat_id);

            rpass.set_pipeline(&material.regular_pipeline);

            for (mesh_id, subbatch) in batch.iter_regular_meshes() {
                let mesh = ctx.meshes.get(mesh_id);

                rpass.push_debug_group(&format!("Mesh Batch (Regular {})", mesh_id.inner));

                rpass.set_index_buffer(mesh.ibuf.slice(..));
                rpass.set_vertex_buffer(0, mesh.vbuf.slice(..));
                rpass.set_vertex_buffer(1, list_instances_buf.slice(subbatch.instances_range()));
                if let Some(params) = list.params_buf() {
                    rpass.set_vertex_buffer(2, params.slice(subbatch.params_range()));
                }

                if subbatch.textures_per_instance() > 0 {
                    rpass.set_vertex_buffer(
                        3,
                        list.tex_rects_buf()
                            .unwrap()
                            .slice(subbatch.tex_rects_range()),
                    );
                }

                let mut prev_textures = subbatch.textures().next().unwrap();
                let mut batch_size = 0;
                for (i, textures) in subbatch.textures().enumerate() {
                    let i = i as u32;

                    if textures == prev_textures && i != subbatch.num_instances() - 1 {
                        batch_size += 1;
                        continue;
                    }

                    let mut bg_offset = 1;

                    if let Some(bind_group) =
                        self.get_bind_group(ctx.textures, textures, &mut atlas_indices)
                    {
                        rpass.set_bind_group(bg_offset, bind_group, &[]);
                        bg_offset += 1;
                    }

                    if material.uses_envmap {
                        rpass.set_bind_group(bg_offset, list.envmap_bind_group().unwrap(), &[]);
                    }

                    let start = i - batch_size;
                    let end = i + 1;
                    rpass.draw_indexed(0..mesh.num_indices, 0, start..end);

                    prev_textures = textures;
                    batch_size = 0;
                }

                rpass.pop_debug_group();
            }

            let joints_bind_group = match list.joints_bind_group() {
                Some(v) if batch.iter_animated_meshes().next().is_some() => v,
                _ => {
                    rpass.pop_debug_group();
                    continue;
                }
            };

            let pipeline = material
                .animated_pipeline
                .as_ref()
                .expect("Material doesn't support animations");
            rpass.set_pipeline(pipeline);

            for (mesh_id, subbatch) in batch.iter_animated_meshes() {
                let mesh = ctx.meshes.get_animated(mesh_id);

                rpass.push_debug_group(&format!("Mesh Batch (Animated {})", mesh_id.inner));

                rpass.set_index_buffer(mesh.ibuf.slice(..));
                rpass.set_vertex_buffer(0, mesh.vbuf.slice(..));
                rpass.set_vertex_buffer(1, list_instances_buf.slice(subbatch.instances_range()));
                if let Some(params) = list.params_buf() {
                    rpass.set_vertex_buffer(2, params.slice(subbatch.params_range()));
                }

                if subbatch.textures_per_instance() > 0 {
                    rpass.set_vertex_buffer(
                        3,
                        list.tex_rects_buf()
                            .unwrap()
                            .slice(subbatch.tex_rects_range()),
                    );
                }

                for (i, textures) in subbatch.textures().enumerate() {
                    let mut bg_offset = 1;

                    if let Some(bind_group) =
                        self.get_bind_group(ctx.textures, textures, &mut atlas_indices)
                    {
                        rpass.set_bind_group(bg_offset, bind_group, &[]);
                        bg_offset += 1;
                    }

                    if material.uses_envmap {
                        rpass.set_bind_group(bg_offset, list.envmap_bind_group().unwrap(), &[]);
                        bg_offset += 1;
                    }

                    let offset = subbatch.joints_ranges()[i].start;
                    rpass.set_bind_group(bg_offset, &joints_bind_group, &[offset as u32]);

                    let i = i as u32;
                    rpass.draw_indexed(0..mesh.num_indices, 0, i..i + 1)
                }

                rpass.pop_debug_group();
            }

            rpass.pop_debug_group();
        }
    }
}
