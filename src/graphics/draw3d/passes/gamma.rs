use anyhow::Result;

use crate::graphics::draw3d::{DrawCtx3D, InitCtx3D};

#[derive(Debug)]
pub struct GammaCorrectionPass {
    pipeline: wgpu::RenderPipeline,
    bind_group_layout: wgpu::BindGroupLayout,
}

impl GammaCorrectionPass {
    pub fn new(ctx: &InitCtx3D<'_>) -> Result<GammaCorrectionPass> {
        let desc = wgpu::BindGroupLayoutDescriptor {
            label: Some("Bind Group (Gamma Correction)/Layout"),
            entries: &[wgpu::BindGroupLayoutEntry {
                binding: 0,
                visibility: wgpu::ShaderStage::FRAGMENT,
                ty: wgpu::BindingType::SampledTexture {
                    dimension: wgpu::TextureViewDimension::D2,
                    component_type: wgpu::TextureComponentType::Float,
                    multisampled: false,
                },
                count: None,
            }],
        };
        let bind_group_layout = ctx.device.create_bind_group_layout(&desc);

        let desc = wgpu::PipelineLayoutDescriptor {
            label: Some("Graphics Pipeline (Gamma Correction)/Layout"),
            bind_group_layouts: &[ctx.globals.bind_group_layout(), &bind_group_layout],
            push_constant_ranges: &[],
        };
        let layout = ctx.device.create_pipeline_layout(&desc);

        let vert_shader = ctx.load_shader("gamma.vert.spv")?;
        let frag_shader = ctx.load_shader("gamma.frag.spv")?;

        let desc = wgpu::RenderPipelineDescriptor {
            label: Some("Graphics Pipeline (Gamma Correction)"),
            layout: Some(&layout),
            vertex_stage: wgpu::ProgrammableStageDescriptor {
                module: &vert_shader,
                entry_point: "main",
            },
            fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
                module: &frag_shader,
                entry_point: "main",
            }),
            rasterization_state: None,
            primitive_topology: wgpu::PrimitiveTopology::TriangleList,
            color_states: &[wgpu::TextureFormat::Bgra8Unorm.into()],
            depth_stencil_state: None,
            vertex_state: wgpu::VertexStateDescriptor {
                index_format: wgpu::IndexFormat::Uint16,
                vertex_buffers: &[],
            },
            sample_count: 1,
            sample_mask: !0,
            alpha_to_coverage_enabled: false,
        };
        let pipeline = ctx.device.create_render_pipeline(&desc);

        Ok(GammaCorrectionPass {
            pipeline,
            bind_group_layout,
        })
    }

    pub fn run(&mut self, ctx: &mut DrawCtx3D<'_>, src: &'static str, dst: &'static str) {
        let desc = wgpu::BindGroupDescriptor {
            label: Some("Bind Group (Gamma Correction)"),
            layout: &self.bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: wgpu::BindingResource::TextureView(ctx.framebuffers.get_view(src)),
            }],
        };
        let bind_group = ctx.device.create_bind_group(&desc);

        let color_attachment = wgpu::RenderPassColorAttachmentDescriptor {
            attachment: ctx.framebuffers.get_view(dst),
            resolve_target: None,
            ops: wgpu::Operations {
                load: wgpu::LoadOp::Clear(wgpu::Color::WHITE),
                store: true,
            },
        };

        let desc = wgpu::RenderPassDescriptor {
            color_attachments: &[color_attachment],
            depth_stencil_attachment: None,
        };
        let mut rpass = ctx.encoder.begin_render_pass(&desc);
        rpass.set_bind_group(0, ctx.globals.bind_group(), &[]);
        rpass.set_bind_group(1, &bind_group, &[]);
        rpass.set_pipeline(&self.pipeline);
        rpass.draw(0..3, 0..1);
    }
}
