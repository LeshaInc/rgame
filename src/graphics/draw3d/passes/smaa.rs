use anyhow::Result;

use crate::graphics::draw3d::{DrawCtx3D, InitCtx3D};

pub struct SmaaPass {
    search_tex_view: wgpu::TextureView,
    area_tex_view: wgpu::TextureView,
    edge_detection_pipeline: wgpu::RenderPipeline,
    edge_detection_bgl: wgpu::BindGroupLayout,
    blending_weight_calculation_pipeline: wgpu::RenderPipeline,
    blending_weight_calculation_bgl: wgpu::BindGroupLayout,
    neighborhood_blending_pipeline: wgpu::RenderPipeline,
    neighborhood_blending_bgl: wgpu::BindGroupLayout,
}

impl SmaaPass {
    pub fn new(ctx: &InitCtx3D<'_>) -> Result<SmaaPass> {
        let label = "Bind Group (SMAA Edge Detection)/Layout";
        let edge_detection_bgl = create_bind_group_layout(ctx, label, 1);
        let label = "Bind Group (SMAA Blending Weight Calculation)/Layout";
        let blending_weight_calculation_bgl = create_bind_group_layout(ctx, label, 3);
        let label = "Bind Group (SMAA Neighborhood Blending)/Layout";
        let neighborhood_blending_bgl = create_bind_group_layout(ctx, label, 2);
        Ok(SmaaPass {
            search_tex_view: create_search_tex_view(ctx),
            area_tex_view: create_area_tex_view(ctx),
            edge_detection_pipeline: create_edge_detection_pipeline(ctx, &edge_detection_bgl)?,
            edge_detection_bgl,
            blending_weight_calculation_pipeline: create_blending_weight_calculation_pipeline(
                ctx,
                &blending_weight_calculation_bgl,
            )?,
            blending_weight_calculation_bgl,
            neighborhood_blending_pipeline: create_neighborhood_blending_pipeline(
                ctx,
                &neighborhood_blending_bgl,
            )?,
            neighborhood_blending_bgl,
        })
    }

    pub fn run(&self, ctx: &mut DrawCtx3D<'_>) {
        let col_srgb_view = ctx.framebuffers.get_view("ColorSRGB");
        let out_col_view = ctx.framebuffers.get_swapchain_view();
        let edges_view = ctx.framebuffers.get_view("SMAAEdges");
        let blend_view = ctx.framebuffers.get_view("SMAABlend");
        let ds_view = ctx.framebuffers.get_view("DepthStencil");

        let label = "Bind Group (SMAA Edge Detection)";
        let edge_detection_bg =
            create_bind_group(ctx, &self.edge_detection_bgl, label, &[col_srgb_view]);

        let label = "Bind Group (SMAA Blending Weight Calculation)";
        let blending_weight_calculation_bg = create_bind_group(
            ctx,
            &self.blending_weight_calculation_bgl,
            label,
            &[edges_view, &self.area_tex_view, &self.search_tex_view],
        );

        let label = "Bind Group (SMAA Neighborhood Blending)";
        let neighborhood_blending_bg = create_bind_group(
            ctx,
            &self.neighborhood_blending_bgl,
            label,
            &[col_srgb_view, blend_view],
        );

        let width = ctx.resolution.width as f32;
        let height = ctx.resolution.height as f32;
        let push_constants = &[
            (1.0 / width).to_bits(),
            (1.0 / height).to_bits(),
            width.to_bits(),
            height.to_bits(),
        ];

        let ops = wgpu::Operations {
            load: wgpu::LoadOp::Clear(wgpu::Color::TRANSPARENT),
            store: true,
        };

        let desc = wgpu::RenderPassDescriptor {
            color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                attachment: edges_view,
                resolve_target: None,
                ops,
            }],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachmentDescriptor {
                attachment: ds_view,
                depth_ops: None,
                stencil_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(0),
                    store: true,
                }),
            }),
        };
        let mut rpass = ctx.encoder.begin_render_pass(&desc);
        rpass.set_pipeline(&self.edge_detection_pipeline);
        rpass.set_bind_group(0, ctx.globals.bind_group(), &[]);
        rpass.set_bind_group(1, &edge_detection_bg, &[]);
        rpass.set_push_constants(
            wgpu::ShaderStage::VERTEX | wgpu::ShaderStage::FRAGMENT,
            0,
            push_constants,
        );
        rpass.draw(0..3, 0..1);
        drop(rpass);

        let desc = wgpu::RenderPassDescriptor {
            color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                attachment: blend_view,
                resolve_target: None,
                ops,
            }],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachmentDescriptor {
                attachment: ds_view,
                depth_ops: None,
                stencil_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Load,
                    store: false,
                }),
            }),
        };
        let mut rpass = ctx.encoder.begin_render_pass(&desc);
        rpass.set_pipeline(&self.blending_weight_calculation_pipeline);
        rpass.set_bind_group(0, ctx.globals.bind_group(), &[]);
        rpass.set_bind_group(1, &blending_weight_calculation_bg, &[]);
        rpass.set_push_constants(
            wgpu::ShaderStage::VERTEX | wgpu::ShaderStage::FRAGMENT,
            0,
            push_constants,
        );
        rpass.set_stencil_reference(1);
        rpass.draw(0..3, 0..1);
        drop(rpass);

        let desc = wgpu::RenderPassDescriptor {
            color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                attachment: out_col_view,
                resolve_target: None,
                ops,
            }],
            depth_stencil_attachment: None,
        };
        let mut rpass = ctx.encoder.begin_render_pass(&desc);
        rpass.set_pipeline(&self.neighborhood_blending_pipeline);
        rpass.set_bind_group(0, ctx.globals.bind_group(), &[]);
        rpass.set_bind_group(1, &neighborhood_blending_bg, &[]);
        rpass.set_push_constants(
            wgpu::ShaderStage::VERTEX | wgpu::ShaderStage::FRAGMENT,
            0,
            push_constants,
        );
        rpass.draw(0..3, 0..1);
        drop(rpass);
    }
}

fn create_bind_group(
    ctx: &DrawCtx3D<'_>,
    layout: &wgpu::BindGroupLayout,
    label: &str,
    tex_views: &[&wgpu::TextureView],
) -> wgpu::BindGroup {
    let entries = tex_views
        .iter()
        .enumerate()
        .map(|(i, tex_view)| wgpu::BindGroupEntry {
            binding: i as _,
            resource: wgpu::BindingResource::TextureView(tex_view),
        })
        .collect::<Vec<_>>();
    let desc = &wgpu::BindGroupDescriptor {
        label: Some(label),
        layout,
        entries: &entries,
    };
    ctx.device.create_bind_group(&desc)
}

const TEXTURE_BINDING: wgpu::BindingType = wgpu::BindingType::SampledTexture {
    dimension: wgpu::TextureViewDimension::D2,
    component_type: wgpu::TextureComponentType::Float,
    multisampled: false,
};

fn create_bind_group_layout(
    ctx: &InitCtx3D<'_>,
    label: &str,
    num_textures: u32,
) -> wgpu::BindGroupLayout {
    let entries = (0..num_textures)
        .map(|i| wgpu::BindGroupLayoutEntry {
            binding: i,
            ty: TEXTURE_BINDING,
            visibility: wgpu::ShaderStage::FRAGMENT,
            count: None,
        })
        .collect::<Vec<_>>();
    let desc = &wgpu::BindGroupLayoutDescriptor {
        label: Some(label),
        entries: &entries,
    };
    ctx.device.create_bind_group_layout(&desc)
}

fn create_edge_detection_pipeline(
    ctx: &InitCtx3D<'_>,
    bg_layout: &wgpu::BindGroupLayout,
) -> Result<wgpu::RenderPipeline> {
    let vert_shader = ctx.load_shader("smaa/edge_detection.vert.spv")?;
    let frag_shader = ctx.load_shader("smaa/edge_detection.frag.spv")?;
    Ok(create_pipeline(
        ctx,
        bg_layout,
        &vert_shader,
        &frag_shader,
        Some(wgpu::DepthStencilStateDescriptor {
            format: wgpu::TextureFormat::Depth24PlusStencil8,
            depth_write_enabled: false,
            depth_compare: wgpu::CompareFunction::Always,
            stencil: wgpu::StencilStateDescriptor {
                front: wgpu::StencilStateFaceDescriptor {
                    compare: wgpu::CompareFunction::Always,
                    fail_op: wgpu::StencilOperation::Zero,
                    depth_fail_op: wgpu::StencilOperation::Zero,
                    pass_op: wgpu::StencilOperation::IncrementClamp,
                },
                back: wgpu::StencilStateFaceDescriptor::IGNORE,
                read_mask: !0,
                write_mask: !0,
            },
        }),
    ))
}

fn create_blending_weight_calculation_pipeline(
    ctx: &InitCtx3D<'_>,
    bg_layout: &wgpu::BindGroupLayout,
) -> Result<wgpu::RenderPipeline> {
    let vert_shader = ctx.load_shader("smaa/blending_weight_calculation.vert.spv")?;
    let frag_shader = ctx.load_shader("smaa/blending_weight_calculation.frag.spv")?;
    Ok(create_pipeline(
        ctx,
        bg_layout,
        &vert_shader,
        &frag_shader,
        Some(wgpu::DepthStencilStateDescriptor {
            format: wgpu::TextureFormat::Depth24PlusStencil8,
            depth_write_enabled: false,
            depth_compare: wgpu::CompareFunction::Always,
            stencil: wgpu::StencilStateDescriptor {
                front: wgpu::StencilStateFaceDescriptor {
                    compare: wgpu::CompareFunction::Equal,
                    fail_op: wgpu::StencilOperation::Keep,
                    depth_fail_op: wgpu::StencilOperation::Keep,
                    pass_op: wgpu::StencilOperation::Keep,
                },
                back: wgpu::StencilStateFaceDescriptor::IGNORE,
                read_mask: !0,
                write_mask: !0,
            },
        }),
    ))
}

fn create_neighborhood_blending_pipeline(
    ctx: &InitCtx3D<'_>,
    bg_layout: &wgpu::BindGroupLayout,
) -> Result<wgpu::RenderPipeline> {
    let vert_shader = ctx.load_shader("smaa/neighborhood_blending.vert.spv")?;
    let frag_shader = ctx.load_shader("smaa/neighborhood_blending.frag.spv")?;
    Ok(create_pipeline(
        ctx,
        bg_layout,
        &vert_shader,
        &frag_shader,
        None,
    ))
}

fn create_pipeline(
    ctx: &InitCtx3D<'_>,
    bg_layout: &wgpu::BindGroupLayout,
    vert_shader: &wgpu::ShaderModule,
    frag_shader: &wgpu::ShaderModule,
    depth_stencil_state: Option<wgpu::DepthStencilStateDescriptor>,
) -> wgpu::RenderPipeline {
    let layout = ctx
        .device
        .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("Graphics Pipeline (SMAA Neighborhood Blending)/Layout"),
            bind_group_layouts: &[ctx.globals.bind_group_layout(), bg_layout],
            push_constant_ranges: &[wgpu::PushConstantRange {
                stages: wgpu::ShaderStage::VERTEX | wgpu::ShaderStage::FRAGMENT,
                range: 0..16,
            }],
        });

    let desc = &wgpu::RenderPipelineDescriptor {
        label: Some("Graphics Pipeline (SMAA Neighborhood Blending)"),
        layout: Some(&layout),
        vertex_stage: wgpu::ProgrammableStageDescriptor {
            module: &vert_shader,
            entry_point: "main",
        },
        fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
            module: &frag_shader,
            entry_point: "main",
        }),
        rasterization_state: Some(Default::default()),
        primitive_topology: wgpu::PrimitiveTopology::TriangleList,
        color_states: &[wgpu::TextureFormat::Bgra8Unorm.into()],
        depth_stencil_state,
        vertex_state: wgpu::VertexStateDescriptor {
            index_format: wgpu::IndexFormat::Uint32,
            vertex_buffers: &[],
        },
        sample_count: 1,
        sample_mask: !0,
        alpha_to_coverage_enabled: false,
    };

    ctx.device.create_render_pipeline(&desc)
}

fn create_search_tex_view(ctx: &InitCtx3D<'_>) -> wgpu::TextureView {
    let format = wgpu::TextureFormat::R8Unorm;
    let data = include_bytes!("smaa_search.raw");
    let tex = load_raw_texture(ctx, format, data, 66, 33, "SMAA Search Texture");
    tex.create_view(&Default::default())
}

fn create_area_tex_view(ctx: &InitCtx3D<'_>) -> wgpu::TextureView {
    let format = wgpu::TextureFormat::Rg8Unorm;
    let data = include_bytes!("smaa_area.raw");
    let tex = load_raw_texture(ctx, format, data, 160, 560, "SMAA Area Texture");
    tex.create_view(&Default::default())
}

fn load_raw_texture(
    ctx: &InitCtx3D<'_>,
    format: wgpu::TextureFormat,
    data: &[u8],
    width: u32,
    height: u32,
    label: &str,
) -> wgpu::Texture {
    let size = wgpu::Extent3d {
        width,
        height,
        depth: 1,
    };

    let texture = ctx.device.create_texture(&wgpu::TextureDescriptor {
        label: Some(label),
        size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format,
        usage: wgpu::TextureUsage::COPY_DST | wgpu::TextureUsage::SAMPLED,
    });

    let view = wgpu::TextureCopyView {
        texture: &texture,
        mip_level: 0,
        origin: wgpu::Origin3d::ZERO,
    };

    let data_layout = wgpu::TextureDataLayout {
        offset: 0,
        bytes_per_row: data.len() as u32 / height,
        rows_per_image: height,
    };

    ctx.queue.write_texture(view, &data, data_layout, size);

    texture
}
