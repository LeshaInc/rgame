use nalgebra::Matrix4;
use std::num::NonZeroU64;
use std::ops::{Deref, Range};

use fxhash::FxHashMap;
use rayon::prelude::*;

use super::{
    AnimatedMesh, AnimationSource, DrawCtx3D, DrawList3D, Instance, JointTransform, Material, Mesh,
    Pose,
};
use crate::assets::{Assets, Id};
use crate::graphics::{view_as_bytes, Texture};

#[derive(Default)]
pub struct UploadedDrawList {
    instances_buf: Option<wgpu::Buffer>,
    params_buf: Option<wgpu::Buffer>,
    tex_rects_buf: Option<wgpu::Buffer>,
    batches: FxHashMap<Id<Material>, MaterialBatch>,
    joints_bind_group: Option<wgpu::BindGroup>,
    envmap_bind_group: Option<wgpu::BindGroup>,
    // TODO: move texture grouping by atlas here
}

impl UploadedDrawList {
    pub fn create(
        ctx: &mut DrawCtx3D<'_>,
        list: &DrawList3D,
        list_label: &str,
    ) -> UploadedDrawList {
        let mut output = UploadedDrawList::default();

        ctx.encoder.push_debug_group("Upload instances");
        output.instances_buf = upload_instances(ctx, list, list_label);
        ctx.encoder.pop_debug_group();

        ctx.encoder.push_debug_group("Upload material parameters");
        output.params_buf = upload_params(ctx, list, list_label);
        ctx.encoder.pop_debug_group();

        ctx.encoder.push_debug_group("Upload texture rects");
        output.tex_rects_buf = upload_tex_rects(ctx, list, list_label);
        ctx.encoder.pop_debug_group();

        ctx.encoder.push_debug_group("Upload joints");
        let joints = process_animations(ctx.assets, list);
        let joints_buf = upload_joints(ctx, list_label, &joints);
        ctx.encoder.pop_debug_group();

        if let Some(joints_buf) = joints_buf {
            let label = format!("Draw List ({})/Joints Bind Group", list_label);
            let desc = &wgpu::BindGroupDescriptor {
                label: Some(&label),
                layout: &ctx.materials.joints_bind_group_layout,
                entries: &[wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer(joints_buf.slice(..)),
                }],
            };
            output.joints_bind_group = Some(ctx.device.create_bind_group(&desc))
        }

        if let Some(envmap_id) = list.envmap() {
            let envmap = ctx.envmaps.get(envmap_id);

            let irradiance_view = envmap.irradiance.create_view(&wgpu::TextureViewDescriptor {
                dimension: Some(wgpu::TextureViewDimension::Cube),
                ..Default::default()
            });
            let prefilter_view = envmap.prefilter.create_view(&wgpu::TextureViewDescriptor {
                dimension: Some(wgpu::TextureViewDimension::Cube),
                ..Default::default()
            });
            let brdf_lut_view = ctx
                .brdf_lut
                .create_view(&wgpu::TextureViewDescriptor::default());

            let label = format!(
                "Draw List ({})/Bind Group (EnvMap {})",
                list_label, envmap_id.inner
            );

            output.envmap_bind_group =
                Some(ctx.device.create_bind_group(&wgpu::BindGroupDescriptor {
                    label: Some(&label),
                    layout: &ctx.materials.envmap_bind_group_layout,
                    entries: &[
                        wgpu::BindGroupEntry {
                            binding: 0,
                            resource: wgpu::BindingResource::TextureView(&irradiance_view),
                        },
                        wgpu::BindGroupEntry {
                            binding: 1,
                            resource: wgpu::BindingResource::TextureView(&prefilter_view),
                        },
                        wgpu::BindGroupEntry {
                            binding: 2,
                            resource: wgpu::BindingResource::TextureView(&brdf_lut_view),
                        },
                    ],
                }));
        }

        let mut instances_offset = 0;
        let mut params_offset = 0;
        let mut tex_rects_offset = 0;
        let mut joints_offset = 0;

        for (mat_id, mat_batch) in list.iter_material_batches() {
            let material = ctx.materials.get_material(mat_id);

            let mut make_regular_batch = |batch: &super::encoder::RegularMeshBatch| {
                let instances_size =
                    batch.instances().len() as u64 * std::mem::size_of::<Instance>() as u64;
                let params_size = batch.params().len() as u64;
                let tex_rects_size = batch.textures().len() as u64 * 16;

                let batch = RegularMeshBatch {
                    instances_range: instances_offset..instances_offset + instances_size,
                    params_range: params_offset..params_offset + params_size,
                    tex_rects_range: tex_rects_offset..tex_rects_offset + tex_rects_size,
                    textures: batch.textures().to_vec(),
                    textures_per_instance: batch.textures_per_instance(),
                };

                instances_offset += instances_size;
                params_offset += params_size;
                tex_rects_offset += tex_rects_size;

                batch
            };

            let regular_meshes = mat_batch
                .iter_regular_meshes()
                .map(|(mesh_id, mesh_batch)| (mesh_id, make_regular_batch(mesh_batch)))
                .collect();

            let animated_meshes = mat_batch
                .iter_animated_meshes()
                .map(|(mesh_id, mesh_batch)| {
                    let regular = make_regular_batch(mesh_batch);

                    let joints_size =
                        MAX_JOINTS as u64 * std::mem::size_of::<JointTransform>() as u64;

                    let batch = AnimatedMeshBatch {
                        regular,
                        joints_ranges: (0..mesh_batch.animations().len())
                            .map(|_| {
                                let range = joints_offset..joints_offset + joints_size;
                                joints_offset += joints_size;
                                range
                            })
                            .collect(),
                    };

                    (mesh_id, batch)
                })
                .collect();

            output.batches.insert(
                mat_id,
                MaterialBatch {
                    name: material.name.clone(),
                    regular_meshes,
                    animated_meshes,
                },
            );
        }

        output
    }

    pub fn instances_buf(&self) -> Option<&wgpu::Buffer> {
        self.instances_buf.as_ref()
    }

    pub fn params_buf(&self) -> Option<&wgpu::Buffer> {
        self.params_buf.as_ref()
    }

    pub fn tex_rects_buf(&self) -> Option<&wgpu::Buffer> {
        self.tex_rects_buf.as_ref()
    }

    pub fn joints_bind_group(&self) -> Option<&wgpu::BindGroup> {
        self.joints_bind_group.as_ref()
    }

    pub fn envmap_bind_group(&self) -> Option<&wgpu::BindGroup> {
        self.envmap_bind_group.as_ref()
    }

    pub fn iter_batches(&self) -> impl Iterator<Item = (Id<Material>, &MaterialBatch)> + '_ {
        self.batches.iter().map(|(mat_id, batch)| (*mat_id, batch))
    }

    #[allow(dead_code)]
    pub fn par_iter_batches(
        &self,
    ) -> impl ParallelIterator<Item = (Id<Material>, &MaterialBatch)> + '_ {
        self.batches
            .par_iter()
            .map(|(mat_id, batch)| (*mat_id, batch))
    }
}

pub struct MaterialBatch {
    name: String,
    regular_meshes: FxHashMap<Id<Mesh>, RegularMeshBatch>,
    animated_meshes: FxHashMap<Id<AnimatedMesh>, AnimatedMeshBatch>,
}

impl MaterialBatch {
    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn iter_all_meshes(&self) -> impl Iterator<Item = &RegularMeshBatch> + '_ {
        let regular = self.iter_regular_meshes().map(|(_, batch)| batch);
        let animated = self
            .iter_animated_meshes()
            .map(|(_, batch)| &batch as &RegularMeshBatch);
        regular.chain(animated)
    }

    pub fn iter_regular_meshes(&self) -> impl Iterator<Item = (Id<Mesh>, &RegularMeshBatch)> + '_ {
        self.regular_meshes
            .iter()
            .map(|(mesh_id, subbatch)| (*mesh_id, subbatch))
    }

    pub fn iter_animated_meshes(
        &self,
    ) -> impl Iterator<Item = (Id<AnimatedMesh>, &AnimatedMeshBatch)> + '_ {
        self.animated_meshes
            .iter()
            .map(|(mesh_id, subbatch)| (*mesh_id, subbatch))
    }
}

pub struct RegularMeshBatch {
    instances_range: Range<u64>,
    params_range: Range<u64>,
    tex_rects_range: Range<u64>,
    textures: Vec<Id<Texture>>,
    textures_per_instance: u8,
}

impl RegularMeshBatch {
    pub fn instances_range(&self) -> Range<u64> {
        self.instances_range.clone()
    }

    pub fn num_instances(&self) -> u32 {
        ((self.instances_range.end - self.instances_range.start)
            / std::mem::size_of::<Instance>() as u64) as u32
    }

    pub fn params_range(&self) -> Range<u64> {
        self.params_range.clone()
    }

    pub fn tex_rects_range(&self) -> Range<u64> {
        self.tex_rects_range.clone()
    }

    pub fn textures(&self) -> impl Iterator<Item = &[Id<Texture>]> + '_ {
        if self.textures_per_instance == 0 {
            either::Left((0..self.num_instances()).map(|_| &[] as &[Id<Texture>]))
        } else {
            either::Right(self.textures.chunks(self.textures_per_instance as _))
        }
    }

    pub fn textures_per_instance(&self) -> u8 {
        self.textures_per_instance
    }
}

const MAX_JOINTS: usize = 256;

pub struct AnimatedMeshBatch {
    regular: RegularMeshBatch,
    joints_ranges: Vec<Range<u64>>,
}

impl AnimatedMeshBatch {
    pub fn joints_ranges(&self) -> &[Range<u64>] {
        &self.joints_ranges
    }
}

impl Deref for AnimatedMeshBatch {
    type Target = RegularMeshBatch;

    fn deref(&self) -> &RegularMeshBatch {
        &self.regular
    }
}

fn upload_instances(
    ctx: &mut DrawCtx3D,
    list: &DrawList3D,
    list_label: &str,
) -> Option<wgpu::Buffer> {
    let num_instances = get_num_instances(list);
    if num_instances == 0 {
        return None;
    }

    let size = get_instances_buf_size(num_instances);
    let buf = create_instances_buf(ctx, list_label, size);
    write_instances_buf(ctx, list, size, &buf);

    Some(buf)
}

fn get_num_instances(list: &DrawList3D) -> usize {
    list.par_iter_material_batches()
        .map(|(_, b)| {
            let regular = b
                .iter_regular_meshes()
                .map(|(_, sb)| sb.instances().len())
                .sum::<usize>();
            let animated = b
                .iter_animated_meshes()
                .map(|(_, sb)| sb.instances().len())
                .sum::<usize>();
            regular + animated
        })
        .sum()
}

fn get_instances_buf_size(num_instances: usize) -> u64 {
    let instance_size = std::mem::size_of::<Instance>();
    num_instances as u64 * instance_size as u64
}

fn create_instances_buf(ctx: &mut DrawCtx3D<'_>, list_label: &str, size: u64) -> wgpu::Buffer {
    create_drawlist_buf(
        ctx,
        list_label,
        wgpu::BufferUsage::VERTEX,
        size,
        "Instances Buffer",
    )
}

fn write_instances_buf(ctx: &mut DrawCtx3D<'_>, list: &DrawList3D, size: u64, buf: &wgpu::Buffer) {
    let nz_size = NonZeroU64::new(size).unwrap();
    let belt = &mut ctx.belt;
    let mut view = belt.write_buffer(ctx.encoder, &buf, 0, nz_size, ctx.device);

    let mut offset = 0;
    for (_, batch) in list.iter_material_batches() {
        for (_, subbatch) in batch.iter_regular_meshes() {
            let bytes = view_as_bytes(subbatch.instances());
            view[offset..offset + bytes.len()].copy_from_slice(bytes);
            offset += bytes.len();
        }

        for (_, subbatch) in batch.iter_animated_meshes() {
            let bytes = view_as_bytes(subbatch.instances());
            view[offset..offset + bytes.len()].copy_from_slice(bytes);
            offset += bytes.len();
        }
    }
}

fn upload_params(ctx: &mut DrawCtx3D, list: &DrawList3D, list_label: &str) -> Option<wgpu::Buffer> {
    let size = get_params_buf_size(list);
    if size == 0 {
        return None;
    }

    let buf = create_params_buf(ctx, list_label, size);
    write_params_buf(ctx, list, size, &buf);

    Some(buf)
}

fn get_params_buf_size(list: &DrawList3D) -> u64 {
    list.par_iter_material_batches()
        .map(|(_, b)| {
            let regular = b
                .iter_regular_meshes()
                .map(|(_, sb)| sb.params().len() as u64)
                .sum::<u64>();
            let animated = b
                .iter_animated_meshes()
                .map(|(_, sb)| sb.params().len() as u64)
                .sum::<u64>();
            regular + animated
        })
        .sum()
}

fn create_params_buf(ctx: &mut DrawCtx3D<'_>, list_label: &str, size: u64) -> wgpu::Buffer {
    create_drawlist_buf(
        ctx,
        list_label,
        wgpu::BufferUsage::VERTEX,
        size,
        "Material Parameters Buffer",
    )
}

fn write_params_buf(ctx: &mut DrawCtx3D<'_>, list: &DrawList3D, size: u64, buf: &wgpu::Buffer) {
    let nz_size = NonZeroU64::new(size).unwrap();
    let belt = &mut ctx.belt;
    let mut view = belt.write_buffer(ctx.encoder, &buf, 0, nz_size, ctx.device);

    let mut offset = 0;
    for (_, batch) in list.iter_material_batches() {
        for (_, subbatch) in batch.iter_regular_meshes() {
            let bytes = subbatch.params();
            view[offset..offset + bytes.len()].copy_from_slice(bytes);
            offset += bytes.len();
        }

        for (_, subbatch) in batch.iter_animated_meshes() {
            let bytes = subbatch.params();
            view[offset..offset + bytes.len()].copy_from_slice(bytes);
            offset += bytes.len();
        }
    }
}

fn upload_tex_rects(
    ctx: &mut DrawCtx3D<'_>,
    list: &DrawList3D,
    list_label: &str,
) -> Option<wgpu::Buffer> {
    let size = get_tex_rects_buf_size(list);
    if size == 0 {
        return None;
    }

    let buf = create_tex_rects_buf(ctx, list_label, size);
    write_tex_rects_buf(ctx, list, size, &buf);

    Some(buf)
}

fn get_tex_rects_buf_size(list: &DrawList3D) -> u64 {
    list.par_iter_material_batches()
        .map(|(_, b)| {
            let regular = b
                .iter_regular_meshes()
                .map(|(_, sb)| sb.textures().len() as u64 * 16)
                .sum::<u64>();
            let animated = b
                .iter_animated_meshes()
                .map(|(_, sb)| sb.textures().len() as u64 * 16)
                .sum::<u64>();
            regular + animated
        })
        .sum()
}

fn create_tex_rects_buf(ctx: &mut DrawCtx3D<'_>, list_label: &str, size: u64) -> wgpu::Buffer {
    create_drawlist_buf(
        ctx,
        list_label,
        wgpu::BufferUsage::VERTEX,
        size,
        "Texture Rects Buffer",
    )
}

fn write_tex_rects_buf(ctx: &mut DrawCtx3D<'_>, list: &DrawList3D, size: u64, buf: &wgpu::Buffer) {
    let nz_size = NonZeroU64::new(size).unwrap();
    let belt = &mut ctx.belt;
    let mut view = belt.write_buffer(ctx.encoder, &buf, 0, nz_size, ctx.device);

    let mut offset = 0;
    for (_, batch) in list.iter_material_batches() {
        let textures0 = batch
            .iter_regular_meshes()
            .flat_map(|(_, sb)| sb.textures());
        let textures1 = batch
            .iter_animated_meshes()
            .flat_map(|(_, sb)| sb.textures());
        let textures = textures0.chain(textures1);

        for &tex_id in textures {
            let rect = ctx.textures.get_texture_rect(tex_id).unwrap_or([0.0; 4]);
            view[offset..offset + 16].copy_from_slice(view_as_bytes(&rect));
            offset += 16;
        }
    }
}

fn process_animations(assets: &Assets, list: &DrawList3D) -> Vec<Vec<Matrix4<f32>>> {
    list.iter_material_batches()
        .flat_map(|(_, b)| b.iter_animated_meshes())
        .flat_map(|(mesh_id, sb)| {
            let animations = sb.animations().iter();
            animations.map(move |anim| (mesh_id, anim))
        })
        .map(|(mesh_id, anim)| {
            let mesh = match assets.get_by_id(mesh_id) {
                Some(v) => v,
                None => return vec![Matrix4::identity(); 256],
            };

            let mut out_pose = Pose::from(mesh.joints.as_slice());

            for (alpha, source) in anim.sources() {
                let alpha = *alpha;
                match source {
                    AnimationSource::Pose(pose) => {
                        out_pose.lerp(&pose, alpha);
                    }
                    AnimationSource::Bundled(anim_name, time) => {
                        let anim = match mesh.animations.get(anim_name.as_str()) {
                            Some(v) => v,
                            None => {
                                tracing::warn!(
                                    mesh_id = mesh_id.inner,
                                    "No such animation: {}",
                                    anim_name
                                );

                                continue;
                            }
                        };

                        let frames = &anim.frames;

                        // TODO: proper fps and looping
                        let frame = (time * 60.0) % (frames.len() as f32 - 1.0).max(1.0);

                        if frame.trunc() - frame < 1e-5 {
                            out_pose.lerp(&frames[frame.trunc() as usize], alpha);
                        } else {
                            let fract = frame.fract();
                            out_pose.lerp(&frames[frame.trunc() as usize], alpha * (1.0 - fract));
                            out_pose.lerp(&frames[frame.trunc() as usize + 1], alpha * fract);
                        }
                    }
                }
            }

            let mut joint_matrices = vec![Matrix4::identity(); 256]; // TODO: make this an array?
            let mut stack = Vec::with_capacity(16);
            stack.push((mesh.root_joint, mesh.skeleton_transform));

            while let Some((joint_id, parent_transform)) = stack.pop() {
                let joint_transform = parent_transform * out_pose.joints[&joint_id].to_matrix();
                let joint = &mesh.joints[joint_id.0 as usize];
                let inv_bind_matrix = joint.inv_bind_matrix;
                joint_matrices[joint_id.0 as usize] = joint_transform * inv_bind_matrix;

                for &child_id in &joint.children {
                    stack.push((child_id, joint_transform));
                }
            }

            joint_matrices
        })
        .collect()
}

fn upload_joints(
    ctx: &mut DrawCtx3D<'_>,
    list_label: &str,
    joints: &[Vec<Matrix4<f32>>],
) -> Option<wgpu::Buffer> {
    let size = get_joints_buf_size(joints);
    if size == 0 {
        return None;
    }

    let buf = create_joints_buf(ctx, list_label, size);
    write_joints_buf(ctx, joints, size, &buf);

    Some(buf)
}

fn get_joints_buf_size(joints: &[Vec<Matrix4<f32>>]) -> u64 {
    (joints.len() * std::mem::size_of::<Matrix4<f32>>() * 256) as u64
}

fn create_joints_buf(ctx: &mut DrawCtx3D<'_>, list_label: &str, size: u64) -> wgpu::Buffer {
    create_drawlist_buf(
        ctx,
        list_label,
        wgpu::BufferUsage::UNIFORM,
        size,
        "Joints Buffer",
    )
}

fn write_joints_buf(
    ctx: &mut DrawCtx3D<'_>,
    joints: &[Vec<Matrix4<f32>>],
    size: u64,
    buf: &wgpu::Buffer,
) {
    let nz_size = NonZeroU64::new(size).unwrap();
    let mut view = ctx
        .belt
        .write_buffer(ctx.encoder, &buf, 0, nz_size, ctx.device);

    let mut offset = 0;
    for joints in joints {
        let bytes = view_as_bytes(&joints);
        view[offset..offset + bytes.len()].copy_from_slice(bytes);
        offset += bytes.len();
    }
}

fn create_drawlist_buf(
    ctx: &mut DrawCtx3D<'_>,
    list_label: &str,
    usage: wgpu::BufferUsage,
    size: u64,
    buf_label: &str,
) -> wgpu::Buffer {
    let label = format!("Draw List ({})/{}", list_label, buf_label);
    let desc = wgpu::BufferDescriptor {
        label: Some(&label),
        size,
        usage: usage | wgpu::BufferUsage::COPY_DST,
        mapped_at_creation: false,
    };
    ctx.device.create_buffer(&desc)
}
