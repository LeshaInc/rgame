use bumpalo::Bump;
use nalgebra::{Matrix4, Vector2, Vector3, Vector4};

#[derive(Clone, Copy, Debug, Default)]
#[repr(C)]
pub struct MeshVertex {
    pub pos: Vector3<f32>,
    pub normal: Vector3<f32>,
    pub tex: Vector2<f32>,
}

impl MeshVertex {
    pub const ATTRIBUTES: &'static [wgpu::VertexFormat] = &[
        wgpu::VertexFormat::Float3,
        wgpu::VertexFormat::Float3,
        wgpu::VertexFormat::Float2,
    ];
}

#[derive(Clone, Copy, Debug, Default)]
#[repr(C)]
pub struct AnimatedMeshVertex {
    pub pos: Vector3<f32>,
    pub normal: Vector3<f32>,
    pub tex: Vector2<f32>,
    pub weights: [f32; 4],
    pub joints: [u8; 4],
}

impl AnimatedMeshVertex {
    pub const ATTRIBUTES: &'static [wgpu::VertexFormat] = &[
        wgpu::VertexFormat::Float3,
        wgpu::VertexFormat::Float3,
        wgpu::VertexFormat::Float2,
        wgpu::VertexFormat::Float4,
        wgpu::VertexFormat::Uint,
    ];
}

#[derive(Clone, Copy, Debug, Default)]
#[repr(C)]
pub struct Instance {
    pub model: [Vector4<f32>; 3],
}

impl Instance {
    pub fn new(model: Matrix4<f32>) -> Instance {
        let rows = model.transpose();
        Instance {
            model: [
                rows.column(0).clone_owned(),
                rows.column(1).clone_owned(),
                rows.column(2).clone_owned(),
            ],
        }
    }

    pub fn model(&self) -> Matrix4<f32> {
        Matrix4::from_columns(&[
            self.model[0],
            self.model[1],
            self.model[2],
            Vector4::new(0.0, 0.0, 0.0, 1.0),
        ])
        .transpose()
    }

    pub const ATTRIBUTES: &'static [wgpu::VertexFormat] = &[
        wgpu::VertexFormat::Float4,
        wgpu::VertexFormat::Float4,
        wgpu::VertexFormat::Float4,
    ];
}

pub fn make_vbuf_desc<'a>(
    bump: &'a Bump,
    attributes: &[wgpu::VertexFormat],
    step_mode: wgpu::InputStepMode,
    shader_location: &mut u32,
) -> wgpu::VertexBufferDescriptor<'a> {
    let mut offset = 0;
    let attributes = bump.alloc_slice_fill_iter(attributes.iter().map(|&format| {
        let attr = wgpu::VertexAttributeDescriptor {
            offset,
            format,
            shader_location: *shader_location,
        };
        offset += format.size();
        *shader_location += 1;
        attr
    }));

    wgpu::VertexBufferDescriptor {
        stride: offset,
        step_mode,
        attributes,
    }
}
