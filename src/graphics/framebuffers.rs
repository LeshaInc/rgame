use fxhash::FxHashMap;
use winit::dpi::PhysicalSize;

#[derive(Debug)]
struct Framebuffer {
    texture: Option<(wgpu::Texture, wgpu::TextureView)>,
    format: wgpu::TextureFormat,
    usage: wgpu::TextureUsage,
    scale: f32,
}

#[derive(Debug)]
pub struct Framebuffers {
    map: FxHashMap<&'static str, Framebuffer>,
    resoultion: PhysicalSize<u32>,
    swapchain_texture: Option<wgpu::SwapChainTexture>,
}

impl Framebuffers {
    pub fn new(resoultion: PhysicalSize<u32>) -> Framebuffers {
        Framebuffers {
            map: FxHashMap::default(),
            resoultion,
            swapchain_texture: None,
        }
    }

    pub fn register(
        &mut self,
        name: &'static str,
        format: wgpu::TextureFormat,
        usage: wgpu::TextureUsage,
        scale: f32,
    ) {
        self.map.insert(
            name,
            Framebuffer {
                texture: None,
                format,
                usage,
                scale,
            },
        );
    }

    pub fn maintain(&mut self, device: &wgpu::Device, resoultion: PhysicalSize<u32>) {
        for (name, entry) in &mut self.map {
            if entry.texture.is_none() || resoultion != self.resoultion {
                let label = format!("Framebuffer ({})/Texture", name);
                let desc = wgpu::TextureDescriptor {
                    label: Some(&label),
                    size: wgpu::Extent3d {
                        width: ((resoultion.width as f32) * entry.scale) as u32,
                        height: ((resoultion.height as f32) * entry.scale) as u32,
                        depth: 1,
                    },
                    mip_level_count: 1,
                    sample_count: 1,
                    dimension: wgpu::TextureDimension::D2,
                    format: entry.format,
                    usage: entry.usage,
                };
                let texture = device.create_texture(&desc);

                let label = format!("Framebuffer ({})/View", name);
                let desc = wgpu::TextureViewDescriptor {
                    label: Some(&label),
                    format: None,
                    dimension: Some(wgpu::TextureViewDimension::D2),
                    aspect: wgpu::TextureAspect::All,
                    base_mip_level: 0,
                    level_count: None,
                    base_array_layer: 0,
                    array_layer_count: None,
                };
                let view = texture.create_view(&desc);

                entry.texture = Some((texture, view));
            }
        }
    }

    pub fn get(&self, name: &'static str) -> &wgpu::Texture {
        &self.map[&name].texture.as_ref().unwrap().0
    }

    pub fn get_view(&self, name: &'static str) -> &wgpu::TextureView {
        &self.map[&name].texture.as_ref().unwrap().1
    }

    pub fn set_swapchain_texture(&mut self, tex: wgpu::SwapChainTexture) {
        self.swapchain_texture = Some(tex);
    }

    pub fn get_swapchain_view(&self) -> &wgpu::TextureView {
        &self.swapchain_texture.as_ref().unwrap().view
    }

    pub fn take_swapchain_texture(&mut self) -> wgpu::SwapChainTexture {
        self.swapchain_texture.take().unwrap()
    }
}
