use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub enum Backend {
    Any,
    Vulkan,
    Metal,
    Dx12,
    Dx11,
    Gl,
}

impl From<Backend> for wgpu::BackendBit {
    fn from(v: Backend) -> wgpu::BackendBit {
        match v {
            Backend::Any => wgpu::BackendBit::all(),
            Backend::Vulkan => wgpu::BackendBit::VULKAN,
            Backend::Metal => wgpu::BackendBit::METAL,
            Backend::Dx12 => wgpu::BackendBit::DX12,
            Backend::Dx11 => wgpu::BackendBit::DX11,
            Backend::Gl => wgpu::BackendBit::GL,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ChosenAdapter {
    name: String,
    vendor: usize,
    device: usize,
    device_type: wgpu::DeviceType,
}

impl ChosenAdapter {
    pub fn from_info(info: wgpu::AdapterInfo) -> ChosenAdapter {
        ChosenAdapter {
            name: info.name,
            vendor: info.vendor,
            device: info.device,
            device_type: info.device_type,
            // there is also info.backend but we don't store it here
        }
    }

    pub fn matches_info(&self, info: &wgpu::AdapterInfo) -> bool {
        self.name == info.name
            && self.vendor == info.vendor
            && self.device == info.device
            && self.device_type == info.device_type
    }
}

#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub enum SmaaPreset {
    Low,
    Medium,
    High,
    Ultra,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct GraphicsSettings {
    pub backend: Backend,
    #[serde(default)]
    pub adapter: Option<ChosenAdapter>,
    pub present_mode: wgpu::PresentMode,
    pub smaa_preset: Option<SmaaPreset>,
}

impl Default for GraphicsSettings {
    fn default() -> GraphicsSettings {
        GraphicsSettings {
            backend: Backend::Any,
            adapter: None,
            present_mode: wgpu::PresentMode::Fifo,
            smaa_preset: None,
        }
    }
}
