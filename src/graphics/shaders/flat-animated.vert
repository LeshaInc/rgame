#version 450

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTex;
layout(location = 3) in vec4 inWeights;
layout(location = 4) in uint inJoints;

layout(location = 5) in vec4 inModelRow0;
layout(location = 6) in vec4 inModelRow1;
layout(location = 7) in vec4 inModelRow2;

layout(location = 8) in vec3 inFlatColor;

layout(location = 0) out vec3 vNormal;
layout(location = 1) out vec3 vFlatColor;

layout(set = 0, binding = 0, std140) uniform Globals {
    mat4 view;
    mat4 projection;
    mat4 viewProjection;
    vec3 cameraPos;
} uGlobals;

layout(set = 1, binding = 0, std140) uniform Joints {
    mat4 uJoints[256];
};

void main() {
    mat4 skinMatrix = 
          inWeights.x * uJoints[inJoints & 0xFF]
        + inWeights.y * uJoints[(inJoints >> 8) & 0xFF]
        + inWeights.z * uJoints[(inJoints >> 16) & 0xFF]
        + inWeights.w * uJoints[(inJoints >> 24) & 0xFF];

    mat4 model = transpose(mat4(
        inModelRow0,
        inModelRow1,
        inModelRow2,
        vec4(0, 0, 0, 1)
    ));

    mat4 skinModel = model * skinMatrix;
    vec4 worldPos = skinModel * vec4(inPos, 1);

    vNormal = (skinModel * vec4(inNormal, 0)).xyz;
    vFlatColor = inFlatColor;

    gl_Position = uGlobals.viewProjection * worldPos;
}
