#version 450

layout(location = 0) in vec3 vNormal;
layout(location = 1) in vec3 vFlatColor;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0, std140) uniform Globals {
    mat4 view;
    mat4 projection;
    mat4 viewProjection;
    vec3 cameraPos;
    vec3 cameraDir;
} uGlobals;

layout(set = 0, binding = 1) uniform sampler uLinearSampler;
layout(set = 0, binding = 2) uniform sampler uNearestSampler;

void main() {
    vec3 normal = normalize(vNormal);
    vec3 lightDir = normalize(vec3(1.0, -2.0, 2));
    float diffuse = max(dot(normal, lightDir), 0.0);
    vec3 color = min(0.3 + diffuse, 1.0) * vFlatColor;
    outColor = vec4(color, 1.0);
}
