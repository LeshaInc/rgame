#version 450

layout(location = 0) in vec2 vTex;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 2) uniform sampler uNearestSampler;
layout(set = 1, binding = 0) uniform texture2D uColorTex;

void main() {
    vec4 color = texture(sampler2D(uColorTex, uNearestSampler), vTex);

    float gamma = 2.2;
    outColor = vec4(pow(color.rgb, vec3(1.0 / gamma)), color.a);
}
