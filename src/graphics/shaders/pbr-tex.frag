#version 450

layout(location = 0) in vec3 vNormal;
layout(location = 1) in vec3 vPosition;
layout(location = 2) in vec2 vTex0;
layout(location = 3) in vec2 vTex1;
layout(location = 4) in vec2 vTex2;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0, std140) uniform Globals {
    mat4 view;
    mat4 projection;
    mat4 viewProjection;
    vec3 cameraPos;
    vec3 cameraDir;
} uGlobals;

layout(set = 0, binding = 1) uniform sampler uLinearSampler;
layout(set = 0, binding = 2) uniform sampler uNearestSampler;

layout(set = 1, binding = 0) uniform texture2D uAlbedo;
layout(set = 1, binding = 1) uniform texture2D uMetalness;
layout(set = 1, binding = 2) uniform texture2D uRoughness;

layout(set = 2, binding = 0) uniform textureCube uIrradianceMap;
layout(set = 2, binding = 1) uniform textureCube uPrefilterMap;
layout(set = 2, binding = 2) uniform texture2D uBRDFLUT;

#include "pbr.glsl"

void main() {
    vec3 albedo = texture(sampler2D(uAlbedo, uLinearSampler), vTex0).rgb;
    float metalness = texture(sampler2D(uMetalness, uLinearSampler), vTex1).r;
    float roughness = texture(sampler2D(uRoughness, uLinearSampler), vTex2).r;

    vec3 normal = normalize(vNormal);

    vec3 lightColor = vec3(1.0, 1.0, 1.0) * 5.0;
    vec3 lightDir = normalize(vec3(1.0, -2.0, 2));

    vec3 radiance = lightColor;

    vec3 viewDir = normalize(-uGlobals.cameraDir);
    vec3 halfway = normalize(viewDir + lightDir);
    vec3 reflDir = reflect(-viewDir, normal);

    float aNV = max(dot(normal, viewDir), 0.001);
    float aNL = max(dot(normal, lightDir), 0.0);
    float aNH = max(dot(normal, halfway), 0.0);

    vec3 R0 = mix(vec3(0.04), albedo, metalness);

    float D = normalDistributionGGX(aNH, roughness);
    vec3  F = fresnel(aNV, R0);
    float G = geometryGGX(aNV, aNL, roughness);
    vec3 DFG = D * F * G;

    vec3 specular = DFG / max(4.0 * aNV * aNL, 0.001);

    vec3 kS = vec3(F);
    vec3 kD = (vec3(1.0) - kS) * (1.0 - metalness);

    vec3 lightOut = (kD * albedo / PI + specular) * radiance * aNL;

    F = fresnelRoughness(max(dot(normal, viewDir), 0.0), R0, roughness);
    
    kS = F;
    kD = (1.0 - kS) * (1.0 - metalness);
    
    vec3 irradiance = texture(samplerCube(uIrradianceMap, uLinearSampler), -normal).rgb;
    vec3 diffuse = irradiance * albedo;
    
    float maxReflLod = float(textureQueryLevels(samplerCube(uPrefilterMap, uLinearSampler)));
    vec3 prefilteredColor = textureLod(samplerCube(uPrefilterMap, uLinearSampler), -reflDir, roughness * maxReflLod).rgb;    
    vec2 brdf = texture(sampler2D(uBRDFLUT, uLinearSampler), vec2(max(dot(normal, viewDir), 0.0), roughness)).rg;
    specular = prefilteredColor * (F * brdf.x + brdf.y);

    vec3 ambient = kD * diffuse + specular;
    vec3 color = ambient + lightOut;

    outColor = vec4(color, 1.0);
}
