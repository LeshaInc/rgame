#version 450

layout(location = 0) in vec3 vNormal;
layout(location = 1) in vec3 vPosition;
layout(location = 2) in vec3 vAlbedo;
layout(location = 3) in float vRoughness;
layout(location = 4) in float vMetalness;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0, std140) uniform Globals {
    mat4 view;
    mat4 projection;
    mat4 viewProjection;
    vec3 cameraPos;
    vec3 cameraDir;
} uGlobals;

layout(set = 0, binding = 1) uniform sampler uLinearSampler;
layout(set = 0, binding = 2) uniform sampler uNearestSampler;

layout(set = 1, binding = 0) uniform textureCube uIrradianceMap;
layout(set = 1, binding = 1) uniform textureCube uPrefilterMap;
layout(set = 1, binding = 2) uniform texture2D uBRDFLUT;

#include "pbr.glsl"

void main() {
    float roughness = vRoughness;
    vec3 normal = normalize(vNormal);

    vec3 lightColor = vec3(1.0, 1.0, 1.0) * 5.0;
    vec3 lightDir = normalize(vec3(1.0, -2.0, 2));

    vec3 radiance = lightColor;

    // TODO: move light computations to pbr.glsl

    vec3 viewDir = normalize(-uGlobals.cameraDir);
    vec3 halfway = normalize(viewDir + lightDir);
    vec3 reflDir = reflect(-viewDir, normal);

    float aNV = max(dot(normal, viewDir), 0.001);
    float aNL = max(dot(normal, lightDir), 0.0);
    float aNH = max(dot(normal, halfway), 0.0);

    vec3 R0 = mix(vec3(0.04), vAlbedo, vMetalness);

    float D = normalDistributionGGX(aNH, roughness);
    vec3  F = fresnel(aNV, R0);
    float G = geometryGGX(aNV, aNL, roughness);
    vec3 DFG = D * F * G;

    vec3 specular = DFG / max(4.0 * aNV * aNL, 0.001);

    vec3 kS = vec3(F);
    vec3 kD = (vec3(1.0) - kS) * (1.0 - vMetalness);

    vec3 lightOut = (kD * vAlbedo / PI + specular) * radiance * aNL;

    F = fresnelRoughness(max(dot(normal, viewDir), 0.0), R0, roughness);
    
    kS = F;
    kD = (1.0 - kS) * (1.0 - vMetalness);
    
    vec3 irradiance = texture(samplerCube(uIrradianceMap, uLinearSampler), -normal).rgb;
    vec3 diffuse = irradiance * vAlbedo;
    
    float maxReflLod = float(textureQueryLevels(samplerCube(uPrefilterMap, uLinearSampler)));
    vec3 prefilteredColor = textureLod(samplerCube(uPrefilterMap, uLinearSampler), -reflDir, roughness * maxReflLod).rgb;    
    vec2 brdf = texture(sampler2D(uBRDFLUT, uLinearSampler), vec2(max(dot(normal, viewDir), 0.0), roughness)).rg;
    specular = prefilteredColor * (F * brdf.x + brdf.y);

    vec3 ambient = kD * diffuse + specular;
    vec3 color = ambient + lightOut;

    outColor = vec4(color, 1.0);
}
