const float PI = 3.14159265359;

// Schlick's approximation of the specular reflection coefficient
// * `aNV` - cosine of the angle between the surface normal and the view direction
// * `R0` - base reflectivity
vec3 fresnel(float aNV, vec3 R0) {
    return R0 + (1.0 - R0) * pow(1.0 - aNV, 5.0);
}

vec3 fresnelRoughness(float aNV, vec3 R0, float roughness) {
    return R0 + (max(vec3(1.0 - roughness), R0) - R0) * pow(max(1.0 - aNV, 0.0), 5.0);
}

// Trowbridge-Reitz GGX normal distribution function
// * `aNH` - cosine of the angle between the surface normal and the halfway vector
float normalDistributionGGX(float aNH, float roughness) {
    float roughnessSq = roughness * roughness;
    float a = aNH * aNH * (roughnessSq - 1.0) + 1.0;
    return roughnessSq / (PI * a * a);
}

// Schlick GGX geometry function
float geometryGGXSub(float angle, float roughness) {
    roughness += 1;
    float k = roughness * roughness / 4.0;
    return angle / (angle * (1.0 - k) + k);
}

// Schlick-Smith GGX geometry function
// * `aNV` - cosine of the angle between the surface normal and the view direction
// * `aNL` - cosine of the angle between the surface normal and the light direction
float geometryGGX(float aNV, float aNL, float roughness) {
    return geometryGGXSub(aNV, roughness) * geometryGGXSub(aNL, roughness);
}
