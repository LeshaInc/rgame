#version 450

layout(location = 0) in vec2 vTex;
layout(location = 1) in vec2 vPix;
layout(location = 2) in vec4 vOffset[3];

layout(location = 0) out vec4 outColor;

layout(push_constant) uniform PushConstant {
    vec4 SMAA_RT_METRICS;
};

layout(set = 0, binding = 1) uniform sampler uSampler;
layout(set = 1, binding = 0) uniform texture2D uEdgesTex;
layout(set = 1, binding = 1) uniform texture2D uAreaTex;
layout(set = 1, binding = 2) uniform texture2D uSearchTex;

#define SMAA_GLSL_4 1
#define SMAA_SAMPLER uSampler
#define SMAA_PRESET_ULTRA
#define SMAA_INCLUDE_VS 0
#define SMAA_INCLUDE_PS 1
#include "SMAA.glsl"

void main() {
    outColor = SMAABlendingWeightCalculationPS(vTex, vPix, vOffset, uEdgesTex, uAreaTex, uSearchTex, vec4(0.0));
}
