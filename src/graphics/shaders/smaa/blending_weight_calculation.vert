#version 450

layout(location = 0) out vec2 vTex;
layout(location = 1) out vec2 vPix;
layout(location = 2) out vec4 vOffset[3];

layout(push_constant) uniform PushConstant {
    vec4 SMAA_RT_METRICS;
};

layout(set = 0, binding = 1) uniform sampler uSampler;

#define SMAA_GLSL_4 1
#define SMAA_SAMPLER uSampler
#define SMAA_PRESET_ULTRA
#define SMAA_INCLUDE_VS 1
#define SMAA_INCLUDE_PS 0
#include "SMAA.glsl"

void main() {
    float x = -1.0 + float((gl_VertexIndex & 1) << 2);
    float y = -1.0 + float((gl_VertexIndex & 2) << 1);
    vec2 tex = vec2((x + 1) * 0.5, (-y + 1) * 0.5);
    vTex = tex;
    SMAABlendingWeightCalculationVS(vTex, vPix, vOffset);
    gl_Position = vec4(x, y, 0, 1);
}
