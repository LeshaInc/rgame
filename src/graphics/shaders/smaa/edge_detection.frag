#version 450

layout(location = 0) in vec2 vTex;
layout(location = 1) in vec4 vOffset[3];

layout(location = 0) out vec2 outColor;

layout(push_constant) uniform PushConstant {
    vec4 SMAA_RT_METRICS;
};

layout(set = 0, binding = 1) uniform sampler uSampler;
layout(set = 1, binding = 0) uniform texture2D uColorTex;

#define SMAA_GLSL_4 1
#define SMAA_SAMPLER uSampler
#define SMAA_PRESET_ULTRA
#define SMAA_INCLUDE_VS 0
#define SMAA_INCLUDE_PS 1
#include "SMAA.glsl"

void main() {
    outColor = SMAALumaEdgeDetectionPS(vTex, vOffset, uColorTex);
}
