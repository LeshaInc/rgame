#version 450

layout(location = 0) in vec2 vTex;
layout(location = 1) in vec4 vOffset;

layout(location = 0) out vec4 outColor;

layout(push_constant) uniform PushConstant {
    vec4 SMAA_RT_METRICS;
};

layout(set = 0, binding = 1) uniform sampler uSampler;
layout(set = 1, binding = 0) uniform texture2D uColorTex;
layout(set = 1, binding = 1) uniform texture2D uBlendTex;

#define SMAA_GLSL_4 1
#define SMAA_SAMPLER uSampler
#define SMAA_PRESET_ULTRA
#define SMAA_INCLUDE_VS 0
#define SMAA_INCLUDE_PS 1
#include "SMAA.glsl"

void main() {
    outColor = SMAANeighborhoodBlendingPS(vTex, vOffset, uColorTex, uBlendTex);
}
