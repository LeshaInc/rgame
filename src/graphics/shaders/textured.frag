#version 450

layout(location = 0) in vec3 vNormal;
layout(location = 1) in vec2 vTex;
layout(location = 2) in vec3 vTint;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0, std140) uniform Globals {
    mat4 uView;
    mat4 uProjection;
    mat4 uViewProjection;
    vec3 uCameraPos;
    vec3 cameraDir;
};

layout(set = 0, binding = 1) uniform sampler uLinearSampler;
layout(set = 0, binding = 2) uniform sampler uNearestSampler;

layout(set = 1, binding = 0) uniform texture2D uTexture;

void main() {
    vec4 texColor = texture(sampler2D(uTexture, uLinearSampler), vTex);

    vec3 lightDir = vec3(1.0, -1.0, -0.5);
    float diffuse = max(dot(vNormal, lightDir), 0.0);
    vec3 color = (0.3 + diffuse) * texColor.rgb * vTint;

    outColor = vec4(color, 1.0);
}
