#version 450

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTex;

layout(location = 3) in vec4 inModelRow0;
layout(location = 4) in vec4 inModelRow1;
layout(location = 5) in vec4 inModelRow2;

layout(location = 6) in vec3 inFlatColor;

layout(location = 7) in vec4 inTexRect;

layout(location = 0) out vec3 vNormal;
layout(location = 1) out vec2 vTex;
layout(location = 2) out vec3 vFlatColor;

layout(set = 0, binding = 0, std140) uniform Globals {
    mat4 view;
    mat4 projection;
    mat4 viewProjection;
    vec3 cameraPos;
    vec3 cameraDir;
} uGlobals;

void main() {
    mat4 model = transpose(mat4(
        inModelRow0,
        inModelRow1,
        inModelRow2,
        vec4(0, 0, 0, 1)
    ));

    vec4 worldPos = model * vec4(inPos, 1);

    vNormal = (model * vec4(inNormal, 0)).xyz;
    vTex = inTex * inTexRect.zw + inTexRect.xy;
    vFlatColor = inFlatColor;

    gl_Position = uGlobals.viewProjection * worldPos;
}
