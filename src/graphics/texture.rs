use anyhow::{bail, Context, Result};
use png::{BitDepth, ColorType, Decoder, Transformations};

use crate::assets::{Asset, AssetDefaultLoader, AssetReader, SimpleAssetLoader};

#[derive(Clone, Debug)]
pub struct Texture {
    pub format: TextureFormat,
    /// Texture data, or None if it's already uploaded on GPU
    pub data: Option<TextureData>,
}

#[derive(Clone, Debug)]
pub struct TextureData {
    pub bpp: u8,
    pub width: u32,
    pub height: u32,
    pub data: Vec<u8>,
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum TextureFormat {
    /// sRGB + Alpha, 24 bpp, 8 bpc
    Srgba8,
    /// Linear grayscale pixels, 8 bpp
    Grayscale8,
}

impl From<TextureFormat> for wgpu::TextureFormat {
    fn from(v: TextureFormat) -> Self {
        match v {
            TextureFormat::Srgba8 => wgpu::TextureFormat::Rgba8UnormSrgb,
            TextureFormat::Grayscale8 => wgpu::TextureFormat::R8Unorm,
        }
    }
}

#[derive(Default)]
pub struct PngLoader;

impl Asset for Texture {}

impl AssetDefaultLoader for Texture {
    type Loader = PngLoader;
}

impl SimpleAssetLoader<Texture> for PngLoader {
    fn load(&self, reader: impl AssetReader) -> Result<Texture> {
        let mut decoder = Decoder::new(reader);
        decoder.set_transformations(Transformations::EXPAND);
        let (_info, mut reader) = decoder.read_info().context("Bad PNG")?;
        let (color_type, bit_depth) = reader.output_color_type();

        if bit_depth != BitDepth::Eight {
            bail!(
                "{} bit depth is not supported (8 bit is the only supported)",
                bit_depth as u8
            );
        }

        let (bpp, format) = match color_type {
            ColorType::RGBA => (4, TextureFormat::Srgba8),
            ColorType::Grayscale => (1, TextureFormat::Grayscale8),
            v => bail!(
                "{:?} format is not supported (RGBA and grayscale are the only supported)",
                v
            ),
        };

        let mut buffer = vec![0; reader.output_buffer_size()];
        let width = reader.info().width;
        let height = reader.info().height;

        reader.next_frame(&mut buffer).context("Bad PNG")?;

        Ok(Texture {
            format,
            data: Some(TextureData {
                bpp,
                width,
                height,
                data: buffer,
            }),
        })
    }
}
