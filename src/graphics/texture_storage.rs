use fxhash::FxHashMap;

use crate::assets::{Assets, Id};
use crate::graphics::{AtlasEntryEncoder, Texture, TextureAtlas, TextureFormat};

#[derive(Default)]
pub struct TextureStorage {
    atlases: Vec<(TextureFormat, TextureAtlas<Id<Texture>>)>,
    id_to_atlas: FxHashMap<Id<Texture>, usize>,
}

impl TextureStorage {
    pub fn upload(
        &mut self,
        device: &wgpu::Device,
        encoder: &mut wgpu::CommandEncoder,
        assets: &mut Assets,
    ) {
        'outer: for (handle, texture) in assets.iter_mut::<Texture>() {
            let data = match texture.data.take() {
                Some(v) => v,
                None => continue,
            };

            let data_fn = |mut encoder: AtlasEntryEncoder<'_>| {
                let stride = (data.bpp as usize) * (data.width as usize);
                for y in 0..data.height as usize {
                    let src_row = &data.data[y * stride..(y + 1) * stride];
                    let dst_row = &mut encoder[y];
                    dst_row.copy_from_slice(src_row);
                }
            };

            for (atlas_idx, (format, atlas)) in self.atlases.iter_mut().enumerate() {
                if *format != texture.format {
                    continue;
                }

                if atlas.add(handle.id(), (data.width, data.height), &data_fn) {
                    self.id_to_atlas.insert(handle.id(), atlas_idx);
                    continue 'outer;
                }
            }

            // could not find appropriate atlas

            let format = texture.format.into();
            let bpp = match texture.format {
                TextureFormat::Srgba8 => 4,
                TextureFormat::Grayscale8 => 1,
            };

            let atlas_idx = self.atlases.len();
            let usage = wgpu::TextureUsage::SAMPLED
                | wgpu::TextureUsage::COPY_DST
                | wgpu::TextureUsage::COPY_SRC;
            let size = data.width.max(data.height).next_power_of_two();
            let label = format!("{:?}-{}", format, atlas_idx);
            let mut atlas = TextureAtlas::new(device, format, bpp, usage, size, Some(&label));
            assert!(atlas.add(handle.id(), (data.width, data.height), &data_fn));

            tracing::debug!(idx = atlas_idx, format = ?format, size, "Created new atlas");

            self.atlases.push((texture.format, atlas));
            self.id_to_atlas.insert(handle.id(), atlas_idx);
        }

        for (_, atlas) in &mut self.atlases {
            atlas.flush(device, encoder);
        }
    }

    pub fn cleanup(&mut self, assets: &mut Assets) {
        assets.cleanup_with(|id, _, _| {
            let atlas_idx = match self.id_to_atlas.remove(&id) {
                Some(v) => v,
                None => return,
            };

            self.atlases[atlas_idx].1.deallocate(&id);
        });
    }

    pub fn get_atlas(&self, idx: usize) -> &TextureAtlas<Id<Texture>> {
        &self.atlases[idx].1
    }

    pub fn get_texture_atlas_idx(&self, id: Id<Texture>) -> usize {
        self.id_to_atlas[&id]
    }

    pub fn get_texture_rect(&self, id: Id<Texture>) -> Option<[f32; 4]> {
        let atlas_idx = self.id_to_atlas.get(&id)?;
        let atlas = &self.atlases[*atlas_idx].1;
        atlas.get_rect_f32(&id)
    }
}
