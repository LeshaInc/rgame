use std::num::NonZeroU64;

use glsl_layout::{Std140, Uniform};

#[derive(Debug)]
pub struct UniformBuffer<T> {
    pub data: T,
    buffer: wgpu::Buffer,
}

impl<T> UniformBuffer<T>
where
    T: Uniform,
    <T as Uniform>::Std140: Sized,
{
    pub fn new(device: &wgpu::Device, label: &str, data: T) -> UniformBuffer<T> {
        let label = format!("Uniform Buffer ({})", label);
        let desc = wgpu::BufferDescriptor {
            label: Some(&label),
            size: Self::size(),
            usage: wgpu::BufferUsage::COPY_DST | wgpu::BufferUsage::UNIFORM,
            mapped_at_creation: false,
        };

        let buffer = device.create_buffer(&desc);
        UniformBuffer { data, buffer }
    }

    fn size() -> u64 {
        std::mem::size_of::<<T as Uniform>::Std140>() as u64
    }

    pub fn upload(
        &self,
        device: &wgpu::Device,
        encoder: &mut wgpu::CommandEncoder,
        belt: &mut wgpu::util::StagingBelt,
    ) {
        let nz_size = NonZeroU64::new(Self::size()).unwrap();
        let mut view = belt.write_buffer(encoder, &self.buffer, 0, nz_size, device);
        view.copy_from_slice(&self.data.std140().as_raw());
    }

    pub fn buffer(&self) -> &wgpu::Buffer {
        &self.buffer
    }
}
