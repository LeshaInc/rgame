use std::fs::File;
use std::io::{BufReader, Read, Seek, SeekFrom};
use std::path::Path;

use anyhow::{anyhow, Context, Result};

pub fn view_as_bytes<T>(slice: &[T]) -> &[u8] {
    unsafe {
        std::slice::from_raw_parts(
            slice.as_ptr() as *const u8,
            slice.len() * std::mem::size_of::<T>(),
        )
    }
}

pub fn load_shader(
    device: &wgpu::Device,
    shaders_path: &Path,
    path: &str,
) -> Result<wgpu::ShaderModule> {
    let path = shaders_path.join(path);
    let mut file = File::open(&path).with_context(|| anyhow!("Cannot open {}", path.display()))?;

    let size = file.seek(SeekFrom::End(0))?;
    file.seek(SeekFrom::Start(0))?;

    let mut reader = BufReader::new(file);
    let mut buffer = Vec::with_capacity(size as usize);
    reader.read_to_end(&mut buffer)?;

    let spirv = wgpu::util::make_spirv(&buffer);
    let module = device.create_shader_module(spirv);

    Ok(module)
}
