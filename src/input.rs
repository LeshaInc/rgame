mod map;
pub use self::map::{ActionName, AxisName, InputMap};

mod modifiers;
pub use self::modifiers::Modifiers;

mod binding;
pub use self::binding::{ActionBinding, AxisBinding, KeyboardBinding, MouseButtonBinding};

use std::ops::Deref;
use std::time::Instant;

use fxhash::FxHashMap;
use nalgebra::{Point2, Vector2};
use winit::dpi::PhysicalSize;
use winit::event::{
    DeviceEvent, ElementState, Event as WinitEvent, KeyboardInput, MouseScrollDelta, WindowEvent,
};
use winit::window::{Window, WindowId};

pub use winit::event::{ModifiersState, MouseButton, ScanCode, VirtualKeyCode as KeyCode};

pub struct Input {
    map: InputMap,
    axis_values: FxHashMap<AxisName, AxisValue>,
    events: Vec<Event>,
    state: EventState,
    last_clear_time: Instant,
    window_id: WindowId,
    is_focused: bool,
    scale_factor: f64,
    window_size: Vector2<f32>,
}

impl Input {
    pub fn new(map: InputMap, window: &Window) -> Input {
        let scale_factor = window.scale_factor();
        let window_size = window.inner_size().to_logical(scale_factor);
        let window_size = Vector2::new(window_size.width, window_size.height);
        Input {
            map,
            axis_values: FxHashMap::default(),
            events: Vec::new(),
            state: EventState {
                instant: Instant::now(),
                modifiers: Modifiers(ModifiersState::empty()),
                cursor_pos: Point2::new(0.0, 0.0),
            },
            last_clear_time: Instant::now(),
            window_id: window.id(),
            is_focused: true,
            scale_factor,
            window_size,
        }
    }

    pub fn map(&self) -> &InputMap {
        &self.map
    }

    pub fn map_mut(&mut self) -> &mut InputMap {
        &mut self.map
    }

    pub fn get_axis(&self, name: &str) -> f32 {
        let axis = match self.axis_values.get(name) {
            Some(v) => v,
            None => {
                if !self.map.has_axis(name) {
                    tracing::warn!("No binding for axis {}", name);
                }
                return 0.0;
            }
        };

        axis.positive.min(1.0) - axis.negative.max(-1.0)
    }

    pub fn cursor_pos(&self) -> Point2<f32> {
        self.state.cursor_pos
    }

    pub fn ndc_cursor_pos(&self) -> Point2<f32> {
        Point2::new(
            self.state.cursor_pos.x / self.window_size.x * 2.0 - 1.0,
            -self.state.cursor_pos.y / self.window_size.y * 2.0 + 1.0,
        )
    }

    pub fn window_size(&self) -> Vector2<f32> {
        self.window_size
    }

    pub fn events(&self) -> impl Iterator<Item = Event> + '_ {
        self.events.iter().copied()
    }

    pub fn keyboard_events(&self) -> impl Iterator<Item = KeyboardEvent> + '_ {
        self.events().flat_map(|ev| match ev {
            Event::Keyboard(ev) => Some(ev),
            _ => None,
        })
    }

    pub fn mouse_button_events(&self) -> impl Iterator<Item = MouseButtonEvent> + '_ {
        self.events().flat_map(|ev| match ev {
            Event::MouseButton(ev) => Some(ev),
            _ => None,
        })
    }

    pub fn mouse_motion_events(&self) -> impl Iterator<Item = MouseMotionEvent> + '_ {
        self.events().flat_map(|ev| match ev {
            Event::MouseMotion(ev) => Some(ev),
            _ => None,
        })
    }

    pub fn scroll_events(&self) -> impl Iterator<Item = ScrollEvent> + '_ {
        self.events().flat_map(|ev| match ev {
            Event::Scroll(ev) => Some(ev),
            _ => None,
        })
    }

    pub fn action_events(&self) -> impl Iterator<Item = ActionEvent> + '_ {
        self.events().flat_map(|ev| match ev {
            Event::Action(ev) => Some(ev),
            _ => None,
        })
    }

    pub fn has_pressed(&self, query: impl Into<ButtonQuery>) -> bool {
        match query.into() {
            ButtonQuery::KeyCode(keycode) => self
                .keyboard_events()
                .any(|ev| ev.is_press() && ev.keycode() == Some(keycode)),
            ButtonQuery::ScanCode(scancode) => self
                .keyboard_events()
                .any(|ev| ev.is_press() && ev.scancode() == scancode),
            ButtonQuery::MouseButton(btn) => self
                .mouse_button_events()
                .any(|ev| ev.is_press() && ev.button() == btn),
            ButtonQuery::Action(name) => self
                .action_events()
                .any(|ev| ev.is_press() && ev.name() == name),
        }
    }

    pub fn has_released(&self, query: impl Into<ButtonQuery>) -> bool {
        match query.into() {
            ButtonQuery::KeyCode(keycode) => self
                .keyboard_events()
                .any(|ev| ev.is_release() && ev.keycode() == Some(keycode)),
            ButtonQuery::ScanCode(scancode) => self
                .keyboard_events()
                .any(|ev| ev.is_release() && ev.scancode() == scancode),
            ButtonQuery::MouseButton(btn) => self
                .mouse_button_events()
                .any(|ev| ev.is_release() && ev.button() == btn),
            ButtonQuery::Action(name) => self
                .action_events()
                .any(|ev| ev.is_release() && ev.name() == name),
        }
    }

    pub fn update_window_size(&mut self, size: PhysicalSize<u32>) {
        let size = size.to_logical(self.scale_factor);
        self.window_size = Vector2::new(size.width, size.height);
    }

    pub fn record_event(&mut self, event: &WinitEvent<'_, ()>) {
        self.state.instant = Instant::now();

        match event {
            WinitEvent::WindowEvent { window_id, event } if *window_id == self.window_id => {
                self.record_window_event(event)
            }
            WinitEvent::DeviceEvent { event, .. } => self.record_device_event(event),
            _ => {}
        }
    }

    pub fn clear_events(&mut self) {
        self.events.clear();
        self.last_clear_time = Instant::now();
    }

    fn record_window_event(&mut self, event: &WindowEvent<'_>) {
        if let WindowEvent::Focused(is_focused) = event {
            self.is_focused = *is_focused;
        }

        if !self.is_focused {
            return;
        }

        match event {
            WindowEvent::KeyboardInput { input, .. } => {
                self.record_keyboard_event(input);
            }

            WindowEvent::CursorMoved { position, .. } => {
                let pos = position.to_logical(self.scale_factor);
                self.state.cursor_pos = Point2::new(pos.x, pos.y);
            }

            WindowEvent::MouseInput { state, button, .. } => {
                self.record_mouse_button_event(*state, *button);
            }

            WindowEvent::MouseWheel { delta, .. } => {
                let delta = match delta {
                    MouseScrollDelta::LineDelta(_, y) => y * 10.0, // idk
                    MouseScrollDelta::PixelDelta(p) => p.y as f32,
                };

                self.events.push(Event::Scroll(ScrollEvent {
                    state: self.state,
                    delta,
                }));
            }

            WindowEvent::ScaleFactorChanged { scale_factor, .. } => {
                self.scale_factor = *scale_factor;
            }

            _ => {}
        }
    }

    fn record_mouse_button_event(&mut self, state: ElementState, button: MouseButton) {
        self.events.push(Event::MouseButton(MouseButtonEvent {
            state: self.state,
            button_state: state,
            button,
        }));

        let binding = ActionBinding::MouseButton(MouseButtonBinding(self.state.modifiers, button));
        self.record_binding_actions(state, &binding);
    }

    fn record_keyboard_event(&mut self, input: &KeyboardInput) {
        self.events.push(Event::Keyboard(KeyboardEvent {
            state: self.state,
            key_state: input.state,
            keycode: input.virtual_keycode,
            scancode: input.scancode,
        }));

        if let Some(keycode) = input.virtual_keycode {
            let key = KeyboardBinding::KeyCode(self.state.modifiers, keycode);
            let binding = ActionBinding::Keyboard(key);
            self.record_binding_actions(input.state, &binding);
            self.process_key_axis(input.state, &key);
        }

        let key = KeyboardBinding::ScanCode(self.state.modifiers, input.scancode);
        let binding = ActionBinding::Keyboard(key);
        self.record_binding_actions(input.state, &binding);
        self.process_key_axis(input.state, &key);
    }

    fn process_key_axis(&mut self, state: ElementState, key: &KeyboardBinding) {
        for &(is_positive, axis) in self.map.axes_for_key(key) {
            let value = self.axis_values.entry(axis).or_default();

            let term = if is_positive {
                &mut value.positive
            } else {
                &mut value.negative
            };

            if state == ElementState::Pressed {
                *term = 1.0;
            } else {
                *term = 0.0;
            }
        }
    }

    fn record_binding_actions(&mut self, state: ElementState, binding: &ActionBinding) {
        for &name in self.map.actions_for_binding(binding) {
            self.events.push(Event::Action(ActionEvent {
                state: self.state,
                action_state: state,
                name,
            }));
        }
    }

    fn record_device_event(&mut self, event: &DeviceEvent) {
        if !self.is_focused {
            return;
        }

        if let DeviceEvent::MouseMotion { delta } = event {
            self.events.push(Event::MouseMotion(MouseMotionEvent {
                state: self.state,
                delta: Vector2::new(delta.0 as f32, delta.1 as f32),
            }))
        }
    }
}

#[derive(Clone, Copy, Default)]
struct AxisValue {
    positive: f32,
    negative: f32,
}

#[derive(Clone, Copy, Debug)]
pub enum Event {
    Keyboard(KeyboardEvent),
    MouseButton(MouseButtonEvent),
    MouseMotion(MouseMotionEvent),
    Scroll(ScrollEvent),
    Action(ActionEvent),
}

#[derive(Clone, Copy, Debug)]
pub struct EventState {
    instant: Instant,
    modifiers: Modifiers,
    cursor_pos: Point2<f32>,
}

impl EventState {
    pub fn instant(&self) -> Instant {
        self.instant
    }

    pub fn modifiers(&self) -> Modifiers {
        self.modifiers
    }

    pub fn cursor_pos(&self) -> Point2<f32> {
        self.cursor_pos
    }
}

#[derive(Clone, Copy, Debug)]
pub struct KeyboardEvent {
    state: EventState,
    key_state: ElementState,
    keycode: Option<KeyCode>,
    scancode: ScanCode,
}

impl KeyboardEvent {
    pub fn is_press(&self) -> bool {
        self.key_state == ElementState::Pressed
    }

    pub fn is_release(&self) -> bool {
        self.key_state == ElementState::Released
    }

    pub fn keycode(&self) -> Option<KeyCode> {
        self.keycode
    }

    pub fn scancode(&self) -> ScanCode {
        self.scancode
    }
}

impl Deref for KeyboardEvent {
    type Target = EventState;

    fn deref(&self) -> &EventState {
        &self.state
    }
}

#[derive(Clone, Copy, Debug)]
pub struct MouseButtonEvent {
    state: EventState,
    button_state: ElementState,
    button: MouseButton,
}

impl MouseButtonEvent {
    pub fn is_press(&self) -> bool {
        self.button_state == ElementState::Pressed
    }

    pub fn is_release(&self) -> bool {
        self.button_state == ElementState::Released
    }

    pub fn button(&self) -> MouseButton {
        self.button
    }
}

impl Deref for MouseButtonEvent {
    type Target = EventState;

    fn deref(&self) -> &EventState {
        &self.state
    }
}

#[derive(Clone, Copy, Debug)]
pub struct MouseMotionEvent {
    state: EventState,
    delta: Vector2<f32>,
}

impl MouseMotionEvent {
    pub fn delta(&self) -> Vector2<f32> {
        self.delta
    }
}

impl Deref for MouseMotionEvent {
    type Target = EventState;

    fn deref(&self) -> &EventState {
        &self.state
    }
}

#[derive(Clone, Copy, Debug)]
pub struct ScrollEvent {
    state: EventState,
    delta: f32,
}

impl ScrollEvent {
    pub fn delta(&self) -> f32 {
        self.delta
    }
}

impl Deref for ScrollEvent {
    type Target = EventState;

    fn deref(&self) -> &EventState {
        &self.state
    }
}

#[derive(Clone, Copy, Debug)]
pub struct ActionEvent {
    state: EventState,
    action_state: ElementState,
    name: ActionName,
}

impl ActionEvent {
    pub fn is_press(&self) -> bool {
        self.action_state == ElementState::Pressed
    }

    pub fn is_release(&self) -> bool {
        self.action_state == ElementState::Released
    }

    pub fn name(&self) -> ActionName {
        self.name
    }
}

impl Deref for ActionEvent {
    type Target = EventState;

    fn deref(&self) -> &EventState {
        &self.state
    }
}

pub enum ButtonQuery {
    KeyCode(KeyCode),
    ScanCode(ScanCode),
    MouseButton(MouseButton),
    Action(ActionName),
}

impl From<KeyCode> for ButtonQuery {
    fn from(v: KeyCode) -> ButtonQuery {
        ButtonQuery::KeyCode(v)
    }
}

impl From<ScanCode> for ButtonQuery {
    fn from(v: ScanCode) -> ButtonQuery {
        ButtonQuery::ScanCode(v)
    }
}

impl From<MouseButton> for ButtonQuery {
    fn from(v: MouseButton) -> ButtonQuery {
        ButtonQuery::MouseButton(v)
    }
}

impl From<ActionName> for ButtonQuery {
    fn from(v: ActionName) -> ButtonQuery {
        ButtonQuery::Action(v)
    }
}

impl From<&'static str> for ButtonQuery {
    fn from(v: &'static str) -> ButtonQuery {
        ButtonQuery::Action(v.into())
    }
}
