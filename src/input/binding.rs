use std::fmt::{self, Display};
use std::str::FromStr;

use serde::de::Error as _;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

use super::{KeyCode, Modifiers, MouseButton, ScanCode};

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum KeyboardBinding {
    KeyCode(Modifiers, KeyCode),
    ScanCode(Modifiers, ScanCode),
}

impl KeyboardBinding {
    pub fn modifiers(self) -> Modifiers {
        match self {
            KeyboardBinding::KeyCode(mods, _) => mods,
            KeyboardBinding::ScanCode(mods, _) => mods,
        }
    }
}

impl Display for KeyboardBinding {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mods = self.modifiers();
        if !mods.is_empty() {
            write!(f, "{}-", mods)?;
        }

        match self {
            KeyboardBinding::KeyCode(_, v) => write!(f, "{:?}", v),
            KeyboardBinding::ScanCode(_, v) => write!(f, "{}", v),
        }
    }
}

impl<'de> Deserialize<'de> for KeyboardBinding {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;

        let mut split = s.rsplitn(2, '-');
        let key = split
            .next()
            .ok_or_else(|| D::Error::custom("empty string"))?;

        let mods =
            Modifiers::from_str(split.next().unwrap_or_default()).map_err(D::Error::custom)?;

        let key_json = serde_json::json!(key);

        if let Ok(code) = serde_json::from_value(key_json) {
            Ok(KeyboardBinding::KeyCode(mods, code))
        } else {
            let code = ScanCode::from_str(key).map_err(D::Error::custom)?;
            Ok(KeyboardBinding::ScanCode(mods, code))
        }
    }
}

impl Serialize for KeyboardBinding {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&format!("{}", self))
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct MouseButtonBinding(pub Modifiers, pub MouseButton);

impl Display for MouseButtonBinding {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if !self.0.is_empty() {
            write!(f, "{}-", self.0)?;
        }

        match self.1 {
            MouseButton::Left => write!(f, "MouseLeft"),
            MouseButton::Middle => write!(f, "MouseMiddle"),
            MouseButton::Right => write!(f, "MouseRight"),
            MouseButton::Other(v) => write!(f, "Mouse{}", v),
        }
    }
}

impl<'de> Deserialize<'de> for MouseButtonBinding {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = <&str>::deserialize(deserializer)?;

        let mut split = s.rsplitn(2, '-');
        let mut button = split
            .next()
            .ok_or_else(|| D::Error::custom("empty string"))?;

        let mods =
            Modifiers::from_str(split.next().unwrap_or_default()).map_err(D::Error::custom)?;

        if !button.starts_with("Mouse") {
            return Err(D::Error::custom("Bad mouse button"));
        }

        button = &button[5..];

        let button = match button {
            "Left" => MouseButton::Left,
            "Middle" => MouseButton::Middle,
            "Right" => MouseButton::Right,
            _ => MouseButton::Other(button.parse().map_err(D::Error::custom)?),
        };

        Ok(MouseButtonBinding(mods, button))
    }
}

impl Serialize for MouseButtonBinding {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&format!("{}", self))
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Deserialize, Serialize)]
#[serde(untagged)]
pub enum ActionBinding {
    Keyboard(KeyboardBinding),
    MouseButton(MouseButtonBinding),
}

impl ActionBinding {
    pub fn is_keyboard(&self) -> bool {
        matches!(self, ActionBinding::Keyboard(_))
    }

    pub fn is_mouse(&self) -> bool {
        matches!(self, ActionBinding::MouseButton(_))
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Deserialize, Serialize)]
#[serde(untagged)]
pub enum AxisBinding {
    Keyboard {
        positive: KeyboardBinding,
        negative: KeyboardBinding,
    },
}

impl AxisBinding {
    pub fn is_keyboard(&self) -> bool {
        true
    }
}
