use std::borrow::Borrow;
use std::fs::File;
use std::io::BufReader;
use std::ops::Deref;
use std::path::Path;

use anyhow::{Context, Result};
use fxhash::FxHashMap;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

use super::{ActionBinding, AxisBinding, KeyboardBinding};

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
struct StaticStr(&'static str);

impl Serialize for StaticStr {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        self.0.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for StaticStr {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        Ok(StaticStr(Box::leak(s.into_boxed_str())))
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Deserialize, Serialize)]
#[serde(transparent)]
pub struct ActionName(StaticStr);

impl Deref for ActionName {
    type Target = str;

    fn deref(&self) -> &str {
        self.0 .0
    }
}

impl From<&'static str> for ActionName {
    fn from(v: &'static str) -> ActionName {
        ActionName(StaticStr(v))
    }
}

impl Borrow<str> for ActionName {
    fn borrow(&self) -> &str {
        &self.0 .0
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Deserialize, Serialize)]
#[serde(transparent)]
pub struct AxisName(StaticStr);

impl Deref for AxisName {
    type Target = str;

    fn deref(&self) -> &str {
        self.0 .0
    }
}

impl From<&'static str> for AxisName {
    fn from(v: &'static str) -> AxisName {
        AxisName(StaticStr(v))
    }
}

impl Borrow<str> for AxisName {
    fn borrow(&self) -> &str {
        &self.0 .0
    }
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct InputMap {
    actions: FxHashMap<ActionName, Vec<ActionBinding>>,
    #[serde(skip)]
    rev_actions: FxHashMap<ActionBinding, Vec<ActionName>>,
    axes: FxHashMap<AxisName, Vec<AxisBinding>>,
    #[serde(skip)]
    rev_key_axes: FxHashMap<KeyboardBinding, Vec<(bool, AxisName)>>,
}

impl InputMap {
    pub fn new() -> InputMap {
        InputMap::default()
    }

    pub fn open(path: &Path) -> Result<InputMap> {
        let file = File::open(path).with_context(|| format!("Cannot open {}", path.display()))?;
        let reader = BufReader::new(file);
        let mut map: InputMap = serde_json::from_reader(reader)?;
        map.update_rev_action();
        map.update_rev_axes();
        Ok(map)
    }

    pub fn merge_with(&mut self, other: &InputMap) {
        for (other_action, other_bindings) in &other.actions {
            let self_bindings = self.actions.entry(*other_action).or_default();

            let other_key = other_bindings.iter().find(|b| b.is_keyboard());
            if let Some(key) = other_key {
                self_bindings.retain(|b| !b.is_keyboard());
                self_bindings.push(*key);
            }

            let other_mouse = other_bindings.iter().find(|b| b.is_mouse());
            if let Some(mouse) = other_mouse {
                self_bindings.retain(|b| !b.is_mouse());
                self_bindings.push(*mouse);
            }
        }

        for (other_axis, other_bindings) in &other.axes {
            let self_bindings = self.axes.entry(*other_axis).or_default();

            let other_key = other_bindings.iter().find(|b| b.is_keyboard());
            if let Some(key) = other_key {
                self_bindings.retain(|b| !b.is_keyboard());
                self_bindings.push(*key);
            }
        }

        self.update_rev_action();
        self.update_rev_axes();
    }

    pub fn actions_for_binding(&self, binding: &ActionBinding) -> &[ActionName] {
        let actions = self.rev_actions.get(binding);
        actions.map(|v| v.as_slice()).unwrap_or(&[])
    }

    pub fn axes_for_key(&self, key: &KeyboardBinding) -> &[(bool, AxisName)] {
        let axes = self.rev_key_axes.get(key);
        axes.map(|v| v.as_slice()).unwrap_or(&[])
    }

    fn update_rev_action(&mut self) {
        self.rev_actions.clear();
        for (action, bindings) in &self.actions {
            for &binding in bindings {
                let actions = self.rev_actions.entry(binding).or_default();
                actions.push(*action);
            }
        }
    }

    fn update_rev_axes(&mut self) {
        self.rev_key_axes.clear();
        for (axis, bindings) in &self.axes {
            for &binding in bindings {
                match binding {
                    AxisBinding::Keyboard { positive, negative } => {
                        let axes = self.rev_key_axes.entry(positive).or_default();
                        axes.push((true, *axis));
                        let axes = self.rev_key_axes.entry(negative).or_default();
                        axes.push((false, *axis));
                    }
                }
            }
        }
    }

    pub fn has_axis(&self, name: &str) -> bool {
        self.axes.contains_key(name)
    }
}
