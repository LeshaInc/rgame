use std::fmt::{self, Display};
use std::str::FromStr;

use anyhow::{bail, Error, Result};
use winit::event::ModifiersState;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Modifiers(pub ModifiersState);

impl Modifiers {
    pub fn is_empty(self) -> bool {
        self.0.is_empty()
    }
}

impl Display for Modifiers {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut is_first = false;
        let mut add_part = |name| {
            if !is_first {
                f.write_str("-")?;
            }
            is_first = false;
            f.write_str(name)
        };

        if self.0.ctrl() {
            add_part("Ctrl")?;
        }
        if self.0.alt() {
            add_part("Alt")?;
        }
        if self.0.shift() {
            add_part("Shift")?;
        }
        if self.0.logo() {
            add_part("Logo")?;
        }

        Ok(())
    }
}

impl FromStr for Modifiers {
    type Err = Error;

    fn from_str(s: &str) -> Result<Modifiers> {
        let mut mods = ModifiersState::empty();

        for part in s.split('-') {
            if part.eq_ignore_ascii_case("ctrl") {
                mods |= ModifiersState::CTRL;
            } else if part.eq_ignore_ascii_case("alt") {
                mods |= ModifiersState::ALT;
            } else if part.eq_ignore_ascii_case("shift") {
                mods |= ModifiersState::SHIFT;
            } else if part.eq_ignore_ascii_case("logo") {
                mods |= ModifiersState::LOGO;
            } else if !part.is_empty() {
                bail!("Invalid modifier")
            }
        }

        Ok(Modifiers(mods))
    }
}
