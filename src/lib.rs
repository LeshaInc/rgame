#[macro_use]
pub mod util;

pub mod assets;
pub mod character;
pub mod graphics;
pub mod world;

pub mod input;
pub use self::input::Input;

mod schedule;
pub use self::schedule::{ScheduleBuilder, StageBuilder};

#[derive(Clone, Copy, Debug)]
pub struct DeltaTime(pub f32);
