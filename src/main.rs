use std::time::Duration;

use legion::{IntoQuery, Resources, World};
use nalgebra::{Point3, UnitQuaternion};
use winit::event::{Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Window, WindowBuilder};

use rgame::assets::{Assets, DirectorySource, Handle};
use rgame::character::{character_bundle, Character};
use rgame::graphics::draw3d::{
    Camera, DrawList3D, DrawList3DEncoder, Instance, Material, MaterialProperties, Mesh,
};
use rgame::graphics::graphics_bundle;
use rgame::input::{Input, InputMap};
use rgame::world::{
    drawing_bundle, hierarchy_bundle, transform_bundle, Position, Rotation, Scale, WorldTransform,
};
use rgame::{DeltaTime, ScheduleBuilder};

fn main() {
    let default_filter = "gfx_backend_vulkan=off,gfx_memory=off,gfx_descriptor=off,warn,rgame=info";
    let filter = match std::env::var("RUST_LOG") {
        Ok(v) if !v.is_empty() => format!("{},{}", default_filter, v),
        _ => default_filter.into(),
    };

    let subscriber = tracing_subscriber::fmt()
        .pretty()
        .with_env_filter(filter)
        .finish();

    tracing_log::LogTracer::init().unwrap();
    tracing::subscriber::set_global_default(subscriber).unwrap();

    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();

    let mut world = World::default();
    let mut resources = Resources::default();

    resources.insert(DrawList3DEncoder::new(DrawList3D::empty()));
    resources.insert(DeltaTime(1.0 / 60.0));
    resources.insert(Camera::default());

    let map = InputMap::open("controls.json".as_ref()).unwrap();
    resources.insert(Input::new(map, &window));

    let source = DirectorySource::new("assets");
    resources.insert(Assets::new(source));

    resources.insert(window);

    let mut schedule_builder = ScheduleBuilder::new();

    schedule_builder
        .add_first_stage("UPDATE")
        .add_last_stage("DRAW")
        .add_bundle(transform_bundle)
        .add_bundle(hierarchy_bundle)
        .add_bundle(graphics_bundle)
        .add_bundle(character_bundle)
        .add_bundle(drawing_bundle);

    schedule_builder.initialize(&mut world, &mut resources);
    let mut schedule = schedule_builder.build();

    let mut assets = resources.get_mut::<Assets>().unwrap();
    let test_mesh: Handle<Mesh> = assets.load("meshes/material_test.mesh");
    let envmap = assets.load("envmaps/test.tar");
    let pbr_material: Handle<Material> = assets.load("materials/pbr.json");

    Character::new().spawn(&mut world, &mut assets, Point3::origin());

    let waiter = assets.spawn_workers();
    let mut old_percentage = 0;
    while waiter.wait_timeout(Duration::from_millis(1)) {
        let progress = assets.progress_counter();
        let percentage = progress.percentage();
        if percentage != old_percentage {
            tracing::info!("Loading {}%", percentage);
            old_percentage = percentage;
        }
    }

    tracing::info!("Loading done");

    assets.flush();
    drop(assets);

    for ro in 0..10 {
        for me in 0..=1 {
            world.push((
                Position(Point3::new(
                    3.5 * (me as f32 - 0.5),
                    2.5 * ro as f32 - 6.0,
                    0.0,
                )),
                Scale(0.05),
                Rotation(UnitQuaternion::from_euler_angles(0.0, 0.0, 0.0)),
                pbr_material.clone(),
                test_mesh.clone(),
                MaterialProperties::new()
                    .add([0.3, 0.5, 0.7])
                    .add([ro as f32 / 9.0])
                    .add([me as f32]),
            ));
        }
    }

    event_loop.run(move |event, _, control_flow| {
        let mut input = resources.get_mut::<Input>().unwrap();
        input.record_event(&event);
        drop(input);

        match event {
            Event::RedrawRequested(_) => {
                let mut encoder = resources.get_mut::<DrawList3DEncoder>().unwrap();
                encoder.set_envmap(&envmap);

                let mut query = <(
                    &WorldTransform,
                    &Handle<Material>,
                    &Handle<Mesh>,
                    &MaterialProperties,
                )>::query();

                for (trans, mat, mesh, props) in query.iter(&world) {
                    let inst = Instance::new(trans.0.to_homogeneous());
                    encoder.mesh(inst, mesh, mat, props, &[]);
                }

                drop(encoder);

                schedule.execute(&mut world, &mut resources);

                let mut input = resources.get_mut::<Input>().unwrap();
                input.clear_events();
            }
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => *control_flow = ControlFlow::Exit,
            Event::MainEventsCleared => {
                let window = resources.get::<Window>().unwrap();
                window.request_redraw();
            }
            _ => {}
        }
    });
}
