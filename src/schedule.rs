use std::collections::VecDeque;

use legion::systems::{Builder, ParallelRunnable, Runnable, Schedule};
use legion::{Resources, World};

struct Stage {
    name: &'static str,
    pos: StagePos,
    builder: Builder,
}

enum StagePos {
    First,
    Last,
    Before(&'static str),
    After(&'static str),
}

impl Stage {
    pub fn new(name: &'static str, pos: StagePos) -> Stage {
        Stage {
            name,
            pos,
            builder: Builder::default(),
        }
    }
}

#[derive(Default)]
pub struct ScheduleBuilder {
    stages: VecDeque<Stage>,
    initializers: Vec<Box<dyn Fn(&mut World, &mut Resources)>>,
}

impl ScheduleBuilder {
    pub fn new() -> ScheduleBuilder {
        Default::default()
    }

    pub fn add_first_stage(&mut self, stage: &'static str) -> &mut Self {
        self.stages.push_front(Stage::new(stage, StagePos::First));
        self
    }

    pub fn add_last_stage(&mut self, stage: &'static str) -> &mut Self {
        self.stages.push_back(Stage::new(stage, StagePos::Last));
        self
    }

    pub fn add_stage_before(&mut self, target: &'static str, stage: &'static str) -> &mut Self {
        self.stages
            .push_front(Stage::new(stage, StagePos::Before(target)));
        self
    }

    pub fn add_stage_after(&mut self, target: &'static str, stage: &'static str) -> &mut Self {
        self.stages
            .push_back(Stage::new(stage, StagePos::After(target)));
        self
    }

    pub fn on_stage(
        &mut self,
        name: &'static str,
        f: impl FnOnce(&mut StageBuilder<'_>),
    ) -> &mut Self {
        let stage = self.stages.iter_mut().find(|s| s.name == name).unwrap();
        let mut builder = StageBuilder {
            builder: &mut stage.builder,
        };

        f(&mut builder);

        self
    }

    pub fn add_bundle(&mut self, bundle: impl FnOnce(&mut ScheduleBuilder)) -> &mut Self {
        bundle(self);
        self
    }

    pub fn add_initializer<F>(&mut self, initializer: F)
    where
        F: Fn(&mut World, &mut Resources) + 'static,
    {
        self.initializers.push(Box::new(initializer));
    }

    pub fn initialize(&self, world: &mut World, resources: &mut Resources) {
        for initializer in &self.initializers {
            initializer(world, resources);
        }
    }

    pub fn build(&mut self) -> Schedule {
        let mut schedules = Vec::with_capacity(self.stages.len());

        // TODO: this is all wrong, proper dependencies are needed

        for stage in &mut self.stages {
            match stage.pos {
                StagePos::First => schedules.insert(0, (stage.name, stage.builder.build())),
                StagePos::Last => schedules.push((stage.name, stage.builder.build())),
                _ => {}
            }
        }

        for i in 0..self.stages.len() {
            let stage = &mut self.stages[i];
            let name = stage.name;
            match stage.pos {
                StagePos::Before(target) => {
                    let mut it = schedules.iter();
                    let idx = it.position(|(n, _)| *n == target).expect("no such stage");
                    schedules.insert(idx, (name, self.stages[i].builder.build()));
                }
                StagePos::After(target) => {
                    let mut it = schedules.iter();
                    let idx = it.position(|(n, _)| *n == target).expect("no such stage");
                    schedules.insert(idx + 1, (name, self.stages[i].builder.build()));
                }
                _ => {}
            }
        }

        schedules
            .into_iter()
            .flat_map(|(_, s)| s.into_vec())
            .collect::<Vec<_>>()
            .into()
    }
}

pub struct StageBuilder<'a> {
    builder: &'a mut Builder,
}

impl StageBuilder<'_> {
    pub fn add_system<S: ParallelRunnable + 'static>(&mut self, system: S) -> &mut Self {
        self.builder.add_system(system);
        self
    }

    pub fn add_thread_local<S: Runnable + 'static>(&mut self, system: S) -> &mut Self {
        self.builder.add_thread_local(system);
        self
    }

    pub fn add_thread_local_fn<F: FnMut(&mut World, &mut Resources) + 'static>(
        &mut self,
        f: F,
    ) -> &mut Self {
        self.builder.add_thread_local_fn(f);
        self
    }

    pub fn flush(&mut self) -> &mut Self {
        self.builder.flush();
        self
    }
}
