use std::ops;

use nalgebra::Vector3;

use crate::util::BoundingVolume;

/// Axis aligned bounding box
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Aabb {
    /// The top-left corner
    pub min: Vector3<f32>,
    /// The bottom-right corner
    pub max: Vector3<f32>,
}

impl Aabb {
    /// Create a new AABR
    pub fn new(min: Vector3<f32>, max: Vector3<f32>) -> Aabb {
        Aabb { min, max }
    }

    /// Create a new AABR
    pub fn from_pos_extent(pos: Vector3<f32>, extent: Vector3<f32>) -> Aabb {
        Aabb {
            min: pos,
            max: pos + extent,
        }
    }

    /// Get extent vector (`max - min`)
    pub fn extent(&self) -> Vector3<f32> {
        self.max - self.min
    }

    /// Get width
    pub fn width(&self) -> f32 {
        self.extent().x
    }

    /// Get height
    pub fn height(&self) -> f32 {
        self.extent().y
    }

    /// Get depth
    pub fn depth(&self) -> f32 {
        self.extent().z
    }

    /// Calculate union with other AABR
    pub fn union(&self, other: &Aabb) -> Aabb {
        Aabb {
            min: self.min.inf(&other.min),
            max: self.max.sup(&other.max),
        }
    }

    /// Calculate intersection with other AABR
    pub fn intersection(&self, other: &Aabb) -> Aabb {
        Aabb {
            min: self.min.inf(&other.min),
            max: self.max.sup(&other.max),
        }
    }

    /// Calculate volume of the AABB
    pub fn volume(&self) -> f32 {
        let extent = self.extent();
        extent.x * extent.y * extent.z
    }

    /// Check whether there is an overlap with other AABR
    pub fn overlaps(&self, other: &Aabb) -> bool {
        self.max.x >= other.min.x
            && self.min.x <= other.max.x
            && self.max.y >= other.min.y
            && self.min.y <= other.max.y
            && self.max.z >= other.min.z
            && self.min.z <= other.max.z
    }

    /// Check whether the AABB contains a point
    pub fn contains_point(&self, point: Vector3<f32>) -> bool {
        self.min.x <= point.x
            && point.x <= self.max.x
            && self.min.y <= point.y
            && point.y <= self.max.y
            && self.min.z <= point.z
            && point.z <= self.max.z
    }
}

impl ops::Mul<f32> for Aabb {
    type Output = Aabb;
    fn mul(self, rhs: f32) -> Aabb {
        Aabb {
            min: self.min * rhs,
            max: self.max * rhs,
        }
    }
}

impl BoundingVolume for Aabb {
    fn union(&self, other: &Self) -> Self {
        Aabb::union(self, other)
    }

    fn overlaps(&self, other: &Self) -> bool {
        Aabb::overlaps(self, other)
    }

    fn surface_area(&self) -> f32 {
        2.0 * (self.width() * self.height()
            + self.width() * self.depth()
            + self.height() * self.depth())
    }
}
