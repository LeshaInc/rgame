use std::ops;

use nalgebra::{Matrix3, Point2, Vector2};

use crate::util::BoundingVolume;

/// Axis aligned bounding rectangle
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Aabr {
    /// The top-left corner
    pub min: Point2<f32>,
    /// The bottom-right corner
    pub max: Point2<f32>,
}

impl Aabr {
    /// Create a new AABR
    pub fn new(min: Point2<f32>, max: Point2<f32>) -> Aabr {
        Aabr { min, max }
    }

    /// Create a new AABR
    pub fn from_pos_extent(pos: Point2<f32>, extent: Vector2<f32>) -> Aabr {
        Aabr {
            min: pos,
            max: pos + extent,
        }
    }

    /// Get width
    pub fn width(&self) -> f32 {
        self.extent().x
    }

    /// Get height
    pub fn height(&self) -> f32 {
        self.extent().y
    }

    /// Get extent vector (`max - min`)
    pub fn extent(&self) -> Vector2<f32> {
        self.max - self.min
    }

    /// Calculate union with other AABR
    pub fn union(&self, other: &Aabr) -> Aabr {
        Aabr {
            min: self.min.inf(&other.min),
            max: self.max.sup(&other.max),
        }
    }

    /// Calculate intersection with other AABR
    pub fn intersection(&self, other: &Aabr) -> Aabr {
        Aabr {
            min: self.min.inf(&other.min),
            max: self.max.sup(&other.max),
        }
    }

    /// Get top left corner
    pub fn top_left(&self) -> Point2<f32> {
        self.min
    }

    /// Get top right corner
    pub fn top_right(&self) -> Point2<f32> {
        self.min + Vector2::<f32>::new(self.width(), 0.0)
    }

    /// Get bottom right corner
    pub fn bottom_right(&self) -> Point2<f32> {
        self.max
    }

    /// Get bottom left corner
    pub fn bottom_left(&self) -> Point2<f32> {
        self.min + Vector2::<f32>::new(0.0, self.height())
    }

    /// Translate AABR by a vector
    pub fn translate(&self, dir: Vector2<f32>) -> Aabr {
        Aabr::new(self.min + dir, self.max + dir)
    }

    /// Transform AABR by a given matrix
    pub fn transform(&self, matrix: &Matrix3<f32>) -> Aabr {
        let points = [
            matrix.transform_point(&self.top_left()),
            matrix.transform_point(&self.top_right()),
            matrix.transform_point(&self.bottom_left()),
            matrix.transform_point(&self.bottom_right()),
        ];

        let (min, max) = points
            .iter()
            .fold((points[0], points[3]), |(min, max), &b| {
                (min.inf(&b), max.sup(&b))
            });

        Aabr::new(min, max)
    }

    /// Calculate area of the AABR
    pub fn area(&self) -> f32 {
        let extent = self.extent();
        extent.x * extent.y
    }

    /// Check whether there is an overlap with other AABR
    pub fn overlaps(&self, other: &Aabr) -> bool {
        self.max.x >= other.min.x
            && self.min.x <= other.max.x
            && self.max.y >= other.min.y
            && self.min.y <= other.max.y
    }

    /// Check whether the AABB contains a point
    pub fn contains_point(&self, point: Vector2<f32>) -> bool {
        self.min.x <= point.x
            && point.x <= self.max.x
            && self.min.y <= point.y
            && point.y <= self.max.y
    }

    /// Move every edge by `value` along normal
    pub fn inflate(&self, value: f32) -> Aabr {
        let vector = Vector2::<f32>::from_element(value);
        Aabr {
            min: self.min - vector,
            max: self.max + vector,
        }
    }

    /// Calculate minkowski difference
    pub fn minkowski_difference(&self, other: &Aabr) -> Aabr {
        Aabr::new((self.min - other.max).into(), (self.max - other.min).into())
    }

    /// Get closest point on bounds
    pub fn closest_point_on_bounds(&self, point: Vector2<f32>) -> Vector2<f32> {
        let mut min_dist = (point.x - self.min.x).abs();
        let mut bounds_pt = Vector2::<f32>::new(self.min.x, point.y);

        let dist = (self.max.x - point.x).abs();
        if dist < min_dist {
            min_dist = dist;
            bounds_pt = Vector2::<f32>::new(self.max.x, point.y);
        }

        let dist = (self.max.y - point.y).abs();
        if dist < min_dist {
            min_dist = dist;
            bounds_pt = Vector2::<f32>::new(point.x, self.max.y);
        }

        let dist = (self.min.y - point.y).abs();
        if dist < min_dist {
            bounds_pt = Vector2::<f32>::new(point.x, self.min.y);
        }

        bounds_pt
    }
}

impl ops::Mul<f32> for Aabr {
    type Output = Aabr;
    fn mul(self, rhs: f32) -> Aabr {
        Aabr {
            min: self.min * rhs,
            max: self.max * rhs,
        }
    }
}

impl BoundingVolume for Aabr {
    fn union(&self, other: &Self) -> Self {
        Aabr::union(self, other)
    }

    fn overlaps(&self, other: &Self) -> bool {
        Aabr::overlaps(self, other)
    }

    fn surface_area(&self) -> f32 {
        self.area()
    }
}
