//! Map for values of any type

use std::any::{Any, TypeId};
use std::collections::hash_map;
use std::marker::PhantomData;

use fxhash::FxHashMap;

/// Unchecked downcast functions
pub unsafe trait DowncastUnchecked: 'static {
    /// # Safety
    /// Value must be T and not something else
    #[inline(always)]
    unsafe fn downcast_ref_unchecked<T: 'static>(&self) -> &T {
        &*(self as *const Self as *const T)
    }

    /// # Safety
    /// Value must be T and not something else
    #[inline(always)]
    unsafe fn downcast_mut_unchecked<T: 'static>(&mut self) -> &mut T {
        &mut *(self as *mut Self as *mut T)
    }

    /// # Safety
    /// Value must be T and not something else
    #[inline(always)]
    unsafe fn downcast_unchecked<T: 'static>(self: Box<Self>) -> Box<T> {
        Box::from_raw(Box::into_raw(self) as *mut T)
    }
}

/// Trait for converting concrete values into trait objects
pub trait IntoBoxDyn<T: ?Sized + DowncastUnchecked>: 'static {
    /// Perform the conversion
    fn into_box_dyn(self) -> Box<T>;
}

/// Implement custom Any type
#[macro_export]
macro_rules! impl_any {
    ($name:path) => {
        unsafe impl $crate::util::anymap::DowncastUnchecked for dyn $name {}
        impl<T: $name> $crate::util::anymap::IntoBoxDyn<dyn $name> for T {
            fn into_box_dyn(self) -> Box<dyn $name> {
                Box::new(self)
            }
        }
    };
}

impl_any!(Any);

unsafe impl DowncastUnchecked for dyn Any + Send + Sync {}
impl<T: Any + Send + Sync> IntoBoxDyn<dyn Any + Send + Sync> for T {
    fn into_box_dyn(self) -> Box<dyn Any + Send + Sync> {
        Box::new(self)
    }
}

/// Map for values of any type
pub struct AnyMap<T: ?Sized = dyn Any> {
    inner: FxHashMap<TypeId, Box<T>>,
}

impl<T: ?Sized> Default for AnyMap<T> {
    fn default() -> AnyMap<T> {
        AnyMap {
            inner: FxHashMap::default(),
        }
    }
}

impl<T: ?Sized + DowncastUnchecked> AnyMap<T> {
    /// Create an empty map
    pub fn new() -> AnyMap<T> {
        Default::default()
    }

    /// Insert a value
    pub fn insert<U: IntoBoxDyn<T>>(&mut self, value: U) {
        let id = TypeId::of::<U>();
        let value = value.into_box_dyn();
        self.inner.insert(id, value);
    }

    /// Get a reference to the value, if it exists
    pub fn get<U: IntoBoxDyn<T>>(&self) -> Option<&U> {
        self.inner
            .get(&TypeId::of::<U>())
            .map(|val| unsafe { val.downcast_ref_unchecked() })
    }

    /// Get a mutable reference to the value, if it exists
    pub fn get_mut<U: IntoBoxDyn<T>>(&mut self) -> Option<&mut U> {
        self.inner
            .get_mut(&TypeId::of::<U>())
            .map(|val| unsafe { val.downcast_mut_unchecked() })
    }

    /// Get a reference to the value as a trait object, if it exists
    pub fn get_dyn(&mut self, id: TypeId) -> Option<&T> {
        self.inner.get(&id).map(|v| &**v)
    }

    /// Get a mutable reference to the value as a trait object, if it exists
    pub fn get_mut_dyn(&mut self, id: TypeId) -> Option<&mut T> {
        self.inner.get_mut(&id).map(|v| &mut **v)
    }

    /// Check whether the map contains the given value
    pub fn contains<U: IntoBoxDyn<T>>(&self) -> bool {
        self.inner.contains_key(&TypeId::of::<U>())
    }

    /// Remove a value from the map, returning the old one if it existed
    pub fn remove<U: IntoBoxDyn<T>>(&mut self) -> Option<U> {
        self.inner
            .remove(&TypeId::of::<U>())
            .map(|val| unsafe { *val.downcast_unchecked() })
    }

    /// Clear the map, dropping every value
    pub fn clear(&mut self) {
        self.inner.clear();
    }

    /// Iterator over trait objects stored inside the map
    pub fn values_mut(&mut self) -> impl Iterator<Item = &mut T> + '_ {
        self.inner.values_mut().map(|b| &mut **b)
    }

    /// Get entry of the value for in-place manipulation
    pub fn entry<U: IntoBoxDyn<T>>(&mut self) -> Entry<'_, T, U> {
        Entry::new(self.inner.entry(TypeId::of::<U>()))
    }
}

/// View into a single `AnyMap` entry, either vacant or occupied
pub struct Entry<'a, T: ?Sized, U> {
    inner: hash_map::Entry<'a, TypeId, Box<T>>,
    marker: PhantomData<U>,
}

impl<'a, T: ?Sized + DowncastUnchecked, U: IntoBoxDyn<T>> Entry<'a, T, U> {
    fn new(inner: hash_map::Entry<'a, TypeId, Box<T>>) -> Self {
        Entry {
            inner,
            marker: PhantomData,
        }
    }

    /// Ensures a value is in the entry by inserting the result of the default function if empty,
    /// and returns a mutable reference to the value in the entry.
    pub fn or_insert_with<F: FnOnce() -> U>(self, default: F) -> &'a mut U {
        let any = self.inner.or_insert_with(|| default().into_box_dyn());
        unsafe { any.downcast_mut_unchecked() }
    }

    /// Ensures a value is in the entry by inserting the default if empty,
    /// and returns a mutable reference to the value in the entry.
    pub fn or_insert(self, default: U) -> &'a mut U {
        self.or_insert_with(|| default)
    }

    /// Ensures a value is in the entry by inserting the default value if empty, and returns a mutable
    /// reference to the value in the entry.
    pub fn or_default(self) -> &'a mut U
    where
        U: Default,
    {
        self.or_insert_with(U::default)
    }

    /// Provides in-place mutable access to an occupied entry before any potential inserts into the map.
    pub fn and_modify<F: FnOnce(&mut U)>(self, f: F) -> Self {
        let inner = self.inner.and_modify(|any| {
            let val = unsafe { any.downcast_mut_unchecked() };
            f(val)
        });
        Self::new(inner)
    }
}
