use slab::Slab;

pub trait BoundingVolume: Copy {
    fn union(&self, other: &Self) -> Self;
    fn overlaps(&self, other: &Self) -> bool;
    fn surface_area(&self) -> f32;
}

const NULL_NODE: u32 = u32::max_value();

#[derive(Clone, Debug)]
struct BvhNode<BV, T> {
    bv: BV,
    parent: u32,
    children: [u32; 2],
    data: Option<T>,
}

impl<BV, T> BvhNode<BV, T> {
    fn is_leaf(&self) -> bool {
        self.data.is_some()
    }
}

/// BVH for collision checking
#[derive(Clone, Debug)]
pub struct Bvh<BV, T = ()> {
    root: u32,
    nodes: Slab<BvhNode<BV, T>>,
}

impl<BV, T> Default for Bvh<BV, T> {
    fn default() -> Bvh<BV, T> {
        Bvh {
            root: NULL_NODE,
            nodes: Slab::new(),
        }
    }
}

/// BVH Node ID
pub struct NodeId(u32);

impl<BV: BoundingVolume, T> Bvh<BV, T> {
    /// Create an empty BVH
    pub fn new() -> Bvh<BV, T> {
        Default::default()
    }

    /// Clear the tree, without deallocating space
    pub fn clear(&mut self) {
        self.root = NULL_NODE;
        self.nodes.clear();
    }

    fn get(&self, id: u32) -> &BvhNode<BV, T> {
        &self.nodes[id as usize]
    }

    fn get_mut(&mut self, id: u32) -> &mut BvhNode<BV, T> {
        &mut self.nodes[id as usize]
    }

    fn alloc_node(&mut self, bv: BV, data: Option<T>) -> u32 {
        let node = BvhNode {
            bv,
            parent: NULL_NODE,
            children: [NULL_NODE; 2],
            data,
        };

        self.nodes.insert(node) as u32
    }

    /// Insert a node
    pub fn insert(&mut self, bv: BV, data: T) -> NodeId {
        let node_id = self.alloc_node(bv, Some(data));

        if self.root == NULL_NODE {
            self.root = node_id;
            return NodeId(node_id);
        }

        let mut cur_id = self.root;
        while !self.get(cur_id).is_leaf() {
            let cur = self.get(cur_id);

            let combined_bv = bv.union(&cur.bv);
            let new_parent_cost = 2.0 * combined_bv.surface_area();
            let min_push_down_cost = 2.0 * (combined_bv.surface_area() - cur.bv.surface_area());

            let children = [self.get(cur.children[0]), self.get(cur.children[1])];
            let mut child_costs = [0.0; 2];
            for (child, child_cost) in children.iter().zip(&mut child_costs) {
                let merged_surface_area = child.bv.union(&bv).surface_area();
                *child_cost = if child.is_leaf() {
                    merged_surface_area + min_push_down_cost
                } else {
                    merged_surface_area - child.bv.surface_area() + min_push_down_cost
                }
            }

            if new_parent_cost < child_costs[0] && new_parent_cost < child_costs[1] {
                break;
            }

            cur_id = if child_costs[0] < child_costs[1] {
                cur.children[0]
            } else {
                cur.children[1]
            };
        }

        let sibling_id = cur_id;
        let sibling = self.get(sibling_id);
        let old_parent_id = sibling.parent;
        let new_parent_bv = bv.union(&sibling.bv);
        let new_parent_id = self.alloc_node(new_parent_bv, None);
        let new_parent = self.get_mut(new_parent_id);
        new_parent.children = [sibling_id, node_id];
        new_parent.parent = old_parent_id;
        self.get_mut(node_id).parent = new_parent_id;
        self.get_mut(sibling_id).parent = new_parent_id;

        if old_parent_id == NULL_NODE {
            self.root = new_parent_id;
        } else {
            let old_parent = self.get_mut(old_parent_id);
            if old_parent.children[0] == sibling_id {
                old_parent.children[0] = new_parent_id;
            } else {
                old_parent.children[1] = new_parent_id;
            }
        }

        self.fix_upwards(new_parent_id);
        NodeId(node_id)
    }

    /// Remove a node
    pub fn remove(&mut self, NodeId(id): NodeId) {
        if id == self.root {
            self.root = NULL_NODE;
            self.nodes.clear();
            return;
        }

        let parent_id = self.get(id).parent;
        let parent = self.get(parent_id);
        let sibling_id = if parent.children[0] == id {
            parent.children[1]
        } else {
            parent.children[0]
        };

        let grandparent_id = self.get(parent_id).parent;
        if grandparent_id == NULL_NODE {
            self.root = sibling_id;
            self.get_mut(sibling_id).parent = NULL_NODE;
            self.nodes.remove(parent_id as usize);
        } else {
            let grandparent = self.get_mut(grandparent_id);
            if grandparent.children[0] == parent_id {
                grandparent.children[0] = sibling_id;
            } else {
                grandparent.children[1] = sibling_id;
            }

            self.nodes.remove(parent_id as usize);
            self.get_mut(sibling_id).parent = grandparent_id;
            self.fix_upwards(grandparent_id);
        }

        self.nodes.remove(id as usize);
    }

    fn fix_upwards(&mut self, mut node_id: u32) {
        while node_id != NULL_NODE {
            let node = self.get(node_id);
            let parent = node.parent;
            let [left_id, right_id] = node.children;
            let bv = self.get(left_id).bv.union(&self.get(right_id).bv);
            self.get_mut(node_id).bv = bv;
            node_id = parent;
        }
    }

    #[inline(always)]
    fn next_overlap(&self, bv: &BV, stack: &mut Vec<u32>) -> Option<(&BV, &T)> {
        while let Some(node_id) = stack.pop() {
            if node_id == NULL_NODE {
                continue;
            }

            let node = self.get(node_id);
            if node.bv.overlaps(&bv) {
                if let Some(ref data) = node.data {
                    return Some((&node.bv, data));
                } else {
                    stack.extend(node.children.iter().copied());
                }
            }
        }

        None
    }

    /// Check whether the given AABR overlaps with any AABR contained in this tree
    pub fn any_overlap(&self, bv: BV) -> bool {
        let mut stack = Vec::new();
        stack.push(self.root);
        self.next_overlap(&bv, &mut stack).is_some()
    }

    /// Iterate through all overlaps
    pub fn iter_overlaps(&self, bv: BV) -> impl Iterator<Item = (&BV, &T)> + '_ {
        let mut stack = Vec::new();
        stack.push(self.root);
        std::iter::from_fn(move || self.next_overlap(&bv, &mut stack))
    }

    /// Iterate through non leaf nodes
    pub fn iter_branches(&self) -> impl Iterator<Item = &BV> + '_ {
        self.nodes
            .iter()
            .filter(|(_, n)| !n.is_leaf())
            .map(|(_, n)| &n.bv)
    }

    /// Iterate through leaf nodes
    pub fn iter_leaves(&self) -> impl Iterator<Item = (&BV, &T)> + '_ {
        self.nodes
            .iter()
            .flat_map(|(_, n)| n.data.as_ref().map(|d| (&n.bv, d)))
    }
}
