use nalgebra::Vector2;

/// Orientation (horizontal or vertical)
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Orientation {
    /// Vertical orientation (Y axis)
    Vertical,
    /// Horizontal orientation (X axis)
    Horizontal,
}

impl Orientation {
    /// Get orientation as a vector (left-to-right, top-to-bottom, normalized)
    pub fn as_vector(self) -> Vector2<f32> {
        match self {
            Orientation::Vertical => Vector2::<f32>::new(0.0, 1.0),
            Orientation::Horizontal => Vector2::<f32>::new(1.0, 0.0),
        }
    }

    /// Get orientation orthogonal to `self`
    pub fn orthogonal(self) -> Orientation {
        match self {
            Orientation::Vertical => Orientation::Horizontal,
            Orientation::Horizontal => Orientation::Vertical,
        }
    }
}
