use legion::component;

use super::WorldTransform;
use crate::assets::Handle;
use crate::graphics::draw3d::{AnimationMix, DrawList3DEncoder, Instance, Model};
use crate::ScheduleBuilder;

#[legion::system(for_each)]
fn animated(
    #[resource] encoder: &mut DrawList3DEncoder,
    transform: &WorldTransform,
    model: &Handle<Model>,
    animations: &Vec<AnimationMix>,
) {
    let instance = Instance::new(transform.0.to_homogeneous());
    encoder.model(instance, model, animations.clone());
}

#[legion::system(for_each)]
#[filter(!component::<Vec<AnimationMix>>())]
fn nonanimated(
    #[resource] encoder: &mut DrawList3DEncoder,
    transform: &WorldTransform,
    model: &Handle<Model>,
) {
    let instance = Instance::new(transform.0.to_homogeneous());
    encoder.model(instance, model, Vec::new());
}

pub fn drawing_bundle(schedule: &mut ScheduleBuilder) {
    schedule.on_stage("DRAW", |stage| {
        stage
            .add_system(animated_system())
            .add_system(nonanimated_system());
    });
}
