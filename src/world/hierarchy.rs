use fxhash::FxHashSet;
use legion::systems::CommandBuffer;
use legion::world::{EntityStore, SubWorld};
use legion::{maybe_changed, Entity};

use crate::ScheduleBuilder;

/// Parent ID. Source of truth for hierarchy computations
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Parent(pub Entity);

/// Previous parent. Internal
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct PrevParent(pub Entity);

/// Set of children in entity hierarchy. May be outdated
#[derive(Clone, Debug)]
pub struct Children(pub FxHashSet<Entity>);

// TODO: handle removed entities with parent

#[legion::system(for_each)]
#[write_component(Children)]
pub fn handle_removed_parent(
    world: &mut SubWorld,
    cbuf: &mut CommandBuffer,
    &entity: &Entity,
    parent: &PrevParent,
) {
    cbuf.remove_component::<PrevParent>(entity);

    let mut parent = match world.entry_mut(parent.0) {
        Ok(v) => v,
        Err(_) => return,
    };

    let children = match parent.get_component_mut::<Children>() {
        Ok(v) => v,
        Err(_) => return,
    };

    children.0.remove(&entity);
}

#[legion::system(for_each)]
#[write_component(Children)]
#[filter(maybe_changed::<Parent>())]
pub fn handle_changed_parent(
    world: &mut SubWorld,
    cbuf: &mut CommandBuffer,
    &entity: &Entity,
    parent: &Parent,
    prev_parent: Option<&mut PrevParent>,
) {
    let parent_id = parent.0;
    let prev_parent_id = prev_parent.as_ref().map(|v| v.0);

    if Some(parent_id) == prev_parent_id {
        // parent wasn't changed
        return;
    }

    // insert into parent's children

    if let Ok(mut parent) = world.entry_mut(parent_id) {
        if let Ok(children) = parent.get_component_mut::<Children>() {
            children.0.insert(entity);
        } else {
            let mut children = FxHashSet::default();
            children.insert(entity);
            cbuf.add_component(parent_id, Children(children));
        }
    }

    // remove from previous parent's children

    if let Some(mut prev_parent) = prev_parent_id.and_then(|id| world.entry_mut(id).ok()) {
        if let Ok(children) = prev_parent.get_component_mut::<Children>() {
            children.0.remove(&entity);
        }
    }

    // update previous parent

    match prev_parent {
        Some(prev_parent) => prev_parent.0 = parent_id,
        None => cbuf.add_component(entity, PrevParent(parent_id)),
    }
}

pub fn hierarchy_bundle(schedule: &mut ScheduleBuilder) {
    schedule.add_stage_before("DRAW", "HIERARCHY");
    schedule.on_stage("HIERARCHY", |stage| {
        stage
            .add_system(handle_removed_parent_system())
            .add_system(handle_changed_parent_system());
    });
}
