use legion::systems::CommandBuffer;
use legion::world::{EntityStore, SubWorld};
use legion::{component, maybe_changed, Entity};
use nalgebra::{Point3, Similarity3, Translation, UnitQuaternion};

use super::{Children, Parent};
use crate::ScheduleBuilder;

/// Position component
#[derive(Clone, Copy, Debug)]
pub struct Position(pub Point3<f32>);

/// Rotation component
#[derive(Clone, Copy, Debug)]
pub struct Rotation(pub UnitQuaternion<f32>);

/// Scale component
#[derive(Clone, Copy, Debug)]
pub struct Scale(pub f32);

/// Local transformation matrix (based on local position, rotation, and scale)
#[derive(Clone, Copy, Debug)]
pub struct LocalTransform(pub Similarity3<f32>);

/// World transformation matrix (includes parent transform)
#[derive(Clone, Copy, Debug)]
pub struct WorldTransform(pub Similarity3<f32>);

/// Add transformation components to entities with position, rotation, or scale
#[legion::system(for_each)]
#[filter((component::<Position>()
    | component::<Rotation>()
    | component::<Scale>())
    & !component::<LocalTransform>()
    & !component::<WorldTransform>())]
fn add_transform_components(cbuf: &mut CommandBuffer, &entity: &Entity) {
    cbuf.add_component(entity, LocalTransform(Similarity3::identity()));
    cbuf.add_component(entity, WorldTransform(Similarity3::identity()));
}

#[legion::system(for_each)]
#[filter(maybe_changed::<Position>())]
#[filter(!component::<Rotation>() & !component::<Scale>())]
fn local_trans_p(pos: &Position, trans: &mut LocalTransform) {
    let translation = Translation::from(pos.0.coords);
    trans.0 = nalgebra::convert(translation);
}

#[legion::system(for_each)]
#[filter(maybe_changed::<Position>() | maybe_changed::<Rotation>())]
#[filter(!component::<Scale>())]
fn local_trans_pr(pos: &Position, rot: &Rotation, trans: &mut LocalTransform) {
    let translation = Translation::from(pos.0.coords);
    trans.0 = Similarity3::from_parts(translation, rot.0, 1.0);
}

#[legion::system(for_each)]
#[filter(maybe_changed::<Position>() | maybe_changed::<Rotation>() | maybe_changed::<Scale>())]
fn local_trans_prs(pos: &Position, rot: &Rotation, scale: &Scale, trans: &mut LocalTransform) {
    let translation = Translation::from(pos.0.coords);
    trans.0 = Similarity3::from_parts(translation, rot.0, scale.0);
}

#[legion::system(for_each)]
#[filter(maybe_changed::<Rotation>())]
#[filter(!component::<Position>() & !component::<Scale>())]
fn local_trans_r(rot: &Rotation, trans: &mut LocalTransform) {
    trans.0 = nalgebra::convert(rot.0);
}

#[legion::system(for_each)]
#[filter(maybe_changed::<Rotation>() | maybe_changed::<Scale>())]
#[filter(!component::<Position>())]
fn local_trans_rs(rot: &Rotation, scale: &Scale, trans: &mut LocalTransform) {
    trans.0 = Similarity3::from_parts(Translation::identity(), rot.0, scale.0);
}

#[legion::system(for_each)]
#[filter(maybe_changed::<Scale>())]
#[filter(!component::<Rotation>() & !component::<Scale>())]
fn local_trans_s(scale: &Scale, trans: &mut LocalTransform) {
    trans.0 = Similarity3::from_parts(Translation::identity(), UnitQuaternion::identity(), scale.0);
}

#[legion::system(for_each)]
#[filter(!component::<Parent>())]
#[write_component(Children)]
#[write_component(LocalTransform)]
#[write_component(WorldTransform)]
fn propagate_trans(world: &mut SubWorld, cbuf: &mut CommandBuffer, entity: &Entity) {
    propagate_trans_rec(world, cbuf, &Similarity3::identity(), *entity);
}

fn propagate_trans_rec(
    world: &mut SubWorld,
    cbuf: &mut CommandBuffer,
    parent_world: &Similarity3<f32>,
    child_id: Entity,
) {
    let mut child = match world.entry_mut(child_id) {
        Ok(v) => v,
        Err(_) => return,
    };

    let child_local = match child.get_component::<LocalTransform>() {
        Ok(v) => v,
        Err(_) => return,
    };

    let child_world = WorldTransform(parent_world * child_local.0);
    match child.get_component_mut::<WorldTransform>() {
        Ok(v) => *v = child_world,
        Err(_) => cbuf.add_component(child_id, child_world),
    }

    let grandchildren = child
        .get_component::<Children>()
        .map(|v| v.0.clone())
        .unwrap_or_default();
    for &grandchild_id in &grandchildren {
        propagate_trans_rec(world, cbuf, &child_world.0, grandchild_id);
    }
}

pub fn transform_bundle(schedule: &mut ScheduleBuilder) {
    schedule.add_stage_after("HIERARCHY", "TRANSFORM");
    schedule.on_stage("TRANSFORM", |stage| {
        stage
            .add_system(add_transform_components_system())
            .flush()
            .add_system(local_trans_p_system())
            .add_system(local_trans_pr_system())
            .add_system(local_trans_prs_system())
            .add_system(local_trans_r_system())
            .add_system(local_trans_rs_system())
            .add_system(local_trans_s_system())
            .flush()
            .add_system(propagate_trans_system());
    });
}
