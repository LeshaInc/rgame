#version 450

layout(location = 0) in vec3 vPos;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform sampler uSampler;
layout(set = 0, binding = 1) uniform texture2D uTexture;

const vec2 invAtan = vec2(0.1591, 0.3183);

vec2 sampleSphericalMap(vec3 v) {
    vec2 uv = vec2(atan(v.y, v.x), asin(v.z));
    uv *= invAtan;
    uv += 0.5;
    return uv;
}

void main() {		
    vec2 uv = sampleSphericalMap(normalize(vPos));
    vec3 color = texture(sampler2D(uTexture, uSampler), uv).rgb;
    outColor = vec4(color, 1.0);
}

