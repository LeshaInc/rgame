#version 450

layout (location = 0) in vec3 inPos;

layout (location = 0) out vec3 vPos;

layout(push_constant) uniform PushConstants {
    mat4 uViewProjection;
};

void main() {
    vPos = inPos;
    gl_Position = uViewProjection * vec4(inPos, 1.0);
    gl_Position.y *= -1;
}
