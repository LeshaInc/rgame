#version 450

layout(location = 0) in vec3 vPos;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform sampler uSampler;
layout(set = 0, binding = 1) uniform textureCube uTexture;

const float PI = 3.14159265359;

void main() {
    vec3 normal = normalize(vPos);
    vec3 irradiance = vec3(0.0);   

    vec3 up = vec3(0.0, 0.0, 1.0);
    vec3 right = normalize(cross(up, normal));
    up = normalize(cross(normal, right));

    float sampleDelta = 0.010;
    float numSamples = 0.0;

    for(float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta) {
        for(float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta) {
            vec3 tangentSample = vec3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
            vec3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * normal; 
            irradiance += texture(samplerCube(uTexture, uSampler), sampleVec).rgb * cos(theta) * sin(theta);
            numSamples++;
        }
    }

    irradiance = PI * irradiance * (1.0 / float(numSamples));

    outColor = vec4(irradiance, 1.0);
}
