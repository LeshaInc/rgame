#version 450

layout(location = 0) in vec3 vPos;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform sampler uSampler;
layout(set = 0, binding = 1) uniform textureCube uTexture;

void main() {
    outColor = vec4(texture(samplerCube(uTexture, uSampler), vPos).rgb, 1.0);
}
