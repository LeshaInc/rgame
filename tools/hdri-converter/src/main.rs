use std::num::NonZeroU32;

use std::fs::File;
use std::io::BufReader;
use std::path::{Path, PathBuf};

use anyhow::{Context, Result};
use futures::executor::block_on;
use image::codecs::hdr::{HdrDecoder, HdrEncoder};
use image::{Pixel, Rgb};
use nalgebra::{Matrix4, Vector3};
use structopt::StructOpt;
use wgpu::util::DeviceExt as _;

#[rustfmt::skip]
const CUBE_VERTICES: &[f32] = &[
    -1.0, -1.0, -1.0,
    -1.0,  1.0, -1.0,
    -1.0,  1.0,  1.0,
    -1.0, -1.0, -1.0,
    -1.0,  1.0,  1.0,
    -1.0, -1.0,  1.0,

     1.0, -1.0, -1.0,
     1.0,  1.0, -1.0,
     1.0,  1.0,  1.0,
     1.0, -1.0, -1.0,
     1.0,  1.0,  1.0,
     1.0, -1.0,  1.0,

    -1.0, -1.0, -1.0,
     1.0, -1.0, -1.0,
     1.0, -1.0,  1.0,
    -1.0, -1.0, -1.0,
     1.0, -1.0,  1.0,
    -1.0, -1.0,  1.0,

    -1.0,  1.0, -1.0,
     1.0,  1.0, -1.0,
     1.0,  1.0,  1.0,
    -1.0,  1.0, -1.0,
     1.0,  1.0,  1.0,
    -1.0,  1.0,  1.0,

    -1.0, -1.0, -1.0,
     1.0, -1.0, -1.0,
     1.0,  1.0, -1.0,
    -1.0, -1.0, -1.0,
     1.0,  1.0, -1.0,
    -1.0,  1.0, -1.0,
                     
    -1.0, -1.0,  1.0,
     1.0, -1.0,  1.0,
     1.0,  1.0,  1.0,
    -1.0, -1.0,  1.0,
     1.0,  1.0,  1.0,
    -1.0,  1.0,  1.0,
];

fn create_projection() -> Matrix4<f32> {
    let persp = Matrix4::new_perspective(1.0, 90f32.to_radians(), 0.1, 10.0);
    let fix_z = Matrix4::new_translation(&Vector3::new(0.0, 0.0, 0.5))
        * Matrix4::new_nonuniform_scaling(&Vector3::new(1.0, 1.0, 0.5));
    fix_z * persp
}

#[rustfmt::skip]
fn create_views() -> [Matrix4<f32>; 6] {
    let r90 = 90f32.to_radians();
    let r180 = 180f32.to_radians();
    [
        Matrix4::from_euler_angles( 0.0,  r90, r180),
        Matrix4::from_euler_angles( 0.0, -r90, r180),
        Matrix4::from_euler_angles(-r90,  0.0,  0.0),
        Matrix4::from_euler_angles( r90,  0.0,  0.0),
        Matrix4::from_euler_angles( 0.0, r180, r180),
        Matrix4::from_euler_angles( 0.0,  0.0, r180),
    ]
}

struct Pipeline {
    device: wgpu::Device,
    queue: wgpu::Queue,
    cube: wgpu::Buffer,
    sampler: wgpu::Sampler,
    convert_bind_group_layout: wgpu::BindGroupLayout,
    cubemap_bind_group_layout: wgpu::BindGroupLayout,
    convert_pipeline: wgpu::RenderPipeline,
    mipmap_pipeline: wgpu::RenderPipeline,
    irradiance_pipeline: wgpu::RenderPipeline,
    filtering_pipeline: wgpu::RenderPipeline,
}

impl Pipeline {
    fn new() -> Result<Pipeline> {
        let instance = wgpu::Instance::new(wgpu::BackendBit::PRIMARY);

        let adapter = block_on(instance.request_adapter(&wgpu::RequestAdapterOptions::default()))
            .context("No adapter")?;

        let desc = &wgpu::DeviceDescriptor {
            features: wgpu::Features::PUSH_CONSTANTS,
            limits: wgpu::Limits {
                max_push_constant_size: 68,
                ..Default::default()
            },
            shader_validation: true,
        };

        let (device, queue) =
            block_on(adapter.request_device(&desc, None)).context("Cannot open device")?;

        let sampler_desc = wgpu::BindGroupLayoutEntry {
            binding: 0,
            visibility: wgpu::ShaderStage::FRAGMENT,
            ty: wgpu::BindingType::Sampler { comparison: false },
            count: None,
        };

        let convert_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: None,
                entries: &[
                    sampler_desc.clone(),
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStage::FRAGMENT,
                        ty: wgpu::BindingType::SampledTexture {
                            dimension: wgpu::TextureViewDimension::D2,
                            component_type: wgpu::TextureComponentType::Float,
                            multisampled: false,
                        },
                        count: None,
                    },
                ],
            });

        let cubemap_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: None,
                entries: &[
                    sampler_desc,
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStage::FRAGMENT,
                        ty: wgpu::BindingType::SampledTexture {
                            dimension: wgpu::TextureViewDimension::Cube,
                            component_type: wgpu::TextureComponentType::Float,
                            multisampled: false,
                        },
                        count: None,
                    },
                ],
            });

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[&convert_bind_group_layout],
            push_constant_ranges: &[wgpu::PushConstantRange {
                stages: wgpu::ShaderStage::VERTEX,
                range: 0..64,
            }],
        });

        let vert_shader =
            device.create_shader_module(wgpu::include_spirv!("../shaders/cube.vert.spv"));
        let frag_shader =
            device.create_shader_module(wgpu::include_spirv!("../shaders/convert.frag.spv"));

        let pipeline_desc = wgpu::RenderPipelineDescriptor {
            label: None,
            layout: Some(&pipeline_layout),
            vertex_stage: wgpu::ProgrammableStageDescriptor {
                module: &vert_shader,
                entry_point: "main",
            },
            fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
                module: &frag_shader,
                entry_point: "main",
            }),
            rasterization_state: Some(wgpu::RasterizationStateDescriptor {
                ..Default::default()
            }),
            primitive_topology: wgpu::PrimitiveTopology::TriangleList,
            color_states: &[wgpu::ColorStateDescriptor {
                format: wgpu::TextureFormat::Rgba32Float,
                alpha_blend: wgpu::BlendDescriptor::default(),
                color_blend: wgpu::BlendDescriptor::default(),
                write_mask: wgpu::ColorWrite::all(),
            }],
            depth_stencil_state: Some(wgpu::DepthStencilStateDescriptor {
                format: wgpu::TextureFormat::Depth24Plus,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::Less,
                stencil: wgpu::StencilStateDescriptor::default(),
            }),
            vertex_state: wgpu::VertexStateDescriptor {
                index_format: wgpu::IndexFormat::Uint32,
                vertex_buffers: &[wgpu::VertexBufferDescriptor {
                    stride: 12,
                    step_mode: wgpu::InputStepMode::Vertex,
                    attributes: &[wgpu::VertexAttributeDescriptor {
                        offset: 0,
                        format: wgpu::VertexFormat::Float3,
                        shader_location: 0,
                    }],
                }],
            },
            sample_count: 1,
            sample_mask: !0,
            alpha_to_coverage_enabled: false,
        };

        let convert_pipeline = device.create_render_pipeline(&pipeline_desc);

        let frag_shader =
            device.create_shader_module(wgpu::include_spirv!("../shaders/mipmap.frag.spv"));

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[&cubemap_bind_group_layout],
            push_constant_ranges: &[wgpu::PushConstantRange {
                stages: wgpu::ShaderStage::VERTEX,
                range: 0..64,
            }],
        });

        let mipmap_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            layout: Some(&pipeline_layout),
            fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
                module: &frag_shader,
                entry_point: "main",
            }),
            ..pipeline_desc.clone()
        });

        let frag_shader =
            device.create_shader_module(wgpu::include_spirv!("../shaders/irradiance.frag.spv"));

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[&cubemap_bind_group_layout],
            push_constant_ranges: &[wgpu::PushConstantRange {
                stages: wgpu::ShaderStage::VERTEX,
                range: 0..64,
            }],
        });

        let irradiance_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            layout: Some(&pipeline_layout),
            fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
                module: &frag_shader,
                entry_point: "main",
            }),
            ..pipeline_desc.clone()
        });

        let frag_shader =
            device.create_shader_module(wgpu::include_spirv!("../shaders/filtering.frag.spv"));

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[&cubemap_bind_group_layout],
            push_constant_ranges: &[
                wgpu::PushConstantRange {
                    stages: wgpu::ShaderStage::VERTEX,
                    range: 0..64,
                },
                wgpu::PushConstantRange {
                    stages: wgpu::ShaderStage::FRAGMENT,
                    range: 64..68,
                },
            ],
        });

        let filtering_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            layout: Some(&pipeline_layout),
            fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
                module: &frag_shader,
                entry_point: "main",
            }),
            ..pipeline_desc
        });

        let cube = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: None,
            contents: view_as_bytes(&CUBE_VERTICES),
            usage: wgpu::BufferUsage::VERTEX,
        });

        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Linear,
            ..Default::default()
        });

        Ok(Pipeline {
            device,
            queue,
            cube,
            sampler,
            convert_bind_group_layout,
            cubemap_bind_group_layout,
            convert_pipeline,
            mipmap_pipeline,
            irradiance_pipeline,
            filtering_pipeline,
        })
    }

    fn create_cubemap(&self, size: u32, mip: u32, usage: wgpu::TextureUsage) -> wgpu::Texture {
        self.device.create_texture(&wgpu::TextureDescriptor {
            label: None,
            size: wgpu::Extent3d {
                width: size,
                height: size,
                depth: 6,
            },
            mip_level_count: mip,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba32Float,
            usage,
        })
    }

    fn upload_hdri(&self, width: u32, height: u32, data: &[Rgb<f32>]) -> wgpu::Texture {
        let size = wgpu::Extent3d {
            width,
            height,
            depth: 1,
        };

        let texture = self.device.create_texture(&wgpu::TextureDescriptor {
            label: None,
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba32Float,
            usage: wgpu::TextureUsage::SAMPLED | wgpu::TextureUsage::COPY_DST,
        });

        let dst = wgpu::TextureCopyView {
            texture: &texture,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
        };

        let layout = wgpu::TextureDataLayout {
            offset: 0,
            bytes_per_row: width * 16,
            rows_per_image: height,
        };

        let data = data.iter().map(|rgb| rgb.to_rgba()).collect::<Vec<_>>();
        self.queue
            .write_texture(dst, view_as_bytes(&data), layout, size);

        texture
    }

    fn download_cubemap(&self, size: u32, mip: u32, cubemap: &wgpu::Texture) -> Vec<Rgb<f32>> {
        let buffer = self.device.create_buffer(&wgpu::BufferDescriptor {
            label: None,
            size: (size * size * 16 * 6) as u64,
            usage: wgpu::BufferUsage::COPY_DST | wgpu::BufferUsage::MAP_READ,
            mapped_at_creation: false,
        });

        let mut encoder = self.device.create_command_encoder(&Default::default());

        let src = wgpu::TextureCopyView {
            texture: &cubemap,
            mip_level: mip,
            origin: wgpu::Origin3d::ZERO,
        };

        let dst = wgpu::BufferCopyView {
            buffer: &buffer,
            layout: wgpu::TextureDataLayout {
                offset: 0,
                bytes_per_row: size * 16,
                rows_per_image: size,
            },
        };

        let extent = wgpu::Extent3d {
            width: size,
            height: size,
            depth: 6,
        };

        encoder.copy_texture_to_buffer(src, dst, extent);
        self.queue.submit(Some(encoder.finish()));

        let slice = buffer.slice(..);
        let map_future = slice.map_async(wgpu::MapMode::Read);
        self.device.poll(wgpu::Maintain::Wait);
        block_on(map_future).unwrap();

        let mapping = slice.get_mapped_range();
        let rgba: &[[f32; 4]] = unsafe {
            std::slice::from_raw_parts(mapping.as_ptr() as *const [f32; 4], mapping.len() * 16)
        };

        let size = size as usize;
        let mut out = vec![Rgb([0.0; 3]); 6 * size * size];

        for side in 0..6 {
            for x in 0..size {
                for y in 0..size {
                    let rgba = rgba[x + (y + side * size) * size];
                    out[x + side * size + y * 6 * size] = Rgb([rgba[0], rgba[1], rgba[2]]);
                }
            }
        }

        out
    }

    fn download_cubemap_hdr(
        &self,
        size: u32,
        mip: u32,
        cubemap: &wgpu::Texture,
    ) -> Result<Vec<u8>> {
        let data = self.download_cubemap(size, mip, cubemap);
        let mut buffer = Vec::new();
        let encoder = HdrEncoder::new(&mut buffer);
        encoder.encode(&data, 6 * size as usize, size as usize)?;
        Ok(buffer)
    }

    fn render_to_cubemap(
        &self,
        pipeline: &wgpu::RenderPipeline,
        bind_group: &wgpu::BindGroup,
        size: u32,
        mip: u32,
        float: Option<f32>,
        cubemap: &wgpu::Texture,
    ) {
        let mut encoder = self.device.create_command_encoder(&Default::default());

        let depth = self.device.create_texture(&wgpu::TextureDescriptor {
            label: None,
            size: wgpu::Extent3d {
                width: size,
                height: size,
                depth: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Depth24Plus,
            usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
        });

        let depth_view = depth.create_view(&wgpu::TextureViewDescriptor::default());

        for side in 0..6 {
            let cubemap_view = cubemap.create_view(&wgpu::TextureViewDescriptor {
                label: None,
                format: Some(wgpu::TextureFormat::Rgba32Float),
                dimension: Some(wgpu::TextureViewDimension::D2),
                aspect: wgpu::TextureAspect::All,
                base_mip_level: mip,
                level_count: Some(NonZeroU32::new(1).unwrap()),
                base_array_layer: side,
                array_layer_count: Some(NonZeroU32::new(1).unwrap()),
            });

            let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                    attachment: &cubemap_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                        store: true,
                    },
                }],
                depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachmentDescriptor {
                    attachment: &depth_view,
                    depth_ops: Some(wgpu::Operations {
                        load: wgpu::LoadOp::Clear(1.0),
                        store: true,
                    }),
                    stencil_ops: None,
                }),
            });

            rpass.set_pipeline(pipeline);
            rpass.set_vertex_buffer(0, self.cube.slice(..));
            rpass.set_bind_group(0, bind_group, &[]);

            let view_projection = create_projection() * create_views()[side as usize];

            let pc = unsafe {
                std::slice::from_raw_parts(view_projection.as_slice().as_ptr() as *const u32, 16)
            };
            rpass.set_push_constants(wgpu::ShaderStage::VERTEX, 0, pc);

            if let Some(float) = float {
                rpass.set_push_constants(wgpu::ShaderStage::FRAGMENT, 64, &[float.to_bits()]);
            }

            rpass.draw(0..(CUBE_VERTICES.len() / 3) as u32, 0..1);
        }

        let cbuf = encoder.finish();
        self.queue.submit(Some(cbuf));
    }

    fn hdri_to_cubemap(&self, hdri: &wgpu::Texture, cubemap_size: u32, cubemap: &wgpu::Texture) {
        let hdri_view = hdri.create_view(&wgpu::TextureViewDescriptor {
            label: None,
            format: Some(wgpu::TextureFormat::Rgba32Float),
            dimension: Some(wgpu::TextureViewDimension::D2),
            aspect: wgpu::TextureAspect::All,
            base_mip_level: 0,
            level_count: None,
            base_array_layer: 0,
            array_layer_count: None,
        });

        let bind_group = self.device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: None,
            layout: &self.convert_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::Sampler(&self.sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::TextureView(&hdri_view),
                },
            ],
        });

        self.render_to_cubemap(
            &self.convert_pipeline,
            &bind_group,
            cubemap_size,
            0,
            None,
            &cubemap,
        );
    }

    fn gen_mipmaps(&self, cubemap: &wgpu::Texture, size: u32, mip_count: u32) {
        for out_mip in 1..mip_count {
            let input_view = cubemap.create_view(&wgpu::TextureViewDescriptor {
                label: None,
                format: Some(wgpu::TextureFormat::Rgba32Float),
                dimension: Some(wgpu::TextureViewDimension::Cube),
                aspect: wgpu::TextureAspect::All,
                base_mip_level: 0,
                level_count: Some(NonZeroU32::new(out_mip).unwrap()),
                base_array_layer: 0,
                array_layer_count: Some(NonZeroU32::new(6).unwrap()),
            });

            let bind_group = self.device.create_bind_group(&wgpu::BindGroupDescriptor {
                label: None,
                layout: &self.cubemap_bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: wgpu::BindingResource::Sampler(&self.sampler),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: wgpu::BindingResource::TextureView(&input_view),
                    },
                ],
            });

            let size = size / 2u32.pow(out_mip);

            self.render_to_cubemap(
                &self.mipmap_pipeline,
                &bind_group,
                size,
                out_mip,
                None,
                &cubemap,
            );
        }
    }

    fn gen_irradiance_map(&self, input: &wgpu::Texture, out_size: u32, output: &wgpu::Texture) {
        let input_view = input.create_view(&wgpu::TextureViewDescriptor {
            label: None,
            format: Some(wgpu::TextureFormat::Rgba32Float),
            dimension: Some(wgpu::TextureViewDimension::Cube),
            aspect: wgpu::TextureAspect::All,
            base_mip_level: 0,
            level_count: None,
            base_array_layer: 0,
            array_layer_count: Some(NonZeroU32::new(6).unwrap()),
        });

        let bind_group = self.device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: None,
            layout: &self.cubemap_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::Sampler(&self.sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::TextureView(&input_view),
                },
            ],
        });

        self.render_to_cubemap(
            &self.irradiance_pipeline,
            &bind_group,
            out_size,
            0,
            None,
            &output,
        );
    }

    fn gen_prefilter_map(
        &self,
        input: &wgpu::Texture,
        out_size: u32,
        mip_count: u32,
        output: &wgpu::Texture,
    ) {
        for out_mip in 0..mip_count {
            let input_view = input.create_view(&wgpu::TextureViewDescriptor {
                label: None,
                format: Some(wgpu::TextureFormat::Rgba32Float),
                dimension: Some(wgpu::TextureViewDimension::Cube),
                aspect: wgpu::TextureAspect::All,
                base_mip_level: 0,
                level_count: None,
                base_array_layer: 0,
                array_layer_count: Some(NonZeroU32::new(6).unwrap()),
            });

            let bind_group = self.device.create_bind_group(&wgpu::BindGroupDescriptor {
                label: None,
                layout: &self.cubemap_bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: wgpu::BindingResource::Sampler(&self.sampler),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: wgpu::BindingResource::TextureView(&input_view),
                    },
                ],
            });

            let roughness = out_mip as f32 / (mip_count - 1) as f32;
            let size = out_size / 2u32.pow(out_mip);

            self.render_to_cubemap(
                &self.filtering_pipeline,
                &bind_group,
                size,
                out_mip,
                Some(roughness),
                &output,
            );
        }
    }
}

pub fn view_as_bytes<T>(slice: &[T]) -> &[u8] {
    unsafe {
        std::slice::from_raw_parts(
            slice.as_ptr() as *const u8,
            slice.len() * std::mem::size_of::<T>(),
        )
    }
}

#[derive(Debug, StructOpt)]
struct Opt {
    /// Input files in HDR format
    #[structopt(parse(from_os_str), required = true)]
    input: Vec<PathBuf>,
    /// Assets directory
    #[structopt(
        short = "o",
        long = "output",
        parse(from_os_str),
        default_value = "assets"
    )]
    output: PathBuf,
}

fn process_hdr(pipeline: &Pipeline, opt: &Opt, input: &Path) -> Result<()> {
    // let mut renderdoc: renderdoc::RenderDoc<renderdoc::V100> = renderdoc::RenderDoc::new()?;
    // renderdoc.start_frame_capture(std::ptr::null(), std::ptr::null());

    let reader = BufReader::new(File::open(input)?);
    let decoder = HdrDecoder::new(reader)?;
    let metadata = decoder.metadata();
    let hdri = decoder.read_image_hdr()?;

    let cubemap_size = 512;
    let cubemap_mips = 6;
    let irradiance_map_size = 32;
    let prefilter_map_size = 512;
    let prefilter_map_mips = 6;

    let hdri_texture = pipeline.upload_hdri(metadata.width, metadata.height, &hdri);
    let usage = wgpu::TextureUsage::OUTPUT_ATTACHMENT
        | wgpu::TextureUsage::SAMPLED
        | wgpu::TextureUsage::COPY_SRC;
    let cubemap = pipeline.create_cubemap(cubemap_size, cubemap_mips, usage);
    pipeline.hdri_to_cubemap(&hdri_texture, cubemap_size, &cubemap);
    pipeline.gen_mipmaps(&cubemap, cubemap_size, cubemap_mips);

    let usage = wgpu::TextureUsage::OUTPUT_ATTACHMENT | wgpu::TextureUsage::COPY_SRC;
    let irradiance_map = pipeline.create_cubemap(irradiance_map_size, 1, usage);
    pipeline.gen_irradiance_map(&cubemap, irradiance_map_size, &irradiance_map);

    let usage = wgpu::TextureUsage::OUTPUT_ATTACHMENT | wgpu::TextureUsage::COPY_SRC;
    let prefilter_map = pipeline.create_cubemap(prefilter_map_size, prefilter_map_mips, usage);
    pipeline.gen_prefilter_map(
        &cubemap,
        prefilter_map_size,
        prefilter_map_mips,
        &prefilter_map,
    );

    let name = input.file_name().unwrap();
    let mut out = opt.output.clone();
    out.push("envmaps/");
    std::fs::create_dir_all(&out).with_context(|| format!("Cannot create {}", out.display()))?;
    out.push(name);
    out.set_extension("tar");
    let file = File::create(&out).with_context(|| format!("Cannot open {}", out.display()))?;

    let mut header = tar::Header::new_gnu();
    let mut tar = tar::Builder::new(file);

    let data = pipeline.download_cubemap_hdr(cubemap_size, 0, &cubemap)?;
    header.set_cksum();
    header.set_mode(0o644);
    header.set_size(data.len() as _);
    tar.append_data(&mut header, "sky.hdr", data.as_slice())?;

    let data = pipeline.download_cubemap_hdr(irradiance_map_size, 0, &irradiance_map)?;
    header.set_cksum();
    header.set_mode(0o644);
    header.set_size(data.len() as _);
    tar.append_data(&mut header, "irradiance.hdr", data.as_slice())?;

    for mip in 0..prefilter_map_mips {
        let size = prefilter_map_size / 2u32.pow(mip);
        let name = format!("prefilter{}.hdr", mip);
        let data = pipeline.download_cubemap_hdr(size, mip, &prefilter_map)?;
        header.set_cksum();
        header.set_mode(0o644);
        header.set_size(data.len() as _);
        tar.append_data(&mut header, name, data.as_slice())?;
    }

    // renderdoc.end_frame_capture(std::ptr::null(), std::ptr::null());

    Ok(())
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    let pipeline = Pipeline::new()?;

    for input in &opt.input {
        process_hdr(&pipeline, &opt, &input)?;
    }

    Ok(())
}
