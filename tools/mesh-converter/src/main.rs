use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::{Path, PathBuf};

use anyhow::{anyhow, bail, Context, Result};
use gltf::animation::util::ReadOutputs;
use gltf::animation::Property;
use gltf::mesh::Mesh;
use gltf::skin::Skin;
use gltf::{buffer, Document};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opt {
    /// Input files in Wavefront OBJ format
    #[structopt(parse(from_os_str), required = true)]
    input: Vec<PathBuf>,
    /// Assets directory
    #[structopt(
        short = "o",
        long = "output",
        parse(from_os_str),
        default_value = "assets"
    )]
    output: PathBuf,
}

fn write_f32_slice(writer: &mut impl Write, data: &[f32]) -> Result<()> {
    for v in data {
        writer.write_all(&v.to_le_bytes())?;
    }

    Ok(())
}

fn process_mesh(
    document: &Document,
    mesh: Mesh<'_>,
    skin: Option<Skin<'_>>,
    buffers: &[buffer::Data],
    opt: &Opt,
) -> Result<()> {
    let name = mesh.name().unwrap();

    let primitive = mesh.primitives().next().unwrap();
    if primitive.mode() != gltf::mesh::Mode::Triangles {
        return Ok(());
    }

    let reader = primitive.reader(|buf| Some(&buffers[buf.index()]));

    let indices = reader.read_indices().context("No indices")?;
    let pos = reader.read_positions().context("No positions")?;
    let norm = reader.read_normals().context("No normals")?;
    let tex = reader.read_tex_coords(0).context("No tex coords")?;

    let weights = reader.read_weights(0);
    let joints = reader.read_joints(0);

    let is_animated = weights.is_some() && joints.is_some();

    let mut path = opt.output.clone();
    path.push("meshes");
    path.push(name);
    path.set_extension(if is_animated { "amesh" } else { "mesh" });

    println!("Create {}", path.display());

    std::fs::create_dir_all(path.parent().unwrap())?;

    let mut writer = File::create(&path)
        .map(BufWriter::new)
        .with_context(|| anyhow!("Cannot open {}", path.display()))?;

    writer.write_all(if is_animated { b"AMESH" } else { b"MESH" })?;

    let indices = indices.into_u32();
    let num_indices = indices.len() as u32;
    writer.write_all(&num_indices.to_le_bytes())?;

    for index in indices {
        writer.write_all(&index.to_le_bytes())?;
    }

    let num_vertices = pos.len() as u32;
    writer.write_all(&num_vertices.to_le_bytes())?;

    let it = pos.zip(norm).zip(tex.into_f32());

    if is_animated {
        let weights = weights.unwrap();
        let joints = joints.unwrap();

        for (((pos, norm), tex), (weights, joints)) in
            it.zip(weights.into_f32().zip(joints.into_u16()))
        {
            write_f32_slice(&mut writer, &pos)?;
            write_f32_slice(&mut writer, &norm)?;
            write_f32_slice(&mut writer, &tex)?;

            if joints.iter().any(|v| *v > 255) {
                bail!("Too many joints");
            }

            write_f32_slice(&mut writer, &weights)?;

            for &j in &joints {
                writer.write_all(&[j as u8])?;
            }
        }
    } else {
        for ((pos, norm), tex) in it {
            write_f32_slice(&mut writer, &pos)?;
            write_f32_slice(&mut writer, &norm)?;
            write_f32_slice(&mut writer, &tex)?;
        }
    }

    if !is_animated {
        return Ok(());
    }

    let skin = skin.unwrap();

    let num_joints = skin.joints().count();

    let mut roots = HashSet::new();
    let mut parents = HashMap::new();

    for joint in skin.joints() {
        roots.insert(joint.index());
    }

    for joint in skin.joints() {
        for child in joint.children() {
            parents.insert(child.index(), joint.index());
            roots.remove(&child.index());
        }
    }

    let root_joint_idx = roots.into_iter().next().unwrap();
    let container_node = document
        .nodes()
        .find(|n| n.children().any(|c| c.index() == root_joint_idx))
        .unwrap();

    let container_transform = container_node.transform().matrix();
    for col in &container_transform {
        write_f32_slice(&mut writer, &*col)?;
    }

    let mut joint_map = HashMap::with_capacity(num_joints);
    for (i, joint) in skin.joints().enumerate() {
        joint_map.insert(joint.index(), i);
    }

    writer.write_all(&[num_joints as u8])?;
    writer.write_all(&[joint_map[&root_joint_idx] as u8])?;

    let reader = skin.reader(|buf| Some(&buffers[buf.index()]));
    let matrices = reader.read_inverse_bind_matrices().unwrap();

    for (matrix, joint) in matrices.zip(skin.joints()) {
        let parent = parents
            .get(&joint.index())
            .map(|v| joint_map[&v] as u8)
            .unwrap_or(255);
        writer.write_all(&[parent])?;

        for col in &matrix {
            write_f32_slice(&mut writer, &*col)?;
        }

        let (trans, rot, scale) = joint.transform().decomposed();
        write_f32_slice(&mut writer, &trans)?;
        write_f32_slice(&mut writer, &rot)?;
        write_f32_slice(&mut writer, &scale)?;
    }

    for animation in document.animations() {
        let name = animation.name().unwrap();
        let mut per_joint = HashMap::new();

        for channel in animation.channels() {
            let target = channel.target();
            let joint_idx = match joint_map.get(&target.node().index()) {
                Some(&v) => v as u8,
                None => continue,
            };

            let mut entry = per_joint.entry(joint_idx).or_insert((None, None, None));

            match target.property() {
                Property::Translation => entry.0 = Some(channel),
                Property::Rotation => entry.1 = Some(channel),
                Property::Scale => entry.2 = Some(channel),
                _ => bail!("Bad skeleton "),
            }
        }

        if per_joint.is_empty() {
            continue;
        }

        let name_len = name.len() as u32;
        writer.write_all(&name_len.to_le_bytes())?;
        writer.write_all(name.as_bytes())?;

        let num_joints_used = per_joint.len() as u8;
        writer.write_all(&[num_joints_used])?;

        for (joint, (t_chan, r_chan, s_chan)) in per_joint.into_iter() {
            writer.write_all(&[joint])?;

            let t_chan = t_chan.context("Bad skeleton")?;
            let r_chan = r_chan.context("Bad skeleton")?;
            let s_chan = s_chan.context("Bad skeleton")?;

            let t_reader = t_chan.reader(|buf| Some(&buffers[buf.index()]));
            let r_reader = r_chan.reader(|buf| Some(&buffers[buf.index()]));
            let s_reader = s_chan.reader(|buf| Some(&buffers[buf.index()]));

            let num_frames = t_reader.read_inputs().unwrap().len() as u32;

            writer.write_all(&num_frames.to_le_bytes())?;

            let t_out = t_reader.read_outputs().unwrap();
            let r_out = r_reader.read_outputs().unwrap();
            let s_out = s_reader.read_outputs().unwrap();

            let (t_it, r_it, s_it) = match (t_out, r_out, s_out) {
                (
                    ReadOutputs::Translations(t),
                    ReadOutputs::Rotations(r),
                    ReadOutputs::Scales(s),
                ) => (t, r, s),
                _ => bail!("Bad skeleton"),
            };

            for ((t, r), s) in t_it.zip(r_it.into_f32()).zip(s_it) {
                write_f32_slice(&mut writer, &t)?;
                write_f32_slice(&mut writer, &r)?;
                write_f32_slice(&mut writer, &s)?;
            }
        }
    }

    Ok(())
}

fn process_gltf(path: &Path, opt: &Opt) -> Result<()> {
    let (document, buffers, _images) = gltf::import(path).context("Bad GLTF")?;

    for node in document.nodes() {
        let mesh = match node.mesh() {
            Some(v) => v,
            None => continue,
        };

        process_mesh(&document, mesh, node.skin(), &buffers, opt)?;
    }

    Ok(())
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    for input in &opt.input {
        process_gltf(&input, &opt)?;
    }

    Ok(())
}
